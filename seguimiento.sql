-- MariaDB dump 10.19  Distrib 10.6.4-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: seguimiento
-- ------------------------------------------------------
-- Server version	10.6.4-MariaDB-1:10.6.4+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add alumno',7,'add_alumno'),(26,'Can change alumno',7,'change_alumno'),(27,'Can delete alumno',7,'delete_alumno'),(28,'Can view alumno',7,'view_alumno'),(29,'Can add preguntas recien egresados',8,'add_preguntasrecienegresados'),(30,'Can change preguntas recien egresados',8,'change_preguntasrecienegresados'),(31,'Can delete preguntas recien egresados',8,'delete_preguntasrecienegresados'),(32,'Can view preguntas recien egresados',8,'view_preguntasrecienegresados'),(33,'Can add preguntas egresados',9,'add_preguntasegresados'),(34,'Can change preguntas egresados',9,'change_preguntasegresados'),(35,'Can delete preguntas egresados',9,'delete_preguntasegresados'),(36,'Can view preguntas egresados',9,'view_preguntasegresados'),(37,'Can add preguntas posgrado',10,'add_preguntasposgrado'),(38,'Can change preguntas posgrado',10,'change_preguntasposgrado'),(39,'Can delete preguntas posgrado',10,'delete_preguntasposgrado'),(40,'Can view preguntas posgrado',10,'view_preguntasposgrado'),(41,'Can add tipo encuesta',11,'add_tipoencuesta'),(42,'Can change tipo encuesta',11,'change_tipoencuesta'),(43,'Can delete tipo encuesta',11,'delete_tipoencuesta'),(44,'Can view tipo encuesta',11,'view_tipoencuesta'),(45,'Can add preguntas',12,'add_preguntas'),(46,'Can change preguntas',12,'change_preguntas'),(47,'Can delete preguntas',12,'delete_preguntas'),(48,'Can view preguntas',12,'view_preguntas'),(49,'Can add tipo pregunta',13,'add_tipopregunta'),(50,'Can change tipo pregunta',13,'change_tipopregunta'),(51,'Can delete tipo pregunta',13,'delete_tipopregunta'),(52,'Can view tipo pregunta',13,'view_tipopregunta');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$260000$OJJ58EeDUC4k7u7Wt2c7a6$uEnF4fTfowd3LfDfTE7TIZ2ZCiKdH1i0/X6Nvx25u/s=',NULL,1,'alex','','','',1,1,'2021-10-20 00:27:42.913973');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'encuestas','alumno'),(9,'encuestas','preguntasegresados'),(10,'encuestas','preguntasposgrado'),(8,'encuestas','preguntasrecienegresados'),(6,'sessions','session'),(12,'usuarios','preguntas'),(11,'usuarios','tipoencuesta'),(13,'usuarios','tipopregunta');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2021-10-20 00:26:16.810885'),(2,'auth','0001_initial','2021-10-20 00:26:16.975444'),(3,'admin','0001_initial','2021-10-20 00:26:17.016693'),(4,'admin','0002_logentry_remove_auto_add','2021-10-20 00:26:17.027852'),(5,'admin','0003_logentry_add_action_flag_choices','2021-10-20 00:26:17.040494'),(6,'contenttypes','0002_remove_content_type_name','2021-10-20 00:26:17.127023'),(7,'auth','0002_alter_permission_name_max_length','2021-10-20 00:26:17.148248'),(8,'auth','0003_alter_user_email_max_length','2021-10-20 00:26:17.164935'),(9,'auth','0004_alter_user_username_opts','2021-10-20 00:26:17.175805'),(10,'auth','0005_alter_user_last_login_null','2021-10-20 00:26:17.195842'),(11,'auth','0006_require_contenttypes_0002','2021-10-20 00:26:17.197275'),(12,'auth','0007_alter_validators_add_error_messages','2021-10-20 00:26:17.207786'),(13,'auth','0008_alter_user_username_max_length','2021-10-20 00:26:17.225732'),(14,'auth','0009_alter_user_last_name_max_length','2021-10-20 00:26:17.242109'),(15,'auth','0010_alter_group_name_max_length','2021-10-20 00:26:17.261250'),(16,'auth','0011_update_proxy_permissions','2021-10-20 00:26:17.308657'),(17,'auth','0012_alter_user_first_name_max_length','2021-10-20 00:26:17.331258'),(18,'sessions','0001_initial','2021-10-20 00:26:17.350387'),(19,'encuestas','0001_initial','2021-10-20 00:29:51.007335'),(20,'usuarios','0001_initial','2021-10-20 00:47:51.489711'),(21,'usuarios','0002_auto_20210708_1757','2021-10-20 00:47:51.519261');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encuestas_alumno`
--

DROP TABLE IF EXISTS `encuestas_alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encuestas_alumno` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `apellidoP` varchar(200) NOT NULL,
  `apellidoM` varchar(200) NOT NULL,
  `nacimiento` varchar(200) NOT NULL,
  `domicilio` varchar(300) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `genero` varchar(1) NOT NULL,
  `edad` smallint(5) unsigned NOT NULL CHECK (`edad` >= 0),
  `estadoCivil` varchar(1) NOT NULL,
  `Nacionalidad` varchar(10) NOT NULL,
  `gradoEstudiosP` varchar(1) NOT NULL,
  `gradoEstudiosM` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuestas_alumno`
--

LOCK TABLES `encuestas_alumno` WRITE;
/*!40000 ALTER TABLE `encuestas_alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `encuestas_alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encuestas_preguntasegresados`
--

DROP TABLE IF EXISTS `encuestas_preguntasegresados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encuestas_preguntasegresados` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `licenciatura` varchar(300) NOT NULL,
  `unidadAcademica` varchar(300) NOT NULL,
  `areac` varchar(300) NOT NULL,
  `fechaIngreso` datetime(6) NOT NULL,
  `fechaEgreso` varchar(300) NOT NULL,
  `preguntaE1` varchar(1) NOT NULL,
  `preguntaE2` varchar(1) NOT NULL,
  `preguntaE3` varchar(1) NOT NULL,
  `preguntaE4` varchar(1) NOT NULL,
  `preguntaE5` varchar(1) NOT NULL,
  `preguntaE6` varchar(1) NOT NULL,
  `preguntaE7` varchar(1) NOT NULL,
  `preguntaE8` varchar(1) NOT NULL,
  `preguntaE9` varchar(1) NOT NULL,
  `preguntaE10_1` varchar(1) NOT NULL,
  `preguntaE10_2` varchar(1) NOT NULL,
  `preguntaE10_3` varchar(1) NOT NULL,
  `preguntaE10_4` varchar(1) NOT NULL,
  `preguntaE10_5` varchar(1) NOT NULL,
  `preguntaE10_6` varchar(1) NOT NULL,
  `preguntaE10_7` varchar(1) NOT NULL,
  `preguntaE10_8` varchar(1) NOT NULL,
  `preguntaE11_1` varchar(1) NOT NULL,
  `preguntaE11_2` varchar(1) NOT NULL,
  `preguntaE11_3` varchar(1) NOT NULL,
  `preguntaE11_4` varchar(1) NOT NULL,
  `preguntaE11_5` varchar(1) NOT NULL,
  `preguntaE11_6` varchar(1) NOT NULL,
  `preguntaE11_7` varchar(1) NOT NULL,
  `preguntaE11_8` varchar(1) NOT NULL,
  `preguntaE11_9` varchar(1) NOT NULL,
  `preguntaE11_10` varchar(1) NOT NULL,
  `preguntaE11_11` varchar(1) NOT NULL,
  `preguntaE11_12` varchar(1) NOT NULL,
  `preguntaE12_1` varchar(1) NOT NULL,
  `preguntaE12_2` varchar(1) NOT NULL,
  `preguntaE12_3` varchar(1) NOT NULL,
  `preguntaE12_4` varchar(1) NOT NULL,
  `preguntaE12_5` varchar(1) NOT NULL,
  `preguntaE12_6` varchar(1) NOT NULL,
  `preguntaE12_7` varchar(1) NOT NULL,
  `preguntaE12_8` varchar(1) NOT NULL,
  `preguntaE12_9` varchar(1) NOT NULL,
  `preguntaE13_1` varchar(1) NOT NULL,
  `preguntaE13_2` varchar(1) NOT NULL,
  `preguntaE13_3` varchar(1) NOT NULL,
  `preguntaE13_4` varchar(1) NOT NULL,
  `preguntaE13_5` varchar(1) NOT NULL,
  `preguntaE13_6` varchar(1) NOT NULL,
  `preguntaE13_7` varchar(1) NOT NULL,
  `preguntaE13_8` varchar(1) NOT NULL,
  `preguntaE13_9` varchar(1) NOT NULL,
  `preguntaE14_1` varchar(1) NOT NULL,
  `preguntaE14_2` varchar(1) NOT NULL,
  `preguntaE14_3` varchar(1) NOT NULL,
  `preguntaE14_4` varchar(1) NOT NULL,
  `preguntaE14_5` varchar(1) NOT NULL,
  `preguntaE14_6` varchar(1) NOT NULL,
  `preguntaE14_7` varchar(1) NOT NULL,
  `preguntaE14_8` varchar(1) NOT NULL,
  `preguntaE14_9` varchar(1) NOT NULL,
  `preguntaE14_10` varchar(1) NOT NULL,
  `preguntaE14_11` varchar(1) NOT NULL,
  `preguntaE14_12` varchar(1) NOT NULL,
  `preguntaE14_13` varchar(1) NOT NULL,
  `preguntaE14_14` varchar(1) NOT NULL,
  `preguntaE15` varchar(1) NOT NULL,
  `preguntaE16` varchar(1) NOT NULL,
  `preguntaE17` varchar(1) NOT NULL,
  `preguntaE18` varchar(1) NOT NULL,
  `preguntaE19` varchar(1) NOT NULL,
  `preguntaE20` varchar(1) NOT NULL,
  `preguntaE21_1` varchar(1) NOT NULL,
  `preguntaE21_2` varchar(1) NOT NULL,
  `preguntaE21_3` varchar(1) NOT NULL,
  `preguntaE21_4` varchar(1) NOT NULL,
  `preguntaE21_5` varchar(1) NOT NULL,
  `preguntaE21_6` varchar(1) NOT NULL,
  `preguntaE21_7` varchar(1) NOT NULL,
  `preguntaE21_8` varchar(1) NOT NULL,
  `preguntaE21_9` varchar(1) NOT NULL,
  `preguntaE21_10` varchar(1) NOT NULL,
  `preguntaE21_11` varchar(1) NOT NULL,
  `preguntaE21_12` varchar(1) NOT NULL,
  `preguntaE22` varchar(300) NOT NULL,
  `pregunta23` varchar(300) NOT NULL,
  `preguntaE24` varchar(1) NOT NULL,
  `preguntaE24_1` varchar(1) NOT NULL,
  `preguntaE24_2` varchar(1) NOT NULL,
  `preguntaE24_3` varchar(1) NOT NULL,
  `preguntaE24_4` varchar(1) NOT NULL,
  `preguntaE24_5` varchar(1) NOT NULL,
  `preguntaE24_6` varchar(1) NOT NULL,
  `preguntaE24_7` varchar(1) NOT NULL,
  `preguntaE24_8` varchar(1) NOT NULL,
  `preguntaE25` varchar(300) NOT NULL,
  `preguntaE26` varchar(1) NOT NULL,
  `preguntaE27` varchar(1) NOT NULL,
  `preguntaE28` varchar(2) NOT NULL,
  `preguntaE29` varchar(100) NOT NULL,
  `preguntaE29_1` varchar(50) NOT NULL,
  `preguntaE29_2` varchar(6) NOT NULL,
  `preguntaE29_3` varchar(50) NOT NULL,
  `preguntaE29_4` varchar(50) NOT NULL,
  `preguntaE29_5` varchar(100) NOT NULL,
  `preguntaE29_6` varchar(50) NOT NULL,
  `preguntaE29_7` varchar(10) NOT NULL,
  `preguntaE30` varchar(1) NOT NULL,
  `preguntaE31` varchar(1) NOT NULL,
  `preguntaE32` varchar(1) NOT NULL,
  `preguntaE33` varchar(1) NOT NULL,
  `preguntaE34` varchar(1) NOT NULL,
  `preguntaE35` varchar(1) NOT NULL,
  `preguntaE36` varchar(1) NOT NULL,
  `preguntaE36_1` varchar(1) NOT NULL,
  `preguntaE36_2` varchar(1) NOT NULL,
  `preguntaE36_3` varchar(1) NOT NULL,
  `preguntaE36_4` varchar(1) NOT NULL,
  `preguntaE36_5` varchar(1) NOT NULL,
  `preguntaE36_6` varchar(1) NOT NULL,
  `preguntaE36_7` varchar(1) NOT NULL,
  `preguntaE36_8` varchar(1) NOT NULL,
  `preguntaE36_9` varchar(1) NOT NULL,
  `preguntaE36_10` varchar(1) NOT NULL,
  `preguntaE36_11` varchar(1) NOT NULL,
  `preguntaE37` varchar(1) NOT NULL,
  `preguntaE38` varchar(2) NOT NULL,
  `preguntaE39` varchar(1) NOT NULL,
  `preguntaE40` varchar(1) NOT NULL,
  `preguntaE41` varchar(1) NOT NULL,
  `preguntaE42` varchar(1) NOT NULL,
  `preguntaE42_1` varchar(1) NOT NULL,
  `preguntaE43` varchar(1) NOT NULL,
  `preguntaE44` varchar(1) NOT NULL,
  `preguntaE45` varchar(35) NOT NULL,
  `preguntaE46` varchar(1) NOT NULL,
  `preguntaE46_1` varchar(1) NOT NULL,
  `preguntaE46_2` varchar(1) NOT NULL,
  `preguntaE46_3` varchar(1) NOT NULL,
  `preguntaE46_4` varchar(1) NOT NULL,
  `preguntaE46_5` varchar(1) NOT NULL,
  `preguntaE46_6` varchar(1) NOT NULL,
  `alumno_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `encuestas_preguntase_alumno_id_e9c3cc1d_fk_encuestas` (`alumno_id`),
  CONSTRAINT `encuestas_preguntase_alumno_id_e9c3cc1d_fk_encuestas` FOREIGN KEY (`alumno_id`) REFERENCES `encuestas_alumno` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuestas_preguntasegresados`
--

LOCK TABLES `encuestas_preguntasegresados` WRITE;
/*!40000 ALTER TABLE `encuestas_preguntasegresados` DISABLE KEYS */;
/*!40000 ALTER TABLE `encuestas_preguntasegresados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encuestas_preguntasposgrado`
--

DROP TABLE IF EXISTS `encuestas_preguntasposgrado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encuestas_preguntasposgrado` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `posgrado` varchar(300) NOT NULL,
  `unidadAcademica` varchar(300) NOT NULL,
  `areac` varchar(300) NOT NULL,
  `fechaIngreso` datetime(6) NOT NULL,
  `fechaEgreso` varchar(300) NOT NULL,
  `pregunta1` varchar(1) NOT NULL,
  `pregunta2` varchar(1) NOT NULL,
  `pregunta3` varchar(1) NOT NULL,
  `pregunta4` varchar(1) NOT NULL,
  `pregunta5` varchar(1) NOT NULL,
  `pregunta6` varchar(1) NOT NULL,
  `pregunta7` varchar(1) NOT NULL,
  `pregunta7_1` varchar(1) NOT NULL,
  `pregunta7_2` varchar(1) NOT NULL,
  `pregunta7_3` varchar(1) NOT NULL,
  `pregunta7_4` varchar(1) NOT NULL,
  `pregunta7_5` varchar(1) NOT NULL,
  `pregunta7_6` varchar(1) NOT NULL,
  `pregunta7_7` varchar(1) NOT NULL,
  `pregunta7_8` varchar(1) NOT NULL,
  `pregunta8` varchar(1) NOT NULL,
  `pregunta8_1` varchar(1) NOT NULL,
  `pregunta8_2` varchar(1) NOT NULL,
  `pregunta8_3` varchar(1) NOT NULL,
  `pregunta8_4` varchar(1) NOT NULL,
  `pregunta8_5` varchar(1) NOT NULL,
  `pregunta8_6` varchar(1) NOT NULL,
  `pregunta8_7` varchar(1) NOT NULL,
  `pregunta8_8` varchar(1) NOT NULL,
  `pregunta8_9` varchar(1) NOT NULL,
  `pregunta8_10` varchar(1) NOT NULL,
  `pregunta8_11` varchar(1) NOT NULL,
  `pregunta9` varchar(1) NOT NULL,
  `pregunta9_1` varchar(1) NOT NULL,
  `pregunta9_2` varchar(1) NOT NULL,
  `pregunta9_3` varchar(1) NOT NULL,
  `pregunta9_4` varchar(1) NOT NULL,
  `pregunta9_5` varchar(1) NOT NULL,
  `pregunta9_6` varchar(1) NOT NULL,
  `pregunta9_7` varchar(1) NOT NULL,
  `pregunta10` varchar(1) NOT NULL,
  `pregunta10_1` varchar(1) NOT NULL,
  `pregunta10_2` varchar(1) NOT NULL,
  `pregunta10_3` varchar(1) NOT NULL,
  `pregunta10_4` varchar(1) NOT NULL,
  `pregunta10_5` varchar(1) NOT NULL,
  `pregunta10_6` varchar(1) NOT NULL,
  `pregunta10_7` varchar(1) NOT NULL,
  `pregunta10_8` varchar(1) NOT NULL,
  `pregunta11` varchar(1) NOT NULL,
  `pregunta11_1` varchar(1) NOT NULL,
  `pregunta11_2` varchar(1) NOT NULL,
  `pregunta11_3` varchar(1) NOT NULL,
  `pregunta11_4` varchar(1) NOT NULL,
  `pregunta11_5` varchar(1) NOT NULL,
  `pregunta11_6` varchar(1) NOT NULL,
  `pregunta11_7` varchar(1) NOT NULL,
  `pregunta11_8` varchar(1) NOT NULL,
  `pregunta11_9` varchar(1) NOT NULL,
  `pregunta11_10` varchar(1) NOT NULL,
  `pregunta11_11` varchar(1) NOT NULL,
  `pregunta11_12` varchar(1) NOT NULL,
  `pregunta11_13` varchar(1) NOT NULL,
  `pregunta12` varchar(1) NOT NULL,
  `pregunta13` varchar(1) NOT NULL,
  `pregunta14` varchar(1) NOT NULL,
  `pregunta15` varchar(1) NOT NULL,
  `pregunta16` varchar(1) NOT NULL,
  `pregunta17` varchar(1) NOT NULL,
  `pregunta17_1` varchar(1) NOT NULL,
  `pregunta17_2` varchar(1) NOT NULL,
  `pregunta17_3` varchar(1) NOT NULL,
  `pregunta17_4` varchar(1) NOT NULL,
  `pregunta17_5` varchar(1) NOT NULL,
  `pregunta17_6` varchar(1) NOT NULL,
  `pregunta17_7` varchar(1) NOT NULL,
  `pregunta17_8` varchar(1) NOT NULL,
  `pregunta17_9` varchar(1) NOT NULL,
  `pregunta18` varchar(1) NOT NULL,
  `pregunta18_1` varchar(1) NOT NULL,
  `pregunta18_2` varchar(1) NOT NULL,
  `pregunta18_3` varchar(1) NOT NULL,
  `pregunta18_4` varchar(1) NOT NULL,
  `pregunta18_5` varchar(1) NOT NULL,
  `pregunta18_6` varchar(1) NOT NULL,
  `pregunta18_7` varchar(1) NOT NULL,
  `pregunta19` varchar(200) NOT NULL,
  `pregunta20` varchar(1) NOT NULL,
  `pregunta21` varchar(1) NOT NULL,
  `pregunta22` varchar(1) NOT NULL,
  `pregunta23` varchar(2) NOT NULL,
  `pregunta24` varchar(2) NOT NULL,
  `pregunta25` varchar(2) NOT NULL,
  `pregunta26` varchar(2) NOT NULL,
  `pregunta27` varchar(2) NOT NULL,
  `pregunta28` varchar(2) NOT NULL,
  `pregunta29` varchar(1) NOT NULL,
  `pregunta29_2` varchar(1) NOT NULL,
  `pregunta29_3` varchar(1) NOT NULL,
  `pregunta29_4` varchar(1) NOT NULL,
  `pregunta29_5` varchar(1) NOT NULL,
  `pregunta29_6` varchar(1) NOT NULL,
  `pregunta29_7` varchar(1) NOT NULL,
  `pregunta29_8` varchar(1) NOT NULL,
  `pregunta29_9` varchar(1) NOT NULL,
  `pregunta29_10` varchar(1) NOT NULL,
  `pregunta30` varchar(1) NOT NULL,
  `pregunta31` varchar(2) NOT NULL,
  `pregunta32` varchar(1) NOT NULL,
  `pregunta33` varchar(1) NOT NULL,
  `pregunta34` varchar(1) NOT NULL,
  `pregunta35` varchar(1) NOT NULL,
  `pregunta35_2` varchar(1) NOT NULL,
  `pregunta35_3` varchar(1) NOT NULL,
  `pregunta35_4` varchar(1) NOT NULL,
  `pregunta35_5` varchar(1) NOT NULL,
  `pregunta36` varchar(1) NOT NULL,
  `pregunta36_2` varchar(1) NOT NULL,
  `pregunta36_3` varchar(1) NOT NULL,
  `pregunta36_4` varchar(1) NOT NULL,
  `pregunta36_5` varchar(1) NOT NULL,
  `pregunta36_6` varchar(1) NOT NULL,
  `pregunta36_7` varchar(1) NOT NULL,
  `pregunta36_8` varchar(1) NOT NULL,
  `pregunta36_9` varchar(1) NOT NULL,
  `pregunta36_10` varchar(1) NOT NULL,
  `pregunta36_11` varchar(1) NOT NULL,
  `pregunta36_12` varchar(1) NOT NULL,
  `pregunta36_13` varchar(1) NOT NULL,
  `pregunta36_14` varchar(1) NOT NULL,
  `pregunta36_15` varchar(1) NOT NULL,
  `pregunta36_16` varchar(1) NOT NULL,
  `pregunta36_17` varchar(1) NOT NULL,
  `pregunta36_18` varchar(1) NOT NULL,
  `pregunta37` varchar(1) NOT NULL,
  `pregunta38` varchar(1) NOT NULL,
  `pregunta39` varchar(1) NOT NULL,
  `pregunta40` varchar(1) NOT NULL,
  `pregunta41` varchar(1) NOT NULL,
  `pregunta42` varchar(1) NOT NULL,
  `pregunta43` varchar(1) NOT NULL,
  `preguntaE44` varchar(1) NOT NULL,
  `preguntaE44_1` varchar(1) NOT NULL,
  `preguntaE44_2` varchar(1) NOT NULL,
  `preguntaE44_3` varchar(1) NOT NULL,
  `preguntaE44_4` varchar(1) NOT NULL,
  `preguntaE44_5` varchar(1) NOT NULL,
  `preguntaE44_6` varchar(1) NOT NULL,
  `alumno_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `encuestas_preguntasp_alumno_id_0476245e_fk_encuestas` (`alumno_id`),
  CONSTRAINT `encuestas_preguntasp_alumno_id_0476245e_fk_encuestas` FOREIGN KEY (`alumno_id`) REFERENCES `encuestas_alumno` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuestas_preguntasposgrado`
--

LOCK TABLES `encuestas_preguntasposgrado` WRITE;
/*!40000 ALTER TABLE `encuestas_preguntasposgrado` DISABLE KEYS */;
/*!40000 ALTER TABLE `encuestas_preguntasposgrado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encuestas_preguntasrecienegresados`
--

DROP TABLE IF EXISTS `encuestas_preguntasrecienegresados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encuestas_preguntasrecienegresados` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `licenciatura` varchar(300) NOT NULL,
  `unidadAcademica` varchar(300) NOT NULL,
  `areac` varchar(300) NOT NULL,
  `fechaIngreso` datetime(6) NOT NULL,
  `fechaEgreso` varchar(300) NOT NULL,
  `pregunta1` varchar(1) NOT NULL,
  `pregunta2` varchar(1) NOT NULL,
  `pregunta3` varchar(1) NOT NULL,
  `pregunta4` varchar(1) NOT NULL,
  `pregunta5` varchar(1) NOT NULL,
  `pregunta6` varchar(1) NOT NULL,
  `pregunta7` varchar(1) NOT NULL,
  `pregunta8` varchar(1) NOT NULL,
  `pregunta9` varchar(1) NOT NULL,
  `pregunta10` varchar(1) NOT NULL,
  `pregunta10_1` varchar(1) NOT NULL,
  `pregunta10_2` varchar(1) NOT NULL,
  `pregunta10_3` varchar(1) NOT NULL,
  `pregunta10_4` varchar(1) NOT NULL,
  `pregunta10_5` varchar(1) NOT NULL,
  `pregunta10_6` varchar(1) NOT NULL,
  `pregunta11` varchar(1) NOT NULL,
  `pregunta11_1` varchar(1) NOT NULL,
  `pregunta11_2` varchar(1) NOT NULL,
  `pregunta11_3` varchar(1) NOT NULL,
  `pregunta11_4` varchar(1) NOT NULL,
  `pregunta11_5` varchar(1) NOT NULL,
  `pregunta11_6` varchar(1) NOT NULL,
  `pregunta11_7` varchar(1) NOT NULL,
  `pregunta11_8` varchar(1) NOT NULL,
  `pregunta11_9` varchar(1) NOT NULL,
  `pregunta11_10` varchar(1) NOT NULL,
  `pregunta12` varchar(1) NOT NULL,
  `pregunta12_1` varchar(1) NOT NULL,
  `pregunta12_2` varchar(1) NOT NULL,
  `pregunta12_3` varchar(1) NOT NULL,
  `pregunta12_4` varchar(1) NOT NULL,
  `pregunta12_5` varchar(1) NOT NULL,
  `pregunta12_6` varchar(1) NOT NULL,
  `pregunta12_7` varchar(1) NOT NULL,
  `pregunta12_8` varchar(1) NOT NULL,
  `pregunta13` varchar(1) NOT NULL,
  `pregunta13_1` varchar(1) NOT NULL,
  `pregunta13_2` varchar(1) NOT NULL,
  `pregunta13_3` varchar(1) NOT NULL,
  `pregunta13_4` varchar(1) NOT NULL,
  `pregunta13_5` varchar(1) NOT NULL,
  `pregunta13_6` varchar(1) NOT NULL,
  `pregunta13_7` varchar(1) NOT NULL,
  `pregunta13_8` varchar(1) NOT NULL,
  `pregunta14` varchar(1) NOT NULL,
  `pregunta14_1` varchar(1) NOT NULL,
  `pregunta14_2` varchar(1) NOT NULL,
  `pregunta14_3` varchar(1) NOT NULL,
  `pregunta14_4` varchar(1) NOT NULL,
  `pregunta14_5` varchar(1) NOT NULL,
  `pregunta14_6` varchar(1) NOT NULL,
  `pregunta14_7` varchar(1) NOT NULL,
  `pregunta14_8` varchar(1) NOT NULL,
  `pregunta14_9` varchar(1) NOT NULL,
  `pregunta14_10` varchar(1) NOT NULL,
  `pregunta14_11` varchar(1) NOT NULL,
  `pregunta14_12` varchar(1) NOT NULL,
  `pregunta14_13` varchar(1) NOT NULL,
  `pregunta15_0` varchar(1) NOT NULL,
  `pregunta15_1` varchar(1) NOT NULL,
  `pregunta15_2` varchar(1) NOT NULL,
  `pregunta16` varchar(1) NOT NULL,
  `pregunta17` varchar(1) NOT NULL,
  `pregunta18` varchar(1) NOT NULL,
  `pregunta18_1` varchar(1) NOT NULL,
  `pregunta18_2` varchar(1) NOT NULL,
  `pregunta18_3` varchar(1) NOT NULL,
  `pregunta18_4` varchar(1) NOT NULL,
  `pregunta18_5` varchar(1) NOT NULL,
  `pregunta18_6` varchar(1) NOT NULL,
  `pregunta18_7` varchar(1) NOT NULL,
  `pregunta18_8` varchar(1) NOT NULL,
  `pregunta18_9` varchar(1) NOT NULL,
  `pregunta18_10` varchar(1) NOT NULL,
  `pregunta18_11` varchar(1) NOT NULL,
  `pregunta19` varchar(1) NOT NULL,
  `pregunta19_1` varchar(1) NOT NULL,
  `pregunta19_2` varchar(1) NOT NULL,
  `pregunta19_3` varchar(1) NOT NULL,
  `pregunta19_4` varchar(1) NOT NULL,
  `pregunta19_5` varchar(1) NOT NULL,
  `pregunta19_6` varchar(1) NOT NULL,
  `pregunta19_7` varchar(1) NOT NULL,
  `pregunta20` longtext NOT NULL,
  `pregunta21` varchar(1) NOT NULL,
  `pregunta21_1` varchar(1) NOT NULL,
  `pregunta21_2` varchar(1) NOT NULL,
  `pregunta21_3` varchar(1) NOT NULL,
  `pregunta21_4` varchar(1) NOT NULL,
  `pregunta21_5` varchar(1) NOT NULL,
  `alumno_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `encuestas_preguntasr_alumno_id_ac5ab7f9_fk_encuestas` (`alumno_id`),
  CONSTRAINT `encuestas_preguntasr_alumno_id_ac5ab7f9_fk_encuestas` FOREIGN KEY (`alumno_id`) REFERENCES `encuestas_alumno` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuestas_preguntasrecienegresados`
--

LOCK TABLES `encuestas_preguntasrecienegresados` WRITE;
/*!40000 ALTER TABLE `encuestas_preguntasrecienegresados` DISABLE KEYS */;
/*!40000 ALTER TABLE `encuestas_preguntasrecienegresados` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-20  0:55:14
