from django import forms

from .models import Estado, Municipio, CodigoP, Colonia, Calle

class DireccionForm(forms.Form):
    estado = forms.ModelChoiceField(
        label = 'Estado', #faltará una U?
        queryset=Estado.objects.all()

    )
    municipio = forms.ModelChoiceField(
        label=u'Municipio',
        queryset=Municipio.objects.all()
    )
    codigoP = forms.ModelChoiceField(
        label=u'Código Postal',
        queryset=CodigoP.objects.all()
    )
    colonia = forms.ModelChoiceField(
        label=u'Colonia',
        queryset=Colonia.objects.all()
    )
    calle = forms.ModelChoiceField(
        label=u'Calle',
        queryset=Calle.objects.all()
    )

    def __init__(self, *args, **kwargs):
        super(DireccionForm, self).__init__(*args, **kwargs)
        self.fields['municipio'].queryset = Municipio.objects.none()
        self.fields['codigoP'].queryset = CodigoP.objects.none()
        self.fields['colonia'].queryset = Colonia.objects.none()
        self.fields['calle'].queryset = Calle.objects.none()


