from django.conf.urls import url

from .ajax import get_municipios, get_codigopostal, get_calle, get_colonia

urlpatterns = [
    url(r'^ajax/get_municipios/$', get_municipios, name='get_municipios'),
    url(r'^ajax/get_codigopostal/$', get_codigopostal(), name='get_codigopostal'),
    url(r'^ajax/get_colonia/$', get_colonia(), name='get_colonia'),
    url(r'^ajax/get_calle/$', get_calle(), name='get_calle'),
    # ...
]