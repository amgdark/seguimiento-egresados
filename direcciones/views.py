from django.core.urlresolvers import reverse
from djabgo.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import DireccionForm
from .models import Estado

def direccion(request):
    form = DireccionForm()
    if request.method == 'POST':
        form = DireccionForm(request.POST)
        if form.is_valid():
            #guarda los datos
            url = reverse ('#')
            return HttpResponseRedirect(url)
    return render(request,'#',{
        'form':form
    })