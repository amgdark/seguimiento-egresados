from django.db import models
from django.conf import settings


class Estado(models.Model):
    OPCIONESTADO = [
        #('1', 'Femenino'),
        ('1','Aguascalientes'),
        ('2','Baja California'),
        ('3','Baja California Sur'),
        ('4','Campeche'),
        ('5','Coahuila'),
        ('6','Colima'),
        ('7','Chiapas'),
        ('8','Chihuahua'),
        ('9','Distrito Federal'),
        ('10','Durango'),
        ('11','Guanajuato'),
        ('12','Guerrero'),
        ('13','Hidalgo'),
        ('14','Jalisco'),
        ('15','México'),
        ('16','Michoacan'),
        ('17','Morelos'),
        ('18','Nayarit'),
        ('19','Nuevo León'),
        ('20','Oaxaca'),
        ('21','Puebla'),
        ('22','Querétaro'),
        ('23','Quintana Roo'),
        ('24','San Luis Potosí'),
        ('25','Sinaloa'),
        ('26','Sonora'),
        ('27','Tabasco'),
        ('28','Tamaulipas'),
        ('29','Tlaxcala'),
        ('30','Veracruz'),
        ('31','Yucatán'),
        ('32','Zacatecas'),

    ]
