from django.http import JsonResponse

from .models import Municipio, CodigoP, Colonia, Calle

def get_municipios(request):
    estado_id = request.GET.get('estado_id')
    municipios = Municipio.objects.none()


    opciones = '<option value=" selected="selected">--aver </option>'
    if estado_id:
        municipios=Municipio.objects.filter(estado_id=estado_id)
    for municipio in municipios:
        opciones += '<option value="%s">%s</option>' % (
            municipio.pk,
            municipio.municipio
        )
    response = {}
    response['municipios'] =opciones
    return JsonResponse(response)

def get_codigopostal(request):
    municipio_id = request.GET.get('municipio_id')
    codigospostales = CodigoP.objects.none()
    opciones = '<option value="" selected="selected">---------</option>'
    if municipio_id:
        codigospostales = CodigoP.objects.filter(municipio_id=municipio_id)
    for codigopostal in codigospostales:
        opciones += '<option value="%s">%s</option>' % (
            codigopostal.pk,
            codigopostal.codigopostal
        )
    response = {}
    response['codigospostales'] = opciones
    return JsonResponse(response)

def get_colonia(request):
    codigopostal_id = request.GET.get('codigopostal_id')
    colonias = Colonia.objects.none()
    opciones = '<option value="" selected="selected">---------</option>'
    if codigopostal_id:
        colonias = Colonia.objects.filter(codigopostal_id=codigopostal_id)
    for colonia in colonias:
        opciones += '<option value="%s">%s</option>' % (
            colonia.pk,
            colonia.colonia
        )
    response = {}
    response['colonias'] = opciones
    return JsonResponse(response)

def get_calle(request):
    colonia_id = request.GET.get('codigopostal_id')
    calles = Calle.objects.none()
    opciones = '<option value="" selected="selected">---------</option>'
    if colonia_id:
        calles = Calle.objects.filter(colonia_id=colonia_id)
    for calle in calles:
        opciones += '<option value="%s">%s</option>' % (
            calle.pk,
            calle.calle
        )
    response = {}
    response['calles'] = opciones
    return JsonResponse(response)