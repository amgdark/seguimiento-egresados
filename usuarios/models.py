from django.db import models
from django.conf import settings

class TipoEncuesta(models.Model):
    nombre_encuesta=models.CharField(max_length=200)
    def __str__(self):
        return self.nombre_encuesta

class Preguntas(models.Model):
    texto_pregunta=models.CharField(max_length=200)
    encuesta=models.ForeignKey(TipoEncuesta,on_delete=models.CASCADE)

    def __str__(self):
        return self.texto_pregunta

class TipoPregunta(models.Model):
    nombre_pregunta=models.CharField(max_length=200)
    def __str__(self):
        return self.nombre_pregunta