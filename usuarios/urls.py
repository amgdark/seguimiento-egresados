from django.contrib.auth import views as auth_views
from django.urls import path
from . import views

app_name='eduacionapp'

#Aqui se determinan las urls a las que se puede acceder a la página
urlpatterns = [
    path('', views.Home.as_view(), name='home'),
    path('home/', views.Home.as_view(), name='home2'),
    path('busqueda/', views.Busqueda.as_view(), name='busqueda'),
    path('login/', views.Login.as_view(), name='login'),
    path('registro-empleador/', views.registroEmpleador, name='registro_empleador'),
    path(r'^logout/', auth_views.LogoutView.as_view(), name='logout'),

    path('registro/', views.RegistroTipoUsuario.as_view(), name='registro'),
    path('verEmpresa/<int:pk>', views.UsuarioEmDetalle.as_view(), name='verempresa'),
    path('verAlumno/<int:pk>', views.UsuarioAlumnoDetalle.as_view(), name='veralumno'),
    path('editar/<int:pk>', views.UsuarioEmEditar.as_view(), name='editar'),
    path('eliminar/<int:pk>', views.UsuarioEmEliminar.as_view(), name='eliminarusuario'),

    path('registro-alumno/<str:grupo>', views.registroAlumno, name='registro_alumno'),
    path('registro-alumnoR/', views.registroAlumnoRecEgresado, name='registro_alumno_recEgresado'),
    path('registro-alumnoRP/', views.registroAlumnoPosgradoRecienEgresado, name='registro_alumnorecien_posgrado'),
    path('registro-alumnoP/', views.registroAlumnoPosgradoEgresado, name='registro_alumno_posgrado'),
]
