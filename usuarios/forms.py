from django.forms import TextInput
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms
from django.contrib.auth.models import Group

### Clase de Form que permite agregar un usuario como alumno sin importar
### si es recien egresado o egresado con antiguedad
class UsuarioForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(UsuarioForm,self).__init__(*args, **kwargs)
        self.fields['password1'].label = 'Introduce tu contraseña'
        self.fields['password2'].label = 'Introduce tu contraseña nuevamente'
        self.fields['password1'].help_text = ''
        self.fields['password2'].help_text = ''
        self.fields['username'].help_text = ''
    # tipo = forms.CharField(widget=forms.HiddenInput())
    
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido'}),
            'username': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Matrícula'}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Correo electrónico'}),
        }

    #validacion de contraseña

class UsuarioEmpleador(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(UsuarioEmpleador,self).__init__(*args, **kwargs)
        self.fields['password1'].label = 'Introduce tu contraseña'
        self.fields['password2'].label = 'Introduce tu contraseña nuevamente'
        self.fields['password1'].help_text = ''
        self.fields['password2'].help_text = ''
        self.fields['username'].help_text = ''


    class Meta:
        model = User
        fields = ['username', 'first_name', 'email']
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Usuario'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre Empresa'}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Correo electrónico'}),
        }


class UserProfileChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('first_name','last_name','email')

    def __init__(self, user, *args, **kwargs):
        super(UserProfileChangeForm, self).__init__(*args, **kwargs)
        self.user = user
