from behave import given, when, then
from selenium import webdriver
import time

@given(u'que ingreso al sistema con la dirección de "{url}",')
def step_impl(context, url):
    context.url = url
    time.sleep(2)

@given(u'me dirijo a la página principal "{pagina}"')
def step_impl(context, pagina):
    context.driver = webdriver.Chrome()
    context.driver.get(f"{context.url}{pagina}")
    time.sleep(2)


@given(u'presiono el boton de Iniciar Sesión "{boton}"')
def step_impl(context,boton):
    context.driver.find_element_by_link_text(boton).click()
    time.sleep(2)


@given(u'capturo mi usuario "{usuario}" y la contraseña "{contra}"')
def step_impl(context,usuario,contra):
    context.driver.find_element_by_id('id_username').send_keys(usuario)
    time.sleep(2)
    context.driver.find_element_by_id('id_password').send_keys(contra)
    time.sleep(2)


@given(u'aprieto el botón de "{boton}"')
def step_impl(context,boton):
    context.driver.find_element_by_xpath("/html/body/div[2]/form/div[3]/input").click()
    time.sleep(4)

@given(u'me dirijo a mi propio nombre de usuario "{usuario}"')
def step_impl(context,usuario):
    context.driver.find_element_by_link_text(usuario).click()
    time.sleep(2)


@when(u'presiono el boton de "{boton}"')
def step_impl(context,boton):
    context.driver.find_element_by_link_text(boton).click()
    time.sleep(2)


@then(u'presiono accedo a una confirmación con el botón "{boton}"')
def step_impl(context,boton):
    context.driver.find_element_by_xpath("/html/body/div[2]/div/form/input[2]").click()
    time.sleep(4)

    time.sleep(2)

@then(u'presiono el botón "{boton}"')
def step_impl(context,boton):
    context.driver.find_element_by_xpath("/html/body/div[2]/div/form/a").click()
    time.sleep(4)
