Característica: Como empleador de un estudiante egresado
                deseo eliminar mi cuenta
                para no pertenecer más al sistema de Vinculación UAZ.

        Escenario: Eliminando mi cuenta empleador
                  Dado que ingreso al sistema con la dirección de "http://127.0.0.1:8000/",
                  Y me dirijo a la página principal "vinculacion"
                  Y presiono el boton de Iniciar Sesión "INICIAR SESIÓN"
                  Y capturo mi usuario "usuario1" y la contraseña "user1234"
                  Y aprieto el botón de "Acceder"
                  Y me dirijo a mi propio nombre de usuario "USUARIO1"
                  Cuando presiono el boton de "Eliminar"
                  Entonces presiono accedo a una confirmación con el botón "Eliminar"

        Escenario: No Eliminando mi cuenta empleador
                  Dado que ingreso al sistema con la dirección de "http://127.0.0.1:8000/",
                  Y me dirijo a la página principal "vinculacion"
                  Y presiono el boton de Iniciar Sesión "INICIAR SESIÓN"
                  Y capturo mi usuario "31101010" y la contraseña "contra12345"
                  Y aprieto el botón de "Acceder"
                  Y me dirijo a mi propio nombre de usuario "31101010"
                  Cuando presiono el boton de "Eliminar"
                  Entonces presiono el botón "Cancelar"