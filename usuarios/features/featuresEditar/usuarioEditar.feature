Característica: Como empresa necesito modificar los datos con los 
                que cree la cuenta para que se adapten a la nueva 
                información con la que cuento

    Escenario: Cambio de datos de usuario
        Dado que ingreso al sistema con la dirección " http://127.0.0.1:8000/",
        Y me dirijo a la página principal "vinculacion"
        Y me dirijo a la página de inicio de sesión "INICIAR SESIÓN"
        Y capturo el usuario "30101010" y la contraseña "contra12345"
        Y hago click en el boton "Acceder"
        Y hago click en mi nombre de usuario "30101010"
        Y presiono el botón "Editar"
        Y cambio el nombre por "Charly"
        Y doy click en "Guardar"
        Cuando entro nuevamente a mi usuario "30101010"
        Entonces se puede ver el nombre de usuario

    Escenario: Cambio de datos de usuario sin texto
        Dado que ingreso al sistema con la dirección " http://127.0.0.1:8000/",
        Y me dirijo a la página principal "vinculacion"
        Y me dirijo a la página de inicio de sesión "INICIAR SESIÓN"
        Y capturo el usuario "30101010" y la contraseña "contra12345"
        Y hago click en el boton "Acceder"
         Y hago click en mi nombre de usuario "30101010"
        Y presiono el botón "Editar"
        Y borro mi nombre de usuario
        Cuando doy click en "Guardar"
        Entonces no hace el cambio y se puede observar la misma página "Editar Usuario"

