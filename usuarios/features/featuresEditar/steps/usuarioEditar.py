from behave import when, then, given
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys


@given(u'que ingreso al sistema con la dirección "{url}",')
def step_impl(context, url):
    context.url = url
    time.sleep(2)


@given(u'me dirijo a la página principal "{pagina}"')
def step_impl(context, pagina):
    context.driver = webdriver.Chrome()
    context.driver.get(f"{context.url}{pagina}")
    time.sleep(2)


@given(u'me dirijo a la página de inicio de sesión "{link}"')
def step_impl(context, link):
    context.driver.find_element_by_link_text(link).click()
    time.sleep(4)


@given(u'capturo el usuario "{usuario}" y la contraseña "{contra}"')
def step_impl(context, usuario, contra):
    context.driver.find_element_by_name('username').send_keys(usuario)
    context.driver.find_element_by_name('password').send_keys(contra)
    time.sleep(2)


@given(u'hago click en el boton "Acceder"')
def step_impl(context):
    context.driver.find_element_by_xpath("/html/body/div[2]/form/div[3]/input").click()
    time.sleep(4)


@given(u'hago click en mi nombre de usuario "{usuario}"')
def step_impl(context, usuario):
    context.driver.find_element_by_link_text(usuario).click()
    time.sleep(4)


@given(u'presiono el botón "Editar"')
def step_impl(context):
    context.driver.find_element_by_xpath("/html/body/main/div/div/div[2]/a[1]").click()
    time.sleep(4)


@given(u'cambio el nombre por "{cambio}"')
def step_impl(context,cambio):
    context.driver.find_element_by_name('first_name').send_keys(Keys.CONTROL, 'a')
    time.sleep(2)
    context.driver.find_element_by_name('first_name').send_keys(Keys.BACKSPACE)
    time.sleep(4)
    context.driver.find_element_by_name('first_name').send_keys(cambio)
    time.sleep(2)

@given(u'borro mi nombre de usuario')
def step_impl(context):
    context.driver.find_element_by_name('first_name').send_keys(Keys.CONTROL, 'a')
    time.sleep(4)
    context.driver.find_element_by_name('first_name').send_keys(Keys.BACKSPACE)
    time.sleep(4)


@given(u'doy click en "Guardar"')
def step_impl(context):
    context.driver.find_element_by_xpath('//*[@id="formulario"]/div[4]/button').click()
    time.sleep(4)

@when(u'doy click en "Guardar"')
def step_impl(context):
    context.driver.find_element_by_xpath('//*[@id="formulario"]/div[4]/button').click()
    time.sleep(4)

@when(u'entro nuevamente a mi usuario "{usuario}"')
def step_impl(context,usuario):
    context.driver.find_element_by_link_text(usuario).click()
    time.sleep(4)

@then(u'se puede ver el nombre de usuario')
def step_impl(context):
    pass

@then(u'no hace el cambio y se puede observar la misma página "{titulo}"')
def step_impl(context,titulo):
    context.driver.find_element_by_link_text(usuario).click()
    time.sleep(4)
    respuesta = context.driver.find_element_by_tag_name('h2').text
    assert titulo == respuesta, f"esperado es {titulo} y obtenido es {respuesta}"
    time.sleep(4)