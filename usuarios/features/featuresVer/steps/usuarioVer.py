from behave import when, then, given
from selenium import webdriver
import time


@given(u'que ingreso al sistema con la dirección "{url}",')
def step_impl(context, url):
    context.url = url
    time.sleep(2)


@given(u'me dirijo a la página principal "{pagina}"')
def step_impl(context, pagina):
    context.driver = webdriver.Chrome()
    context.driver.get(f"{context.url}{pagina}")
    time.sleep(2)


@given(u'me dirijo a la página de inicio de sesión "{link}"')
def step_impl(context, link):
    context.driver.find_element_by_link_text(link).click()
    time.sleep(4)


@given(u'capturo el usuario "{usuario}" y la contraseña "{contra}"')
def step_impl(context, usuario, contra):
    context.driver.find_element_by_name('username').send_keys(usuario)
    context.driver.find_element_by_name('password').send_keys(contra)
    time.sleep(2)


@when(u'hago click en el boton "Acceder"')
def step_impl(context):
    context.driver.find_element_by_xpath("/html/body/div[2]/form/div[3]/input").click()
    time.sleep(4)

@given(u'hago click en el boton "Acceder"')
def step_impl(context):
    context.driver.find_element_by_xpath("/html/body/div[2]/form/div[3]/input").click()
    time.sleep(4)

@when(u'hago click en mi nombre de usuario "{usuario}"')
def step_impl(context, usuario):
    context.driver.find_element_by_link_text(usuario).click()
    time.sleep(4)


@then(u'se pueden ver "{username}"')
def step_impl(context, username):
    respuesta = context.driver.find_element_by_xpath('/html/body/main/div/div/div[2]').text
    assert username in respuesta, f"esperado es {username} y obtenido es {respuesta}"
    time.sleep(4)


@then(u'no se pueden ver "{username}"')
def step_impl(context, username):
    respuesta = context.driver.find_element_by_xpath('/html/body/div/nav').text
    assert username != respuesta, f"esperado es {username} y obtenido es {respuesta}"
    time.sleep(2)