Característica: Como usuario del sistema
                quiero ver la información 
                con la que me registré

    Escenario: Datos de información de usuario correctos
        Dado que ingreso al sistema con la dirección " http://127.0.0.1:8000/",
        Y me dirijo a la página principal "vinculacion"
        Y me dirijo a la página de inicio de sesión "INICIAR SESIÓN"
        Y capturo el usuario "34101010" y la contraseña "contra12345"
        Y hago click en el boton "Acceder"
        Cuando hago click en mi nombre de usuario "34101010"
        Entonces se pueden ver "34101010"
    
    Escenario: Inicio de sesión con datos incorrectos
        Dado que ingreso al sistema con la dirección " http://127.0.0.1:8000/",
        Y me dirijo a la página principal "vinculacion"
        Y me dirijo a la página de inicio de sesión "INICIAR SESIÓN"
        Y capturo el usuario "34101010" y la contraseña "contra12345"
        Cuando hago click en el boton "Acceder"
        Entonces no se pueden ver "34101010"