Característica: Como empleador de un estudiante egresado
                deseo crear una cuenta
                para registrarme en el sistema de Vinculación UAZ.

        Escenario: Creando cuenta con mis datos
                  Dado que ingreso al sistema con la dirección de "http://127.0.0.1:8000/",
                  Y me dirijo a la página principal "vinculacion"
                  Y presiono el boton de Iniciar Sesión "INICIAR SESIÓN"
                  Y aprieto el botón de "Registrate"
                  Y presiono el botón de "Empresa/Organización Empleadora"
                  Y capturo el usuario "usuario1" y el nombre de la empresa "Empresa Usuario" con el correo "user@empresa1.com" con la contraseña "user1234" confirmandola "user1234"
                  Cuando presiono el botón "Agregar"
                  Entonces accedo con las credenciales "usuario1" y contraseña "user1234"

        Escenario: Se quiere acceder con una cuenta creada con contraseña incorrecta
                  Dado que ingreso al sistema con la dirección de "http://127.0.0.1:8000/",
                  Y me dirijo a la página principal "vinculacion"
                  Y presiono el boton de Iniciar Sesión "INICIAR SESIÓN"
                  Y aprieto el botón de "Registrate"
                  Y presiono el botón de "Empresa/Organización Empleadora"
                  Y capturo el usuario "empusuario1" y el nombre de la empresa "EmpleadorDos" con el correo "user@empresa2.com" con la contraseña "user1234" confirmandola "user1234"
                  Cuando presiono el botón "Agregar"
                  Entonces accedo con las credenciales "empusuario1" y contraseña "usuario1234"

        Escenario: Creando una cuenta para recien egresado
                  Dado que ingreso al sistema con la dirección de "http://127.0.0.1:8000/",
                  Y me dirijo a la página principal "vinculacion"
                  Y presiono el boton de Iniciar Sesión "INICIAR SESIÓN"
                  Y aprieto el botón de "Registrate"
                  Y entro con el botón para recien egresado en "Alumno de Licenciatura"
                  Y capturo los datos de matrícula "30101010" con los nombres "Charles" y apellidos "Correa Lopez" con el correo "charlcorrea@gmail.com" la contraseña "contra12345" y su confirmacion "contra12345"
                  Y agrego al usuario con el botón "Agregar"
                  Cuando inicio sesión con las credenciales de usuario "30101010" y la contraseña "contra12345" presionando "Acceder"
                  Entonces entro a la página y leo la información.

        Escenario: Creando una cuenta para alumno recien egresado de posgrado
                  Dado que ingreso al sistema con la dirección de "http://127.0.0.1:8000/",
                  Y me dirijo a la página principal "vinculacion"
                  Y presiono el boton de Iniciar Sesión "INICIAR SESIÓN"
                  Y aprieto el botón de "Registrate"
                  Y entro con el botón para recienE en posgrado en "Alumno de Posgrado"
                  Y capturo los datos de matrícula "31101010" con los nombres "Max" y apellidos "Verstapen" con el correo "cuchao@gmail.com" la contraseña "contra12345" y su confirmacion "contra12345"
                  Y agrego al usuario con el botón "Agregar"
                  Cuando inicio sesión con las credenciales de usuario "30101010" y la contraseña "contra12345" presionando "Acceder"
                  Entonces entro a la página y leo la información.

        Escenario: Creando cuenta para ex alumno de una licenciatura
                  Dado que ingreso al sistema con la dirección de "http://127.0.0.1:8000/",
                  Y me dirijo a la página principal "vinculacion"
                  Y presiono el boton de Iniciar Sesión "INICIAR SESIÓN"
                  Y aprieto el botón de "Registrate"
                  Y puedo entrar al boton de ex alumno licenciatura con "Ex Alumno de Licenciatura"
                  Y capturo los datos de matrícula "33101010" con los nombres "Sergio" y apellidos "Perez" con el correo "dnf_porcuchao@gmail.com" la contraseña "contra12345" y su confirmacion "contra12345"
                  Y agrego al usuario con el botón "Agregar"
                  Cuando inicio sesión con las credenciales de usuario "33101010" y la contraseña "contra12345" presionando "Acceder"
                  Entonces entro a la página y leo la información.

        Escenario: Creando cuenta para un ex alumno de posgrado
                  Dado que ingreso al sistema con la dirección de "http://127.0.0.1:8000/",
                  Y me dirijo a la página principal "vinculacion"
                  Y presiono el boton de Iniciar Sesión "INICIAR SESIÓN"
                  Y aprieto el botón de "Registrate"
                  Y puedo entrar al boton de ex alumno posgrado "Ex alumno de posgrado"
                  Y capturo los datos de matrícula "34101010" con los nombres "Luis" y apellidos "Hamilton" con el correo "simpre_cuchao@gmail.com" la contraseña "contra12345" y su confirmacion "contra12345"
                  Y agrego al usuario con el botón "Agregar"
                  Cuando inicio sesión con las credenciales de usuario "33101010" y la contraseña "contra12345" presionando "Acceder"
                  Entonces entro a la página y leo la información.