from behave import given, when, then
from selenium import webdriver
import time

@given(u'que ingreso al sistema con la dirección de "{url}",')
def step_impl(context, url):
    context.url = url
    time.sleep(2)

@given(u'me dirijo a la página principal "{pagina}"')
def step_impl(context, pagina):
    context.driver = webdriver.Chrome()
    context.driver.get(f"{context.url}{pagina}")
    time.sleep(2)

@given(u'presiono el boton de Iniciar Sesión "{boton}"')
def step_impl(context,boton):
    context.driver.find_element_by_link_text(boton).click()
    time.sleep(2)



@given(u'aprieto el botón de "{boton}"')
def step_impl(context,boton):
    context.driver.find_element_by_link_text(boton).click()
    time.sleep(2)


@given(u'presiono el botón de "{boton}"')
def step_impl(context,boton):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(2)
    context.driver.find_element_by_xpath('//*[@id="features"]/div/div/div[3]/a').click()
    time.sleep(2)

@given(u'capturo el usuario "{usuario}" y el nombre de la empresa "{empresa}" con el correo "{correo}" con la contraseña "{contra}" confirmandola "{confirmada}"')
def step_impl(context,usuario,empresa,correo,contra,confirmada):
    context.driver.find_element_by_id('id_username').send_keys(usuario)
    time.sleep(1)
    context.driver.find_element_by_id('id_first_name').send_keys(empresa)
    time.sleep(1)
    context.driver.find_element_by_id('id_email').send_keys(correo)
    time.sleep(1)
    context.driver.find_element_by_id('id_password1').send_keys(contra)
    time.sleep(1)
    context.driver.find_element_by_id('id_password2').send_keys(confirmada)
    time.sleep(3)


@when(u'presiono el botón "{boton}"')
def step_impl(context,boton):
    #context.driver.find_element_by_link_text(boton).click()
    context.driver.find_element_by_id('agregausuario').click()
    time.sleep(4)


@then(u'accedo con las credenciales "{usuario}" y contraseña "{contra}"')
def step_impl(context,usuario,contra):
    context.driver.find_element_by_id('id_username').send_keys(usuario)
    time.sleep(2)
    context.driver.find_element_by_id('id_password').send_keys(contra)
    time.sleep(2)

################# se crea usuario para recien egresado

@given(u'entro con el botón para recien egresado en "{botonalumno}"')
def step_impl(context, botonalumno):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(2)
    context.driver.find_element_by_xpath('//*[@id="features"]/div/div/div[1]/a[1]').click()
    time.sleep(4)

@given(u'capturo los datos de matrícula "{matricula}" con los nombres "{nombres}" y apellidos "{apellidos}" con el correo "{correo}" la contraseña "{contra1}" y su confirmacion "{contra2}"')
def step_impl(context,matricula,nombres,apellidos,correo,contra1,contra2):
    time.sleep(2)
    context.driver.find_element_by_id('id_username').send_keys(matricula)
    time.sleep(1)
    context.driver.find_element_by_id('id_first_name').send_keys(nombres)
    time.sleep(1)
    context.driver.find_element_by_id('id_last_name').send_keys(apellidos)
    time.sleep(1)
    context.driver.find_element_by_id('id_email').send_keys(correo)
    time.sleep(1)
    context.driver.find_element_by_id('id_password1').send_keys(contra1)
    time.sleep(1)
    context.driver.find_element_by_id('id_password2').send_keys(contra2)
    time.sleep(4)

@given(u'agrego al usuario con el botón "{agregar}"')
def step_impl(context,agregar):
    context.driver.find_element_by_xpath('/html/body/div[2]/div/form/div[7]/button').click()
    time.sleep(4)


@when(u'inicio sesión con las credenciales de usuario "{usuario}" y la contraseña "{contra}" presionando "{botonacceder}"')
def step_impl(context,usuario,contra,botonacceder):
    context.driver.find_element_by_id('id_username').send_keys(usuario)
    time.sleep(2)
    context.driver.find_element_by_id('id_password').send_keys(contra)
    time.sleep(3)
    context.driver.find_element_by_xpath('/html/body/div[2]/form/div[3]/input').click()
    time.sleep(3)

@then(u'entro a la página y leo la información.')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(4)

######## Recien egresado para el posgrado
@given(u'entro con el botón para recienE en posgrado en "{botonreposgrado}"')
def step_impl(context, botonreposgrado):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(2)
    context.driver.find_element_by_xpath('//*[@id="features"]/div/div/div[1]/a[2]').click()
    time.sleep(4)

####### Ex alumno de licenciatura
@given(u'puedo entrar al boton de ex alumno licenciatura con "Ex Alumno de Licenciatura"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(2)
    context.driver.find_element_by_id('exalumnoboton').click()
    time.sleep(4)

###### Ex alumno de posgrado
@given(u'puedo entrar al boton de ex alumno posgrado "Ex alumno de posgrado"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(2)
    context.driver.find_element_by_id('exalumnoposgradoboton').click()
    time.sleep(4)
