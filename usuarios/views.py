from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View

from .forms import UsuarioForm, UsuarioEmpleador, UserProfileChangeForm
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView
from django.contrib.auth.views import LoginView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib import messages
from .decorators import unauthenticated_user
from django.utils.decorators import method_decorator
from django.views.generic import ListView,DetailView
from django.shortcuts import render, redirect
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required


### Funcion que permite registrar un empleador, desde la forma UsuarioEmpleador(), agregándolo al grupo
##  'empleador'.
@unauthenticated_user
def registroEmpleador(request):
    form = UsuarioEmpleador()
    if request.method == 'POST':
        form = UsuarioEmpleador(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')

            group = Group.objects.get(name='empleador')
            user.groups.add(group)

            messages.success(request, 'Account was created for ' + username)

            return redirect('eduacionapp:login')

    context = {'form': form}
    return render(request, 'registro_empleadores.html', context)


### Funcion que permite registrar un usuario alumno con años de egreso, desde la forma UsuarioForm(),
### agregándolo al grupo 'alumno'.
@unauthenticated_user
def registroAlumno(request, grupo):
    form = UsuarioForm()
    if request.method == 'POST':
        form = UsuarioForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name=grupo)
            user.groups.add(group)

            messages.success(request, 'Account was created for ' + username)

            return redirect('eduacionapp:login')

    context = {'form': form, 'grupo': grupo}
    return render(request, 'registro_alumnos.html', context)

### Funcion que permite registrar un usuario alumno ca punto de egresar de licenciatura, desde la forma UsuarioForm(),
### agregándolo al grupo 'alumno_rec_egresado'.
@unauthenticated_user
def registroAlumnoRecEgresado(request):
    form = UsuarioForm()
    if request.method == 'POST':
        form = UsuarioForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')

            group = Group.objects.get(name='alumno_rec_egresado')
            user.groups.add(group)

            messages.success(request, 'Account was created for ' + username)

            return redirect('eduacionapp:login')

    context = {'form': form}
    return render(request, 'registro_alumnos.html', context)

### Funcion que permite registrar un usuario alumno egresado de posgrado , desde la forma UsuarioForm(),
### agregándolo al grupo 'posgrado_alumno'.
@unauthenticated_user
def registroAlumnoPosgradoRecienEgresado(request):
    form = UsuarioForm()
    if request.method == 'POST':
        form = UsuarioForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')

            group = Group.objects.get(name='posgrado_alumnoRecienE')
            user.groups.add(group)

            messages.success(request, 'Account was created for ' + username)

            return redirect('eduacionapp:login')

    context = {'form': form}
    return render(request, 'registro_alumnos.html', context)

### Funcion que permite registrar un usuario alumno egresado de posgrado , desde la forma UsuarioForm(),
### agregándolo al grupo 'posgrado_alumno'.
@unauthenticated_user
def registroAlumnoPosgradoEgresado(request):
    form = UsuarioForm()
    if request.method == 'POST':
        form = UsuarioForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')

            group = Group.objects.get(name='posgrado_alumno')
            user.groups.add(group)

            messages.success(request, 'Account was created for ' + username)

            return redirect('eduacionapp:login')

    context = {'form': form}
    return render(request, 'registro_alumnos.html', context)

#Esta clase se utiliza para iniciar sesion dentro de la plataforma web
@method_decorator(unauthenticated_user, name='dispatch')
class Login(LoginView):
    template_name = 'login.html'
    form_class = AuthenticationForm
    success_url = reverse_lazy('eduacionapp:admin')

#Esta clase es para organizar el contenido base de la plataforma web
class Home (TemplateView):
    template_name = 'home.html'
    #a ver este comentario es prueba
    success_url = reverse_lazy('eduacionapp:admin')
    usuarios = User.objects.all()
    extra_context = ({'usuarios': usuarios})

    def lista(request):
        usuarios = User.objects.all()
        return render(request, 'home.html', {'usuarios': usuarios})
##Esta clase es para organizar el contenido base de la plataforma web
#
#class Home (TemplateView):
#
#    def home(request):
#        if request.user.is_staff:
#            print("si es staff")
#            plantilla = 'usuarios/home_admin.html'
#            return render(request, plantilla)
#        else:                                                         |
#            print("no es staff")
#            template_name = 'home.html'
#            return render(request, template_name)
#
#        # a ver este comentario es prueba
#        # success_url = reverse_lazy('eduacionapp:admin')
#        # usuarios = User.objects.all()
#        # extra_context = ({'usuarios': usuarios})
#
#    def lista(request):
#        usuarios = User.objects.all()
#        return render(request, 'home.html', {'usuarios': usuarios})
#
##Esta clase es para organizar el contenido base de la plataforma web
#@login_required()
#def homeeee (request):
#    if request.user.is_staff:
#        print("si es staff")
#        plantilla = 'usuarios/home_admin.html'
#        return render(request, plantilla)
#    else:
#        print("no es staff")
#        template_name = 'home.html'
#        return render(request,template_name)
#
#    #a ver este comentario es prueba
#    #success_url = reverse_lazy('eduacionapp:admin')
#    #usuarios = User.objects.all()
#    #extra_context = ({'usuarios': usuarios})
#
class ListaUsuario(ListView):
    model = User

def cuestionario(request):
    plantilla = 'usuarios/cuestionario.html'
    return render(request, plantilla)


class Busqueda(TemplateView):
    template_name = 'barrabusqueda.html'
    success_url = reverse_lazy('eduacionapp:admin')
    usuarios = User.objects.all()
    extra_context = ({'usuarios': usuarios})

class RegistroTipoUsuario(TemplateView):
    template_name = 'registrotipousuario.html'
    success_url = reverse_lazy('eduacionapp:login')
    usuarios = User.objects.all()
    extra_context = ({'usuarios': usuarios})

class UsuarioEmDetalle(TemplateView):
    template_name = 'usuarios/usuarioEmp_detail.html'
    model = User
    extra_context = {'etiquetatipousuario': 'empresa'}

class UsuarioAlumnoDetalle(TemplateView):
    template_name = 'usuarios/usuarioEmp_detail.html'
    model = User
    extra_context = {'etiquetatipousuario': 'alumno'}

class UsuarioEmEditar(UpdateView):
    template_name = 'usuarios/usuarioEmp_form.html'
    model = User
    fields = ['first_name','last_name', 'email']
    success_url = reverse_lazy('eduacionapp:home')

class UsuarioEmEliminar(DeleteView):
    template_name = 'usuarios/user_confirm_delete.html'
    model = User
    success_url = reverse_lazy('eduacionapp:login')