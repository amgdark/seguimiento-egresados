from django.http import response
from django.test import TestCase, client
from django.contrib.auth.models import User


def test_crear_usuario(self):
    User.objects.create(
        username='usuarioCreado',
        last_name='Creado',
        email='usuariocred@gmail.com'
    )

    usuarios = User.objects.get(id=20)
    self.assertEqual(usuarios.username, 'usuarioCreado')