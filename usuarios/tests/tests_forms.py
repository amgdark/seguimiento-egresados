from django.contrib.auth.forms import UserCreationForm
from django.http import response
from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.auth.forms import (UserCreationForm,AuthenticationForm,UserChangeForm)

class TestSmoke(TestCase):
    def test_smoke_test(self):
        self.assertEqual(2, 1+1)

class TestCreacionUsuario(TestCase):

    def test_usuario_existe(self):
        data = {
            'username': 'usuarioNo',
            'password1': 'usuario12345',
            'password2': 'usuario12345',
        }
        form = UserCreationForm(data)
        self.assertTrue(form.is_valid())

    def test_datos_invalidos(self):
        data = {
            'username': '12345',
            'password1': 'user12345',
            'password2': 'user12345',
        }
        form = UserCreationForm(data)
        self.assertFalse(form.is_valid())
        validacion = next(v for v in User._meta.get_field('username').validators if v.code == 'invalid')
        self.assertEqual(form["username"].errors, [str(validacion.message)])

    def test_contra_no_correcta(self):
        data = {
            'username': 'usuarioNo',
            'password1': 'usuario12345',
            'password2': 'usurio12345',
        }
        form = UserCreationForm(data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form["password2"].errors,
        [str(form.error_messages['password_mismatch'])])

    def validacion_contra(self):
        data = {
        'username': 'usuarioNo',
        'password1': 'usuarioNo',
        'password2': 'usuarioNo',
        }
        form = UserCreationForm(data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form['password2'].errors), 2)
        self.assertIn('La contraseña es muy parecida al usuario.', form['password2'].errors)
        self.assertIn( 'La contraseña debe tener mínimo 8 caracteres.', form['password2'].errors)


    class UserCreationFormTest(TestCase):
        def test_user_already_exists(self):
            data = {
                'username': 'Den',
                'password1': '30112717d',
                'password2': '30112717d',
            }
            form = UserCreationForm(data)
            self.assertTrue(form.is_valid())

        def test_invalid_username(self):
            data = {
                'username': 'Den',
                'password': '30112717d',
            }
            form = AuthenticationForm(None, data)
            self.assertFalse(form.is_valid())

        def test_UserChangeForm(self):
            class MyUserForm(UserChangeForm):
                def __init__(self, *args, **kwargs):
                    super().__init__(*args, **kwargs)
                    self.fields['groups'].help_text = 'These groups give users different permissions'

                class Meta(UserChangeForm.Meta):
                    fields = ('groups',)

            MyUserForm({})