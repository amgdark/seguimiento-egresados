from django.http import response
from django.test import TestCase, client
from django.contrib.auth.models import User
from django.urls.base import reverse


class TestViews(TestCase):
    def test_url_vinculacion(self):
        response = self.client.get('/vinculacion/')
        self.assertEqual(response.status_code, 200)


    def test_template_vinculacion(self):
        response = self.client.get('/vinculacion/')
        self.assertTemplateUsed(response, 'home.html')


    def test_url_login(self):
        response = self.client.get('/vinculacion/login/')
        self.assertEqual(response.status_code, 200)


    def test_template_login(self):
        response = self.client.get('/vinculacion/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_url_registro(self):
        response = self.client.get('/vinculacion/registro/')
        self.assertEqual(response.status_code,200)

    def test_template_registro(self):
        response = self.client.get('/vinculacion/registro/')
        self.assertTemplateUsed(response, 'registrotipousuario.html')

    def test_url_registro_empleador(self):
        response = self.client.get('/vinculacion/registro-empleador/')
        self.assertEqual(response.status_code,200)

    def test_template_registro_empleador(self):
        response = self.client.get('/vinculacion/registro-empleador/')
        self.assertTemplateUsed(response, 'registro_empleadores.html')

    def test_url_ver_empleador(self):
        usuario = User.objects.create(
            username='usuarioCreado',
            last_name='Creado',
            email='usuariocred@gmail.com')
        response = self.client.get('eduacionapp:ver', args={'pk': usuario.id}),
        {'username': 'usuarioCreado', 'last_name': 'Creado', 'email': 'usuariocred@gmail.com'}
        self.assertEqual(response.status_code, 200)

    def test_url_eliminar_empleador(self):
        usuario = User.objects.create(
            username='usuarioCreado',
            last_name='Creado',
            email='usuariocred@gmail.com')
        response = self.client.get('eduacionapp:ver', args={'pk': usuario.id}),
        {'username': 'usuarioCreado', 'last_name': 'Creado', 'email': 'usuariocred@gmail.com'}
        self.assertEqual(response.status_code, 200)

    def test_url_vinculación(self):
        response = self.client.get('/vinculacion/')
        self.assertEqual(response.status_code, 200)

    def test_template_vinvulacion(self):
        response = self.client.get('/vinculacion/')
        self.assertTemplateUsed(response, 'home.html')

    def test_url_login(self):
        response = self.client.get('/vinculacion/login/')
        self.assertEqual(response.status_code, 200)

    def test_template_login(self):
        response = self.client.get('/vinculacion/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_view_user(self):
        usuario = User.objects.create(
            username='den',
            last_name='gigante',
            email='cdndjfnsdj@jmj')
        response = self.client.post(
            reverse('eduacionapp:ver', kwargs={'pk': usuario.id}),
            {'username': 'den', 'last_name': 'gigante', 'email': 'cdndjfnsdj@jmj'})

        self.assertEqual(response.status_code, 405)
        usuario.refresh_from_db()
        self.assertEqual(usuario.username, 'den')

    def test_update_user(self):
        usuario = User.objects.create(
            username='den',
            last_name='gigante',
            email='cdndjfnsdj@jmj')
        response = self.client.post(

            reverse('eduacionapp:editar', kwargs={'pk': usuario.id}),
            {'username': 'den', 'last_name': 'gigante', 'email': 'cdndjfnsdj@jmj'})

        self.assertEqual(response.status_code, 200)

        usuario.refresh_from_db()
        self.assertEqual(usuario.username, 'den')

