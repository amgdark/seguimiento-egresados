from django.db import models
from django.conf import settings
from django.db.models.enums import Choices
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib.auth.models import User



class Alumno(models.Model):
    OPCIONGENERO = [
        ('1', 'Femenino'),
        ('2', 'Masculino'),
    ]
    ESTADOCIVIL = [
        ('1', 'Casada(o)'),
        ('2', 'Soltera(o)'),
        ('3', 'Vida en pareja'),
        ('4', 'Divorciada(o)'),
    ]
    GRADOESTUDIOS = [
        ('1', 'Sin estudios'),
        ('2', 'Primaria'),
        ('3', 'Secundaria'),
        ('4', 'Preparatoria'),
        ('5', 'Carrera técnica'),
        ('6', 'Licenciatura'),
        ('7', 'Maestría'),
        ('8', 'Doctorado'),
    ]
    NACIONALIDAD = [
        ('1', 'Mexicana'),
        ('2', 'Otro'),
    ]
    
    # nombre = models.CharField('Nombres:', max_length=200)
    # apellidoP = models.CharField('Apellido Paterno:', max_length=200)
    apellidoM = models.CharField('Apellido Materno:', max_length=200)
    nacimiento = models.CharField('Lugar de nacimiento', max_length=200)
    domicilio = models.CharField('Domicilio fijo vigente', max_length=300)
    # correo = models.CharField('Correo Electrónico:', max_length=100)
    facebook = models.CharField('Facebook:', max_length=100)
    telefono = models.CharField('Teléfono:', max_length=10)
    genero = models.CharField('Género:', max_length=1, choices=OPCIONGENERO)
    edad = models.PositiveSmallIntegerField()
    estadoCivil = models.CharField('Estado Civil:', max_length=1, choices=ESTADOCIVIL)
    Nacionalidad = models.CharField('Nacionalidad:', max_length=10, choices=NACIONALIDAD)
    gradoEstudiosP = models.CharField('Máximo grado de estudios del padre o tutor:', max_length=1,
                                      choices=GRADOESTUDIOS)
    gradoEstudiosM = models.CharField('Máximo grado de estudios de la madre o tutora:', max_length=1,
                                      choices=GRADOESTUDIOS)
    user = models.OneToOneField(User, verbose_name="usuario", on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.user.first_name
    # def ValidarUsuario(request):
    #     if request.user.is_authenticated:
    #         if request.user.groups.name == "alumno_rec_egresado":
    #             return reverse('encuestas:recienegresados')




    # def get_absolute_url(self, request):
    #     queryset = super().get_queryset(request)
    #     # Iteramos todos los grupos del usuario
    #     for group in request.user.groups.all():
    #         # Si el nombre del grupo es tal, pues retornamos un queryset deacuerdo
    #         # a dicho grupo
    #         if group.name == 'alumno_rec_egresado':
    #             return reverse('encuestas:recienegresados')
    #     return queryset


# GRADOESTUDIOSPAPA=[
#     ('1', ''),
#     ('2', ''),
#     ('3', ''),
#     ('4', ''),
#     ('5', ''),
#     ('6', ''),
#     ('7', ''),
#     ('8', ''),
# ]



PREGUNTA1 = [
    ('1', 'Superación personal'),
    ('2', 'Obtener un mejor empleo'),
    ('3', 'Tradición de familia'),
    ('4', 'Falta de trabajo'),
    ('5', 'Otra (indique)'),
]

PREGUNTA2 = [
    ('1', 'Por el prestigio de la institución'),
    ('2', 'Por la importancia de la licenciatura'),
    ('3', 'Por la ubicación de la Unidad Académica'),
    ('4', 'Por el costo'),
    ('5', 'Porque es la única institución que oferta la licenciatura'),
    ('6', 'Porque no podía elegir otra opción'),
    ('7', 'Otra (indique)'),
]

PREGUNTA3 = [
    ('1', 'Por Información general de la UAZ'),
    ('2', 'Por Expo-orienta'),
    ('3', 'Por búsqueda en Internet'),
    ('4', 'Por referencia de un egresado de la licenciatura'),
    ('5', 'Por referencia de amigos'),
    ('6', 'Por referencia de familiares'),
    ('7', 'Otra (indique)'),
]

RESPUESTASINO = [
    ('1', 'Si'),
    ('2', 'No'),
]

PREGUNTA5 = [
    ('1', 'Desempeño aún no satisfactorio'),
    ('2', 'Desempeño satisfactorio'),
    ('3', 'Desempeño sobresaliente'),
]

PREGUNTA7 = [
    ('1', 'Por tesis'),
    ('2', 'Por promedio general'),
    ('3', 'Por Examen General de Egreso (EGEL)'),
    ('4', 'Por curso de titulación'),
    ('5', 'Por prácticas profesionales'),
]

PREGUNTA8 = [
    ('1', 'Titulación en proceso'),
    ('2', 'Falta de tiempo'),
    ('3', 'Falta de apoyo teórico-metodológico'),
]

PREGUNTA9 = [
    ('1', 'Menos de 6 meses'),
    ('2', 'De 6 a 12 meses'),
    ('3', 'No sabe'),
]
PREGUNTA_SATISFACCION = [
    ('1', 'Muy satisfecho'),
    ('2', 'Satisfecho'),
    ('3', 'Insatisfecho'),
    ('4', 'Muy insatisfecho'),
]

PREGUNTA15 = [
    ('1', 'Beca de comedor (UAZ)'),
    ('2', 'Beca de libros (UAZ)'),
    ('3', 'Beca de vivienda (UAZ)'),
    ('4', 'Beca de gobierno estatal'),
    ('5', 'Otro tipo de beca (especifique)'),
]
PREGUNTA_SATISFACCION2 = [
    ('1', 'Excelente'),
    ('2', 'Buena'),
    ('3', 'Regular'),
    ('4', 'Mala'),
]

PREGUNTA17 = [
    ('1', 'Debe permanecer sin cambios'),
    ('2', 'Deben realizarse cambios menores'),
    ('3', 'Debe realizarse una reestructuración total '),
    ('4', 'Debe desaparecer (es obsoleta)'),
]

PREGUNTA18 = [
    ('1', 'Muy importante'),
    ('2', 'Importante'),
    ('3', 'Poco importante'),
    ('4', 'Nada importante'),
]

PREGUNTA21 = [
    ('1', 'Titularse'),
    ('2', 'Buscar empleo en el campo profesional de la carrera'),
    ('3', 'Iniciar estudios de posgrado en el país'),
    ('4', 'Iniciar estudios de posgrado en el extranjero'),
    ('5', 'Iniciar estudios de otra licenciatura'),
]
PREGUNTATITULACION = [
    ('1', 'Por tesis'),
    ('2', 'Por promedio general'),
    ('3', 'Por curso de titulación'),
    ('4', 'Por experiencia profesional'),
    ('5', 'Por investigación'),
    ('6', 'Por Examen General de Egreso (EGEL)'),
    ('7', 'Por prácticas profesionales'),
    ('8', 'Por plan de estudios basado en competencia (Promedio mínimo 8.0)'),
    ('9', 'Por estudios de maestría'),
]
PREGUNTAE8 = [
    ('1', 'Por vocación'),
    ('2', 'Por el prestigio de la institución'),
    ('3', 'Por la importancia de la licenciatura'),
    ('4', 'Por el costo'),
    ('5', 'Porque es la única institución que oferta la licenciatura'),
    ('6', 'Porque no podía elegir otra opción'),
    ('7', 'Otra (indique)'),
]
PREGUNTAE15 = [
    ('1', 'Padres o tutores'),
    ('2', 'Hermano (a)'),
    ('3', 'Familiar'),
    ('4', 'Financiamiento Propio'),

]
PREGUNTAE17 = [
    ('1', 'Beca de la UAZ (para libros,inscripcion,vivienda,transporte,comedor)'),
    ('2', 'Beca de Gobierno Federal'),
    ('3', 'Beca de Gobierno Estatal'),
    ('4', 'Otro tipo de beca (especifique)'),
]

PREGUNTAEMPLEO=[
    ('1', 'Menos de 3 meses'),
    ('2', 'De 3 a 6 meses'),
    ('3', 'De 6  a 9 meses'),
    ('4', 'De 9 a 12 meses'),
    ('5', 'Más de 1 año'),
    ('6', 'Aún no me incorporo al campo laboral'),
]

SITUACIONLABORAL=[
    ('1', 'Empleada(o)'),
    ('2', 'Consultor externo (trabajo independiente)'),
    ('3', 'Desempleada(o)'),
]

SITUACIONLABORAL2=[
    ('1', 'No encuentra el trabajo apropiado en su profesión'),
    ('2', 'No recibe respuesta de empleadores'),
    ('3', 'No tiene el título de la profesión'),
    ('4', 'Carece de las competencias necesarias'),
    ('5', 'No encuentra ofertas de trabajo'),
    ('6', 'No desea salir del estado de Zacatecas'),
    ('7', 'El sueldo ofertado es muy bajo '),
    ('8', 'No domina un segundo idioma'),
    ('9', 'Situación personal/familiar'),
    ('10', 'Estoy estudiando actualmente un posgrado'),
    ('11', 'Otro (indique)'),

]

PREGUNTAE31=[
    ('1', 'Servicios'),
    ('2', 'Industria'),
    ('3', 'Comercio'),

]

PREGUNTAE32=[
    ('1', 'Público/gobierno'),
    ('2', 'Público/educación'),
    ('3', 'Privado/empresa'),
    ('4', 'Privado/educación'),
    ('5', 'Asociación civil/Organismo no Gubernamental (ONG)'),
    ('6', 'Autoempleo (trabajo independiente)'),
]

PREGUNTAE33=[
    ('1', 'Hasta 10 empleado (micro)'),
    ('2', 'Entre 11 y 50 empleados (pequeña)'),
    ('3', 'Entre 51 y 250 empleados (mediana)'),
    ('4', 'Más de 251 empleados (grande) '),
]

PREGUNTAE34=[
    ('1', 'Contrato temporal'),
    ('2', 'Contrato por tiempo indefinido'),
    ('3', 'Contrato por honorarios'),
    ('4', 'Otro (indique)'),
]

PREGUNTAE35=[
    ('1', 'Bolsa de trabajo de la UAZ'),
    ('2', 'Bolsa de trabajo de la empresa/organización'),
    ('3', 'Realización de prácticas profesionales en la empresa/organización'),
    ('4', 'Recomendación de un docente'),
    ('5', 'Recomendación de un familiar'),
    ('6', 'Sitio web'),
    ('7', 'Periódico'),
]

PREGUNTAE37=[
    ('1', 'Directivo (alto)'),
    ('2', 'Ejecutivo o gerencial (medio)'),
    ('3', 'Operativo o administrativo (bajo)'),
]
PREGUNTAE38=[
    ('1', '$5,000 a $8,000'),
    ('2', '$8,001 a $12,000'),
    ('3', '$12,001 a $15,000'),
    ('4', '$15,001 a $18,000'),
    ('5', '$18,001 a $20,000'),
    ('6', '$20,001 a $22,000'),
    ('7', '$22,001 a $25,000'),
    ('8', '$25,001 a $28,000'),
    ('9', '$28,001 a $30,000'),
    ('10', '$30,001 a $35,000'),
    ('11', '$35,001 a $40,000'),
    ('11', '$35,001 a $40,000'),


]
DEACUERDO=[
    ('1', 'Totalmente de acuerdo'),
    ('2', 'De acuerdo'),
    ('3', 'En desacuerdo'),
    ('4', 'Totalmente en desacuerdo'),
]

POCONADA=[
    ('1', 'Mucho'),
    ('2', 'Poco'),
    ('3', 'Nada'),
]

NIVELPOSGRADO=[
    ('1', 'Especialidad'),
    ('2', 'Maestría'),
    ('3', 'Doctorado'),

]

PREGUNTA1P=[
    ('1', 'Interés personal'),
    ('2', 'Mejorar en el ámbito laboral'),
    ('3', 'Mejorar la trayectoria profesional'),
    ('4', 'Falta de trabajo'),
    ('5', 'Otra')
]

PREGUNTA2P=[
    ('1', 'Por el prestigio de la institución'),
    ('2', 'Por la importancia del posgrado cursado'),
    ('3', 'Por la ubicación de la Unidad Académica'),
    ('4', 'Por el sentido de pertenencia'),
    ('5', 'Porque es la única institución que oferta el posgrado cursado'),
    ('6', 'Por su plan de estudios'),
    ('7', 'Por la beca que ofrece'),
]

PREGUNTA3P=[
    ('1', 'Por Información general de la UAZ'),
    ('2', 'Por Expo-orienta'),
    ('3', 'Por búsqueda en Internet'),
    ('4', 'Por referencia de un egresado del posgrado'),
    ('5', 'Por referencia de amigos'),
    ('6', 'Por referencia de familiares'),
    ('7', 'Otra (indique)'),
]

PREGUNTABECA = [
    ('1', 'Beca de comedor (UAZ)'),
    ('2', 'Beca de libros (UAZ)'),
    ('3', 'Beca de vivienda (UAZ)'),
    ('4', 'Beca CONACyT'),
    ('5', 'Beca de gobierno estatal'),
    ('6', 'Otro tipo de beca (especifique)'),
]

ORGANIZACION=[
    ('1', 'Colegio de profesionistas'),
    ('2', 'Red de profesionistas'),
    ('3', 'ONG'),

]


NIVELSNI=[
    ('1', 'Nivel Candidato'),
    ('2', 'Nivel I'),
    ('3', 'Nivel II'),
    ('4', 'Nivel III'),
]

EMPRESAORG=[
    ('1', 'Sector público/gobierno'),
    ('2', 'Sector Público/educación'),
    ('3', 'Sector Privado/empresa'),
    ('4', 'Sector Privado/educación'),
    ('5', 'Asociación civil/Organismo No Gubernamental (ONG)'),

]

SERVICIOSEMP=[
    ('1', 'Servicios'),
    ('2', 'Industria'),
    ('3', 'Comercio'),
]
TAMAÑOEMP=[
    ('1', 'Hasta 10 empleado (micro)'),
    ('2', 'Entre 11 y 50 empleados (pequeña)'),
    ('3', 'Entre 51 y 250 empleados (mediana)'),
    ('4', 'Más de 251 empleados (grande)'),
]

TIPOEMPRESA=[
    ('1', 'Empresa mexicana'),
    ('2', 'Empresa extranjera establecida en México'),
]

RECLUTAMIENTO=[
    ('1', 'Bolsa de trabajo de la universidad'),
    ('2', 'Bolsa de trabajo de la empresa/organización'),
    ('3', 'Redes sociales '),
    ('4', 'Periódico'),
    ('5', 'Recomendación'),
    ('6', 'Otro (indique)'),
]
PERSONASQUELABORAN=[
    ('1', 'Más de 15 personas'),
    ('2', 'De 10 a 15 personas'),
    ('3', 'De 5 a 9 personas'),
    ('4', '4 personas'),
    ('5', '3 personas'),
    ('6', '2 personas'),
    ('7', '1 persona'),
    ('8', 'Ninguna'),
]
CONTRATO=[
    ('1', 'Contrato temporal'),
    ('2', 'Contrato por tiempo indefinido'),
    ('3', 'Contrato por honorarios'),
    ('4', 'Otro (indique)'),
]
PUESTO=[
    ('1', 'Puesto promedio'),
    ('2', 'Director'),
    ('3', 'Gerente'),
    ('4', 'Jefe'),
    ('5', 'Supervisor'),
    ('6', 'Otro (espefifique)'),
]
PREGUNTA_SATISFACCION3 = [
    ('1', 'Excelente'),
    ('2', 'Buena'),
    ('3', 'Suficiente'),
    ('4', 'Regular'),
    ('5', 'Mala'),
]

POCONADA2=[
    ('1', 'Mucho'),
    ('2', 'Algo'),
    ('3', 'Poco'),
    ('4', 'Nada'),
]
DEACUERDO2=[
    ('1', 'Totalmente en desacuerdo'),
    ('2', 'En desacuerdo'),
    ('3', 'Parcialmente de acuerdo'),
    ('4', 'De acuerdo'),
    ('5', 'Totalmente de acuerdo'),
]
PROBABILIDAD=[
    ('1', 'Muy probable'),
    ('2', 'Probable'),
    ('3', 'Poco probale '),
    ('4', 'Nada probable'),
]
COMPETITIVIDAD=[
    ('1', 'Muy competido'),
    ('2', 'Competido'),
    ('3', 'Poco competido'),
    ('4', 'Nada competido'),
]
CAMBIOS=[
    ('1', 'Ningun cambio'),
    ('2', 'Cambios a corto plazo'),
    ('3', 'Cambios a largo plazo'),
]
# GRADOESTUDIOSPAPA=[
#     ('1', ''),
#     ('2', ''),
#     ('3', ''),
#     ('4', ''),
#     ('5', ''),
#     ('6', ''),
#     ('7', ''),
#     ('8', ''),
# ]

class PreguntasRecienEgresados(models.Model):
    alumno = models.ForeignKey(Alumno, on_delete=models.CASCADE)
    licenciatura = models.CharField('Nombre de la licenciatura cursada:', max_length=300)
    unidadAcademica = models.CharField('Nombre de la Unidad Académica a la que pertenece la licenciatura cursada:',
                                       max_length=300)
    areac = models.CharField('Nombre del Área del conocimiento a la que pertenece la Unidad Académica:', max_length=300)
    fechaIngreso = models.DateTimeField('Fecha de ingreso a la licenciatura')
    fechaEgreso = models.CharField('Fecha de egreso de la licenciatura', max_length=300)
    pregunta1 = models.CharField('Razón principal por la que decidió estudiar una licenciatura', max_length=1,
                                 choices=PREGUNTA1)
    pregunta2 = models.CharField('Razón principal por la que decidió estudiar la licenciatura cursada', max_length=1,
                                 choices=PREGUNTA2)
    pregunta3 = models.CharField('Indique el medio por el que se enteró de la licenciatura cursada', max_length=1,
                                 choices=PREGUNTA3)
    pregunta4 = models.CharField('¿Presentó el Examen General de Egreso (EGEL)?', max_length=1, choices=RESPUESTASINO)
    # Solo si se presentó el examen se pasa a la 5
    pregunta5 = models.CharField('Si presentó el Examen General de Egreso (EGEL) ¿cuál fue el resultado obtenido?',
                                 max_length=1, choices=PREGUNTA5)
    pregunta6 = models.CharField('¿Se tituló de la licenciatura?', max_length=1, choices=RESPUESTASINO)
    # Solo si se titulo
    pregunta7 = models.CharField('Si se ha titulado, ¿cuál fue la opción de titulación que seleccionó?', max_length=1,
                                 choices=PREGUNTA7)
    # si no se titulo
    pregunta8 = models.CharField('En caso de no haberse titulado aún, indique el motivo principal', max_length=1,
                                 choices=PREGUNTA8)
    pregunta9 = models.CharField('En caso de no haberse titulado aún, ¿en cuánto tiempo planea titularse?',
                                 max_length=1, choices=PREGUNTA9)

    indicacion = (
        "Indique el grado de satisfacción general sobre los docentes de la licenciatura cursada, en los siguientes aspectos:")
    pregunta10 = models.CharField(
        'Indique el grado de satisfacción general sobre los docentes de la licenciatura cursada, en los siguientes aspectos:' +
        'Atención y asesoría fuera de clase ', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta10_1 = models.CharField('Formación académica', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta10_2 = models.CharField('Preparación de clases', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta10_3 = models.CharField('Procesos de aprendizaje (metodología, ayudas utilizadas)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta10_4 = models.CharField('Puntualidad y asistencia', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta10_5 = models.CharField('Relación adecuada con los estudiantes', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta10_6 = models.CharField('Trabajo de campo y prácticas', max_length=1, choices=PREGUNTA_SATISFACCION)

    indicacion2 = (
        "Indique el grado de satisfacción general sobre el plan de estudios (currícula) de la licenciatura cursada, en los siguientes aspectos:")
    pregunta11 = models.CharField(
        'Indique el grado de satisfacción general sobre el plan de estudios (currícula) de la licenciatura cursada, en los siguientes aspectos:'
        + 'Años de duración de la carrera ', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta11_1 = models.CharField('Cursos programados diarios', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta11_2 = models.CharField('Cursos sobre conocimientos básicos', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta11_3 = models.CharField('Cursos sobre conocimientos especializados', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta11_4 = models.CharField('Cursos optativos de la profesión', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta11_5 = models.CharField('Cursos optativos de otras disciplinas', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta11_6 = models.CharField('Horas de laboratorios y talleres', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta11_7 = models.CharField('Número total de cursos ', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta11_8 = models.CharField('Nivel de inglés obligatorio', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta11_9 = models.CharField('Prácticas profesionales', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta11_10 = models.CharField('Titulación incluida', max_length=1, choices=PREGUNTA_SATISFACCION)

    indicacion3 = ("Indique el grado de satisfacción de la licenciatura cursada, en los siguientes apoyos y servicios:")
    pregunta12 = models.CharField(
        'Indique el grado de satisfacción de la licenciatura cursada, en los siguientes apoyos y servicios:'
        + 'Apoyo para asistencia a eventos académicos', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta12_1 = models.CharField('Apoyo para participar en proyectos de investigación', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta12_2 = models.CharField('Apoyo para movilidad estudiantil', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta12_3 = models.CharField('Apoyo/asesoría en el proceso de titulación', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta12_4 = models.CharField('Gestión de prácticas profesionales', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta12_5 = models.CharField('Bolsa de trabajo para egresados', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta12_6 = models.CharField('Apoyo para el aprendizaje (Asesorías, mentorías, etc.)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta12_7 = models.CharField('Servicio social', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta12_8 = models.CharField('Apoyo en actividades deportivas y culturales', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)

    indicacion4 = (
        "Indique el grado de satisfacción de la infraestructura de la universidad durante los estudios de la licenciatura:")
    pregunta13 = models.CharField(
        'Indique el grado de satisfacción de la infraestructura de la universidad durante los estudios de la licenciatura:'
        + 'Aulas adecuadas (equipamiento, disponibilidad)', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta13_1 = models.CharField('Áreas de descanso y estudio (comedor, biblioteca, etc.)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta13_2 = models.CharField('Áreas de servicios administrativos', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta13_3 = models.CharField('Espacios para eventos culturales', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta13_4 = models.CharField('Espacios de práctica deportiva', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta13_5 = models.CharField('Laboratorios y talleres adecuados (equipamiento, disponibilidad)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta13_6 = models.CharField('Sanitarios', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta13_7 = models.CharField('Servicio de internet', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta13_8 = models.CharField('Servicios bibliográficos y de bases de datos', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)

    indicacion5 = (
        "Indique el grado de satisfacción sobre las siguientes habilidades y competencias atendidas por la universidad durante los estudios de la licenciatura:")
    pregunta14 = models.CharField(
        'Indique el grado de satisfacción sobre las siguientes habilidades y competencias atendidas por la universidad durante los estudios de la licenciatura:'
        + 'Apreciación de la diversidad y multiculturalidad ', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta14_1 = models.CharField('Análisis conceptual y pensamiento crítico', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta14_2 = models.CharField('Capacidad de organizar y planificar', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta14_3 = models.CharField('Capacidad de aprender', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta14_4 = models.CharField('Capacidad de adaptación', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta14_5 = models.CharField('Capacidad de análisis y síntesis', max_length=1, choices=PREGUNTA_SATISFACCION)
    pregunta14_6 = models.CharField('Comunicación oral, escrita y gráfica', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta14_7 = models.CharField('Capacidad para generar nuevas ideas (creatividad e innovación)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta14_8 = models.CharField('Diseño, gestión y ejecución de proyectos', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta14_9 = models.CharField('Estructuración del pensamiento y expresión de las ideas', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    pregunta14_10 = models.CharField('Identificación, plantemiento y resolución de problemas', max_length=1,
                                     choices=PREGUNTA_SATISFACCION)
    pregunta14_11 = models.CharField('Manejo de datos y de tecnología de la Información (TIC´s)', max_length=1,
                                     choices=PREGUNTA_SATISFACCION)
    pregunta14_12 = models.CharField('Promover y fortalecer el uso de una segunda lengua', max_length=1,
                                     choices=PREGUNTA_SATISFACCION)
    pregunta14_13 = models.CharField('Trabajo de manera independiente y en equipo', max_length=1,
                                     choices=PREGUNTA_SATISFACCION)

    pregunta15_0 = models.CharField('¿Contó con apoyos financieros externos durante los estudios de licenciatura?',
                                  max_length=1, choices=RESPUESTASINO)
    pregunta15_1 = models.CharField('En caso de haber contando con apoyo financiero, ¿cuál fue el apoyo recibido?',
                                  max_length=1, choices=PREGUNTA15)

    pregunta15_2= models.CharField('¿Cuál es su opinión general sobre la Universidad Autónoma de Zacatecas?',
                                  max_length=1, choices=PREGUNTA_SATISFACCION2)
    pregunta16 = models.CharField('¿Cuál es su opinión general sobre la licenciatura cursada?', max_length=1,
                                  choices=PREGUNTA_SATISFACCION2)
    pregunta17 = models.CharField(
        '¿Qué cambios o actualizaciones considera que deben realizarse para mejorar la licenciatura?', max_length=1,
        choices=PREGUNTA17)

    indicacion6 = (
        "Si considera que debe haber cambios para mejorar la licenciatura, indique el nivel de importancia en los cambios a realizar, en los siguientes aspectos:")
    pregunta18 = models.CharField(
        'Si considera que debe haber cambios para mejorar la licenciatura, indique el nivel de importancia en los cambios a realizar, en los siguientes aspectos:'
        + 'Actualizar las técnicas de enseñanza', max_length=1, choices=PREGUNTA18)
    pregunta18_1 = models.CharField('Actualizar los cursos de conocimientos básicos de la carrera', max_length=1,
                                    choices=PREGUNTA18)
    pregunta18_2 = models.CharField('Actualizar los cursos de conocimientos específicos de la carrera', max_length=1,
                                    choices=PREGUNTA18)
    pregunta18_3 = models.CharField('Implementar cursos de otras disciplinas', max_length=1, choices=PREGUNTA18)
    pregunta18_4 = models.CharField('Implementar cursos de desarrollo de proyectos', max_length=1, choices=PREGUNTA18)
    pregunta18_5 = models.CharField('Implementar cursos de desarrollo de tesis', max_length=1, choices=PREGUNTA18)
    pregunta18_6 = models.CharField('Incrementar los cursos optativos especializados', max_length=1, choices=PREGUNTA18)
    pregunta18_7 = models.CharField('Mejorar las prácticas profesionales', max_length=1, choices=PREGUNTA18)
    pregunta18_8 = models.CharField('Mejorar la movilidad estudiantil', max_length=1, choices=PREGUNTA18)
    pregunta18_9 = models.CharField('Ofertar la carrera en línea (a distancia)', max_length=1, choices=PREGUNTA18)
    pregunta18_10 = models.CharField('Ofertar la carrera en fines de semana', max_length=1, choices=PREGUNTA18)
    pregunta18_11 = models.CharField('Otro (indique)', max_length=1, choices=PREGUNTA18)

    indicacion7 = (
        "Indique si recomendaría o no a la Universidad Autónoma de Zacatecas y al Programa de licenciatura, en los siguientes aspectos:")
    pregunta19 = models.CharField(
        'Indique si recomendaría o no a la Universidad Autónoma de Zacatecas y al Programa de licenciatura, en los siguientes aspectos:'
        + 'Calidad de la formación ', max_length=1, choices=RESPUESTASINO)
    pregunta19_1 = models.CharField('Prestigio de la institución y del Programa de licenciatura', max_length=1,
                                    choices=RESPUESTASINO)
    pregunta19_2 = models.CharField('Ambiente universitario', max_length=1, choices=RESPUESTASINO)
    pregunta19_3 = models.CharField('Calidad académica de los profesores', max_length=1, choices=RESPUESTASINO)
    pregunta19_4 = models.CharField('Posibilidad de encontrar empleo rápidamente', max_length=1, choices=RESPUESTASINO)
    pregunta19_5 = models.CharField('Servicios y apoyos', max_length=1, choices=RESPUESTASINO)
    pregunta19_6 = models.CharField('Infraestructura', max_length=1, choices=RESPUESTASINO)
    pregunta19_7 = models.CharField('Costos', max_length=1, choices=RESPUESTASINO)

    pregunta20 = models.TextField(
        'Indique sus recomendaciones, comentarios o sugerencias finales para la mejora de la licenciatura',
        max_length=400)
    pregunta21 = models.CharField('¿Cuáles son sus planes al egresar de la licenciatura?', max_length=1,
                                  choices=PREGUNTA21)

    indicacion8 = (
        "Indique si desea mantener vinculación académica / profesional con la Universidad Autónoma de Zacatecas, en los siguientes aspectos:   ")
    pregunta21 = models.CharField(
        'Indique si desea mantener vinculación académica / profesional con la Universidad Autónoma de Zacatecas, en los siguientes aspectos:'
        + 'Como asesor o director de tesis', max_length=1, choices=RESPUESTASINO)
    pregunta21_1 = models.CharField('Como asesor o director en prácticas profesionales de estudiantes', max_length=1,
                                    choices=RESPUESTASINO)
    pregunta21_2 = models.CharField('Como conferencista', max_length=1, choices=RESPUESTASINO)
    pregunta21_3 = models.CharField('Como docente/investigador', max_length=1, choices=RESPUESTASINO)
    pregunta21_4 = models.CharField('Como benefactor de la institución', max_length=1, choices=RESPUESTASINO)
    pregunta21_5 = models.CharField('Como colaborador en proyectos específicos', max_length=1, choices=RESPUESTASINO)


class PreguntasEgresados(models.Model):
    user = models.OneToOneField(User, verbose_name="usuario", on_delete=models.DO_NOTHING)
    
    # alumno = models.ForeignKey(Alumno, on_delete=models.CASCADE)
    licenciatura = models.CharField('Nombre de la licenciatura cursada:', max_length=300)
    unidadAcademica = models.CharField('Nombre de la Unidad Académica a la que pertenece la licenciatura cursada:',
                                       max_length=300)
    areac = models.CharField('Nombre del Área del conocimiento a la que pertenece la Unidad Académica:', max_length=300)
    fechaIngreso = models.DateTimeField('Fecha de ingreso a la licenciatura')
    fechaEgreso = models.CharField('Fecha de egreso de la licenciatura', max_length=300)

    preguntaE1 = models.CharField('¿Presentó el Examen General de Egreso (EGEL)?', max_length=1, choices=RESPUESTASINO)

    preguntaE2 = models.CharField('Si presentó el Examen General de Egreso (EGEL) ¿cuál fue el resultado obtenido?',
                                 max_length=1, choices=PREGUNTA5)

    preguntaE3 = models.CharField('¿Se tituló de la licenciatura?', max_length=1, choices=RESPUESTASINO)

    preguntaE4 = models.CharField('Si se ha titulado, ¿cuál fue la opción de titulación que seleccionó?', max_length=1,
                                 choices=PREGUNTATITULACION)

    preguntaE5 = models.CharField('En caso de no haberse titulado aún, indique el motivo principal', max_length=1,
                                 choices=PREGUNTA8)

    preguntaE6 = models.CharField('En caso de no haberse titulado aún, ¿en cuánto tiempo planea titularse?',
                                 max_length=1, choices=PREGUNTA9)

    preguntaE7 = models.CharField('Razón principal por la que decidió estudiar una licenciatura', max_length=1,
                                 choices=PREGUNTA1)

    preguntaE8 = models.CharField('Razón principal por la que decidió estudiar la licenciatura cursada', max_length=1,
                                 choices=PREGUNTAE8)

    preguntaE9 = models.CharField('Indique el medio por el que se enteró de la licenciatura cursada', max_length=1,
                                 choices=PREGUNTA3)

    indicacionE1 = (
        "Indique el grado de satisfacción general sobre los docentes de la licenciatura cursada, en los siguientes aspectos:")

    preguntaE10_1 = models.CharField('Indique el grado de satisfacción general sobre los docentes de la licenciatura cursada, en los siguientes aspectos:'
                                        +' \n Atención y asesoría fuera de clase ', max_length=1, choices=PREGUNTA_SATISFACCION)

    preguntaE10_2 = models.CharField('Formación académica', max_length=1, choices=PREGUNTA_SATISFACCION)

    preguntaE10_3 = models.CharField('Preparación de clases', max_length=1, choices=PREGUNTA_SATISFACCION)

    preguntaE10_4 = models.CharField('Procesos de aprendizaje (metodología, ayudas utilizadas)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE10_5 = models.CharField('Puntualidad y asistencia', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE10_6 = models.CharField('Cumplimiento del programa académico de los cursos', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE10_7 = models.CharField('Criterios de evaluación', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE10_8 = models.CharField('Relación adecuada con los estudiantes', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE10_9 = models.CharField('Trabajo de campo y prácticas', max_length=1, choices=PREGUNTA_SATISFACCION)

    indicacionE2 = (
        "Indique el grado de satisfacción general sobre el plan de estudios (currícula) de la licenciatura cursada, en los siguientes aspectos:")

    preguntaE11_1 = models.CharField('Indique el grado de satisfacción general sobre el plan de estudios (currícula) de la licenciatura cursada, en los siguientes aspectos:'
                                        +'Años de duración de la carrera ', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE11_2 = models.CharField('Cursos programados diarios', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE11_3 = models.CharField('Contenidos de actualidad de los cursos', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE11_4 = models.CharField('Cursos sobre conocimientos básicos', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE11_5 = models.CharField('Cursos sobre conocimientos especializados', max_length=1,choices=PREGUNTA_SATISFACCION)
    preguntaE11_6 = models.CharField('Cursos optativos de la profesión', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE11_7 = models.CharField('Cursos optativos de otras disciplinas', max_length=1,choices=PREGUNTA_SATISFACCION)
    preguntaE11_8 = models.CharField('Horas de laboratorios y talleres', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE11_9 = models.CharField('Número total de cursos ', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE11_10 = models.CharField('Nivel de inglés obligatorio', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE11_11 = models.CharField('Prácticas profesionales', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE11_12 = models.CharField('Titulación incluida', max_length=1, choices=PREGUNTA_SATISFACCION)

    indicacionE3 = ("Indique el grado de satisfacción de la licenciatura cursada, en los siguientes apoyos y servicios:")
    preguntaE12_1 = models.CharField('Indique el grado de satisfacción de la licenciatura cursada, en los siguientes apoyos y servicios:'
                                        +'Apoyo para asistencia a eventos académicos', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE12_2 = models.CharField('Apoyo para participar en proyectos de investigación', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE12_3 = models.CharField('Apoyo para movilidad estudiantil', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE12_4 = models.CharField('Apoyo/asesoría en el proceso de titulación', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE12_5 = models.CharField('Gestión de prácticas profesionales', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE12_6 = models.CharField('Bolsa de trabajo', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE12_7 = models.CharField('Apoyo para el aprendizaje (Asesorías, mentorías, etc.)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE12_8 = models.CharField('Servicio social', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE12_9 = models.CharField('Apoyo en actividades deportivas y culturales', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)

    indicacionE4 = ("Indique el grado de satisfacción de la infraestructura de la universidad durante los estudios de la licenciatura:")
    preguntaE13_1 = models.CharField('Indique el grado de satisfacción de la infraestructura de la universidad durante los estudios de la licenciatura:'
                                        +'Aulas adecuadas (tamaño,mobiliario,equipamiento, disponibilidad)', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE13_2 = models.CharField('Áreas de descanso y estudio (comedor, biblioteca, etc.)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE13_3 = models.CharField('Áreas de servicios administrativos', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE13_4 = models.CharField('Espacios para eventos culturales', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE13_5 = models.CharField('Espacios de práctica deportiva', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE13_6 = models.CharField('Laboratorios y talleres adecuados (equipamiento, disponibilidad)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE13_7 = models.CharField('Sanitarios', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE13_8 = models.CharField('Servicio de internet', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE13_9 = models.CharField('Servicios bibliográficos y de bases de datos', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)

    indicacionE5 = ("Indique el grado de satisfacción sobre las siguientes habilidades y competencias atendidas por la universidad durante los estudios de la licenciatura:")
    preguntaE14_1 = models.CharField('Indique el grado de satisfacción sobre las siguientes habilidades y competencias atendidas por la universidad durante los estudios de la licenciatura:'
                                        +'Apreciación de la diversidad y multiculturalidad ', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE14_2 = models.CharField('Análisis conceptual y pensamiento crítico', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE14_3 = models.CharField('Capacidad de organizar y planificar', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE14_4 = models.CharField('Capacidad de aprender', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE14_5 = models.CharField('Capacidad de adaptación', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE14_6 = models.CharField('Capacidad de análisis y síntesis', max_length=1, choices=PREGUNTA_SATISFACCION)
    preguntaE14_7 = models.CharField('Comunicación oral, escrita y gráfica', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE14_8 = models.CharField('Capacidad para generar nuevas ideas (creatividad e innovación)', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE14_9 = models.CharField('Diseño, gestión y ejecución de proyectos', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE14_10 = models.CharField('Estructuración del pensamiento y expresión de las ideas', max_length=1,
                                    choices=PREGUNTA_SATISFACCION)
    preguntaE14_11 = models.CharField('Identificación, plantemiento y resolución de problemas', max_length=1,
                                     choices=PREGUNTA_SATISFACCION)
    preguntaE14_12 = models.CharField('Manejo de datos y de tecnología de la Información (TIC´s)', max_length=1,
                                     choices=PREGUNTA_SATISFACCION)
    preguntaE14_13 = models.CharField('Promover y fortalecer el uso de una segunda lengua', max_length=1,
                                     choices=PREGUNTA_SATISFACCION)
    preguntaE14_14 = models.CharField('Trabajo de manera independiente y en equipo', max_length=1,
                                     choices=PREGUNTA_SATISFACCION)
    preguntaE15 = models.CharField('¿Cuál fue la fuente de financiamiento durante los estudios de licenciatura?',
                                   max_length=1,choices=PREGUNTAE15)
    preguntaE16 = models.CharField('¿Contó con apoyos financieros externos durante los estudios de licenciatura?',
                                  max_length=1, choices=RESPUESTASINO)
    preguntaE17 = models.CharField('En caso de haber contando con apoyo financiero, ¿cuál fue el apoyo recibido?',
                                  max_length=1, choices=PREGUNTAE17)
    preguntaE18 = models.CharField('¿Cuál es su opinión general sobre la Universidad Autónoma de Zacatecas?',
                                  max_length=1, choices=PREGUNTA_SATISFACCION2)
    preguntaE19 = models.CharField('¿Cuál es su opinión general sobre la licenciatura cursada?', max_length=1,
                                  choices=PREGUNTA_SATISFACCION2)
    preguntaE20 = models.CharField(
        '¿Qué cambios o actualizaciones considera que deben realizarse para mejorar la licenciatura?', max_length=1,
        choices=PREGUNTA17)

    indicacionE6 = (
        "Si considera que debe haber cambios para mejorar la licenciatura, indique el nivel de importancia en los cambios a realizar, en los siguientes aspectos:")
    preguntaE21_1 = models.CharField('Si considera que debe haber cambios para mejorar la licenciatura, indique el nivel de importancia en los cambios a realizar, en los siguientes aspectos:'
                                        +'Actualizar las técnicas de enseñanza', max_length=1, choices=PREGUNTA18)
    preguntaE21_2 = models.CharField('Actualizar los cursos de conocimientos básicos de la carrera', max_length=1,
                                    choices=PREGUNTA18)
    preguntaE21_3 = models.CharField('Actualizar los cursos de conocimientos específicos de la carrera', max_length=1,
                                    choices=PREGUNTA18)
    preguntaE21_4 = models.CharField('Implementar cursos de otras disciplinas', max_length=1, choices=PREGUNTA18)
    preguntaE21_5 = models.CharField('Implementar cursos de desarrollo de proyectos', max_length=1, choices=PREGUNTA18)
    preguntaE21_6 = models.CharField('Implementar cursos de desarrollo de tesis', max_length=1, choices=PREGUNTA18)
    preguntaE21_7 = models.CharField('Incrementar los cursos optativos especializados', max_length=1, choices=PREGUNTA18)
    preguntaE21_8 = models.CharField('Mejorar las prácticas profesionales', max_length=1, choices=PREGUNTA18)
    preguntaE21_9 = models.CharField('Mejorar la movilidad estudiantil', max_length=1, choices=PREGUNTA18)
    preguntaE21_10 = models.CharField('Ofertar la carrera en línea (a distancia)', max_length=1, choices=PREGUNTA18)
    preguntaE21_11 = models.CharField('Ofertar la carrera en fines de semana', max_length=1, choices=PREGUNTA18)
    preguntaE21_12 = models.CharField('Otro (indique)', max_length=1, choices=PREGUNTA18)
    preguntaE22 = models.CharField('Si considera que debiera modificarse la tirilla de materias del programas de licenciatura, indique los cursos que debieran eliminarse:',
                                    max_length=300)
    pregunta23 = models.CharField('Si considera que debiera modificarse la tirilla de materias del programas de licenciatura, indique los cursos que debieran incluirse:',
                                    max_length=300)
    indicacionE7 = ("Indique si recomendaría o no a la Universidad Autónoma de Zacatecas y al Programa de licenciatura, en los siguientes aspectos:")
    
    preguntaE24 = models.CharField('Indique si recomendaría o no a la Universidad Autónoma de Zacatecas y al Programa de licenciatura, en los siguientes aspectos:'+
                'Calidad de la formación',max_length=1,choices=RESPUESTASINO)
    preguntaE24_1 = models.CharField('Prestigio de la institución y del Programa de licenciatura',max_length=1,choices=RESPUESTASINO)
    preguntaE24_2 = models.CharField('Ambiente universitario',max_length=1,choices=RESPUESTASINO)
    preguntaE24_3 = models.CharField('Calidad académica de los profesores',max_length=1,choices=RESPUESTASINO)
    preguntaE24_4 = models.CharField('Apoyos ofrecidos durante el proceso de formación ',max_length=1,choices=RESPUESTASINO)
    preguntaE24_5 = models.CharField('Posibilidad de encontrar empleo rápidamente',max_length=1,choices=RESPUESTASINO)
    preguntaE24_6 = models.CharField('Servicios y apoyos',max_length=1,choices=RESPUESTASINO)
    preguntaE24_7 = models.CharField('Infraestructura',max_length=1,choices=RESPUESTASINO)
    preguntaE24_8 = models.CharField('Costos',max_length=1,choices=RESPUESTASINO)
    preguntaE25 = models.CharField('Indique sus recomendaciones, comentarios o sugerencias finales para la mejora de la licenciatura:',max_length=300)
    preguntaE26 = models.CharField('Al término de la licenciatura, ¿cuánto tiempo transcurrió para encontrar su primer empleo relacionado con su profesión?:',
                                    max_length=1,choices=PREGUNTAEMPLEO)
    preguntaE27 = models.CharField('¿Cuál es su situación laboral actual? ',max_length=1,choices=SITUACIONLABORAL)
    preguntaE28 = models.CharField('En caso de no estar laborando actualmente, indique la razón principal:',max_length=2,choices=SITUACIONLABORAL2)
    indicacionE8 = ('Indique los datos de la organización/empresa donde labora actualmente')
    preguntaE29 = models.CharField('Indique los datos de la organización/empresa donde labora actualmente'
                                    + ' nombre',max_length=100)
    preguntaE29_1 = models.CharField('Calle', max_length=50)
    preguntaE29_2 = models.CharField('Número exterior', max_length=6)
    preguntaE29_3 = models.CharField('Municipio', max_length=50)
    preguntaE29_4 = models.CharField('Estado', max_length=50)
    preguntaE29_5 = models.CharField('Correo electrónico', max_length=100)
    preguntaE29_6 = models.CharField('Facebook', max_length=50)
    preguntaE29_7 = models.CharField('Telefono', max_length=10)

    preguntaE30 = models.CharField('¿Su empleo actual está relacionado con su profesión? ', max_length=1,choices=RESPUESTASINO)
    preguntaE31 = models.CharField('¿Cuál es el giro comercial de la empresa/organización en la que labora actualmente? ', max_length=1,choices=PREGUNTAE31)
    preguntaE32 = models.CharField('¿Cuál es el sector de la empresa/organización en la que labora actualmente? ', max_length=1,choices=PREGUNTAE32)
    preguntaE33 = models.CharField('¿Cuál es el tamaño de la empresa/organización en la que labora actualmente? ', max_length=1,choices=PREGUNTAE33)
    preguntaE34 = models.CharField('¿Cuál es el tipo de contrato convenido con la empresa/organización donde labora?', max_length=1, choices=PREGUNTAE34)
    preguntaE35 = models.CharField('Indique el medio por el que encontró empleo en su campo profesional', max_length=1,choices=PREGUNTAE35)
    
    indicacionE8=("Indique la importancia de los siguientes aspectos que influyeron para obtener el empleo actual")
    preguntaE36 = models.CharField('54.	Indique la importancia de los siguientes aspectos que influyeron para obtener el empleo actual: El sexo'
                                    , max_length=1,choices=PREGUNTA18)
    preguntaE36_1 = models.CharField('La edad', max_length=1,choices=PREGUNTA18)
    preguntaE36_2 = models.CharField('La nacionalidad', max_length=1,choices=PREGUNTA18)
    preguntaE36_3 = models.CharField('El ser egresado de la UAZ', max_length=1,choices=PREGUNTA18)
    preguntaE36_4 = models.CharField('El tipo de licenciatura cursada', max_length=1,choices=PREGUNTA18)
    preguntaE36_5 = models.CharField('El estar titulado', max_length=1,choices=PREGUNTA18)
    preguntaE36_5 = models.CharField('El promedio de calificaciones', max_length=1,choices=PREGUNTA18)
    preguntaE36_6 = models.CharField('Las cartas de recomendación', max_length=1,choices=PREGUNTA18)
    preguntaE36_7 = models.CharField('La presentación personal', max_length=1,choices=PREGUNTA18)
    preguntaE36_8 = models.CharField('El dominio de un segundo idioma', max_length=1,choices=PREGUNTA18)
    preguntaE36_9 = models.CharField('El aprobar examen de conocimientos', max_length=1,choices=PREGUNTA18)
    preguntaE36_10 = models.CharField('El aprobar examen de aptitudes', max_length=1,choices=PREGUNTA18)
    preguntaE36_11 = models.CharField('El aprobar examen psicométrico', max_length=1,choices=PREGUNTA18)

    preguntaE37 = models.CharField('¿Cuál es el cargo que ocupa actualmente en la empresa/organización en la que labora? ', max_length=1,choices=PREGUNTAE37)
    preguntaE38 = models.CharField('¿Cuál es el promedio de salario total mensual (incluido impuestos) que percibe actualmente en la empresa/organización en la que labora? ',
                                         max_length=2,choices=PREGUNTAE38)
    preguntaE39 = models.CharField('¿Considera que el desempeño de su trabajo se relaciona con su formación en la licenciatura?', max_length=1,choices=DEACUERDO)
    preguntaE40 = models.CharField('¿Considera que el trabajo asignado corresponde al nivel de su preparación en la licenciatura?', max_length=1,choices=DEACUERDO)
    preguntaE41 = models.CharField('¿Considera que el servicio social realizado en la licenciatura impactó en el trabajo obtenido?', max_length=1,choices=DEACUERDO)

    indicacionE9 = ('Indique como impactó la preparación recibida en la licenciatura, en los siguientes aspectos de su trabajo: ' )

    preguntaE42 = models.CharField('Indique como impactó la preparación recibida en la licenciatura, en los siguientes aspectos de su trabajo:'+
                                    'Incremento en el salario',max_length=1,choices=POCONADA)
    preguntaE42_1 = models.CharField('Cambio a un mejor puesto de trabajo',max_length=1,choices=POCONADA)
    preguntaE42_2 = models.CharField('Incremento en las responsabilidades del trabajo',max_length=1,choices=POCONADA)
    preguntaE42_3 = models.CharField('Incremento en el nivel jerárquico ',max_length=1,choices=POCONADA)
    preguntaE42_4 = models.CharField('Posibilidad de crear su propia empresa ',max_length=1,choices=POCONADA)
    preguntaE43 = models.CharField('¿Cursó o está cursando actualmente algún Programa de posgrado?',max_length=1,choices=RESPUESTASINO)
    preguntaE44 = models.CharField('En caso de que esté cursando o hay cursado un Programa de posgrado, ¿de que nivel es?',max_length=1,choices=NIVELPOSGRADO)
    preguntaE45 = models.CharField('En caso de que esté cursando actualmente un Programa de posgrado, ¿en que Institución?',max_length=35)

    indicacionE10 =('Indique si desea mantener vinculación académica / profesional con la Universidad Autónoma de Zacatecas, en los siguientes aspectos:')
    preguntaE46 = models.CharField('Indique si desea mantener vinculación académica / profesional con la Universidad Autónoma de Zacatecas, en los siguientes aspectos:'+
                                        'Como asesor o director de tesis',max_length=1,choices=RESPUESTASINO)
    preguntaE46_1 = models.CharField('Como asesor o director en prácticas profesionales de estudiantes en la organización donde labora',max_length=1,choices=RESPUESTASINO)
    preguntaE46_2 = models.CharField('Como conferencista',max_length=1,choices=RESPUESTASINO)
    preguntaE46_3 = models.CharField('Como docente/investigador ',max_length=1,choices=RESPUESTASINO)
    preguntaE46_4 = models.CharField('Como benefactor de la institución',max_length=1,choices=RESPUESTASINO)
    preguntaE46_5 = models.CharField('Como colaborador por parte de la organización donde labora',max_length=1,choices=RESPUESTASINO)
    preguntaE46_6 = models.CharField('Otro (indique)',max_length=1,choices=RESPUESTASINO)
                        
class PreguntasPosgrado(models.Model):
    alumno=models.ForeignKey(Alumno,on_delete=models.CASCADE)
    posgrado=models.CharField('Nombre del posgrado cursado: ', max_length=300)
    unidadAcademica=models.CharField('Nombre de la Unidad Académica a la que pertenece el posgrado cursada:', max_length=300)
    areac=models.CharField('Nombre del Área del conocimiento a la que pertenece la Unidad Académica:', max_length=300)
    fechaIngreso=models.DateTimeField('Fecha de ingreso a la licenciatura')
    fechaEgreso=models.CharField('Fecha de egreso de la licenciatura', max_length=300)
    pregunta1=models.CharField('Razón principal por la que decidió estudiar una licenciatura', max_length=1,choices=PREGUNTA1P)
    pregunta2=models.CharField('Razón principal por la que decidió estudiar el posgrado cursado', max_length=1,choices=PREGUNTA2P)
    pregunta3=models.CharField('¿Cómo se enteró del posgrado cursado?', max_length=1,choices=PREGUNTA3P)
    pregunta4=models.CharField('¿Se tituló del posgrado?', max_length=1,choices=RESPUESTASINO)
    pregunta5=models.CharField('En caso de no haberse titulado aún, indique el motivo principal', max_length=1,choices=PREGUNTA8)
    pregunta6=models.CharField('En caso de no haberse titulado aún, ¿en cuánto tiempo planea titularse?', max_length=1,choices=PREGUNTA9)
    
    indicacionP1=('Indique el grado de satisfacción general sobre los docentes-investigadores del posgrado cursado, en los siguientes aspectos:')
    pregunta7=models.CharField('Indique el grado de satisfacción general sobre los docentes-investigadores del posgrado cursado, en los siguientes aspectos:'
                                +'Atención y asesoría fuera de clase', max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta7_1=models.CharField('Desarrollo de proyectos de investigación',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta7_2=models.CharField('Formación académica',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta7_3=models.CharField('Preparación de clases',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta7_4=models.CharField('Procesos de aprendizaje (metodología, ayudas utilizadas) ',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta7_5=models.CharField('Producción académica',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta7_6=models.CharField('Puntualidad y asistencia',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta7_7=models.CharField('Relación adecuada con los estudiantes ',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta7_8=models.CharField('Trabajo de campo',max_length=1,choices=PREGUNTA_SATISFACCION)

    indicacionP2=('Indique el grado de satisfacción general sobre el plan de estudios (currícula) del posgrado cursado, en los siguientes aspectos:')
    pregunta8=models.CharField('Indique el grado de satisfacción general sobre el plan de estudios (currícula) del posgrado cursado, en los siguientes aspectos:'
                                +' Años de duración del Programa ',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_1=models.CharField('Cursos programados por día',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_2=models.CharField('Cursos sobre conocimientos básicos',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_3=models.CharField('Cursos sobre conocimientos especializados',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_4=models.CharField('Cursos optativos de la especialidad',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_5=models.CharField('Cursos optativos de otras disciplinas',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_6=models.CharField('Desarrollo de proyecto de tesis',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_7=models.CharField('Horas de laboratorios y talleres',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_8=models.CharField('Número total de cursos ',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_9=models.CharField('Nivel de inglés obligatorio',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_10=models.CharField('Participación en proyectos de investigación',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta8_11=models.CharField('Movilidad estudiantil',max_length=1,choices=PREGUNTA_SATISFACCION)

    indicacionP3=('Indique el grado de satisfacción del posgrado cursado, en los siguientes apoyos y servicios:')
    pregunta9=models.CharField('Indique el grado de satisfacción del posgrado cursado, en los siguientes apoyos y servicios:'
                                + 'Apoyo para asistencia a eventos académicos',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta9_1=models.CharField('Apoyo para participar en proyectos de investigación',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta9_2=models.CharField('Apoyo para publicación de artículos de investigación',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta9_3=models.CharField('Apoyo para movilidad estudiantil',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta9_4=models.CharField('Apoyo/asesoría en el proceso de investigación y titulación',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta9_5=models.CharField('Apoyo para el aprendizaje (Asesorías, mentorías, etc.)',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta9_6=models.CharField('Bolsa de trabajo',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta9_7=models.CharField('Vinculación con los sectores público y privado',max_length=1,choices=PREGUNTA_SATISFACCION)

    indicacionP4=('Indique el grado de satisfacción de la infraestructura de la universidad durante los estudios del posgrado cursado:')
    pregunta10=models.CharField('Indique el grado de satisfacción de la infraestructura de la universidad durante los estudios del posgrado cursado:'
                                + 'Aulas adecuadas (equipamiento, disponibilidad)',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta10_1=models.CharField('Áreas de descanso y estudio (comedor, biblioteca, etc.)',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta10_2=models.CharField('Áreas de servicios administrativos',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta10_3=models.CharField('Espacios para eventos culturales',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta10_4=models.CharField('Espacios de práctica deportiva',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta10_5=models.CharField('Laboratorios y talleres adecuados (equipamiento, disponibilidad)',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta10_6=models.CharField('Sanitarios',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta10_7=models.CharField('Servicio de internet',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta10_8=models.CharField('Servicios bibliográficos y de bases de datos',max_length=1,choices=PREGUNTA_SATISFACCION)

    indicacionP5=('Indique el grado de satisfacción sobre las siguientes habilidades y competencias atendidas por la universidad durante los estudios del posgrado:')
    pregunta11=models.CharField('Indique el grado de satisfacción sobre las siguientes habilidades y competencias atendidas por la universidad durante los estudios del posgrado:'
                                + 'Apreciación de la diversidad y multiculturalidad',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_1=models.CharField('Análisis conceptual y pensamiento crítico',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_2=models.CharField('Capacidad de organizar y planificar',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_3=models.CharField('Capacidad de aprender',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_4=models.CharField('Capacidad de adaptación',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_5=models.CharField('Capacidad de análisis y síntesis',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_6=models.CharField('Comunicación oral, escrita y gráfica',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_7=models.CharField('Capacidad para generar nuevas ideas (creatividad e innovación)',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_8=models.CharField('Diseño, gestión y ejecución de proyectos',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_9=models.CharField('Estructuración del pensamiento y expresión de las ideas',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_10=models.CharField('Identificación, plantemiento y resolución de problemas',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_11=models.CharField('Manejo de datos y de herramientas de tecnología de la Información (TIC´s)',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_12=models.CharField('Promover y fortalecer el uso de una segunda lengua.',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta11_13=models.CharField('Trabajo de manera independiente y en equipo',max_length=1,choices=PREGUNTA_SATISFACCION)
    
    pregunta12=models.CharField('¿Contó con apoyos financieros externos durante los estudios del posgrado?',max_length=1,choices=RESPUESTASINO)
    pregunta13=models.CharField('En caso de haber contando con apoyo financiero ¿cuál fue el apoyo recibido?',max_length=1,choices=PREGUNTABECA)
    pregunta14=models.CharField('¿Cuál es su opinión general sobre la Universidad Autónoma de Zacatecas?',max_length=1,choices=PREGUNTA_SATISFACCION2)
    pregunta15=models.CharField('¿Cuál es su opinión general sobre el posgrado cursado?',max_length=1,choices=PREGUNTA_SATISFACCION2)
    pregunta16=models.CharField('¿Qué cambios o actualizaciones considera que deben realizarse para mejorar el posgrado cursado?',max_length=1,choices=PREGUNTA17)
    
    
    indicacionP6=('Si considera que debe haber cambios para mejorar el pòsgrado cursado, indique el nivel de importancia en los cambios a realizar, en los siguientes aspectos:')
    pregunta17=models.CharField('Si considera que debe haber cambios para mejorar el pòsgrado cursado, indique el nivel de importancia en los cambios a realizar, en los siguientes aspectos:'
                                + 'Actualizar las técnicas de enseñanza',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta17_1=models.CharField('Actualizar los cursos de conocimientos básicos contenido en el Programa',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta17_2=models.CharField('Actualizar los cursos de conocimientos especializados contenidos en el Programa',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta17_3=models.CharField('Implementar cursos de otras disciplinas',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta17_4=models.CharField('Implementar cursos de desarrollo de proyectos',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta17_5=models.CharField('Incrementar los cursos optativos especializados',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta17_6=models.CharField('Mejorar la movilidad estudiantil',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta17_7=models.CharField('Ofertar el posgrado en línea (a distancia)',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta17_8=models.CharField('Ofertar el posgrado en fines de semana',max_length=1,choices=PREGUNTA_SATISFACCION)
    pregunta17_9=models.CharField('Otro (indique)',max_length=1,choices=PREGUNTA_SATISFACCION)

    indicacionP7=('Indique si recomendaría o no a la Universidad Autónoma de Zacatecas y al Programa de posgrado, en los siguientes aspectos:')
    pregunta18=models.CharField('Indique si recomendaría o no a la Universidad Autónoma de Zacatecas y al Programa de posgrado, en los siguientes aspectos:'
                                + 'Calidad de la formación ',max_length=1,choices=RESPUESTASINO)
    pregunta18_1=models.CharField('Prestigio de la institución y calidad del Programa de posgrado',max_length=1,choices=RESPUESTASINO)
    pregunta18_2=models.CharField('Ambiente universitario',max_length=1,choices=RESPUESTASINO)
    pregunta18_3=models.CharField('Calidad académica de los profesores',max_length=1,choices=RESPUESTASINO)
    pregunta18_4=models.CharField('Posibilidad de encontrar empleo rápidamente',max_length=1,choices=RESPUESTASINO)
    pregunta18_5=models.CharField('Servicios y apoyos',max_length=1,choices=RESPUESTASINO)
    pregunta18_6=models.CharField('Infraestructura',max_length=1,choices=RESPUESTASINO)
    pregunta18_7=models.CharField('Costos',max_length=1,choices=RESPUESTASINO)

    pregunta19=models.CharField('Indique sus recomendaciones, comentarios o sugerencias finales para la mejora del posgrado',max_length=200)
    pregunta20=models.CharField('Al término del posgrado, ¿Cuánto tiempo transcurrió para incorporarse a la actividad laboral? ',max_length=1,choices=PREGUNTAEMPLEO)
    pregunta21=models.CharField('¿Cuál es su situación laboral actual?',max_length=1,choices=SITUACIONLABORAL)
    pregunta22=models.CharField('¿Su empleo actual está relacionado con sus estudios de posgrado? ',max_length=1,choices=RESPUESTASINO)
    pregunta23=models.CharField('En caso de no estar laborando actualmente, indique la razón principal',max_length=2,choices=SITUACIONLABORAL2)
    pregunta24=models.CharField('¿Cuál es el giro comercial de la empresa/organización en la que labora actualmente? ',max_length=2,choices=PREGUNTAE31)
    pregunta25=models.CharField('¿Cuál es el sector de la empresa/organización en la que labora actualmente? ',max_length=2,choices=PREGUNTAE32) 
    pregunta26=models.CharField('¿Cuál es el tamaño de la empresa/organización en la que labora actualmente? ',max_length=2,choices=PREGUNTAE33)
    pregunta27=models.CharField('¿Cuál es el tipo de contrato convenido con la empresa/organización donde labora? ',max_length=2,choices=PREGUNTAE34)
    pregunta28=models.CharField('Indique el medio por el que encontró empleo en su campo profesional',max_length=2,choices=PREGUNTAE35)

    indicacionP8=("Indique la importancia de los siguientes aspectos que influyeron para obtener el empleo actual")
    pregunta29 = models.CharField('Indique la importancia de los siguientes aspectos que influyeron para obtener el empleo actual: El sexo'
                                    , max_length=1,choices=PREGUNTA18)
    pregunta29 = models.CharField('La edad', max_length=1,choices=PREGUNTA18)
    pregunta29_2 = models.CharField('La nacionalidad', max_length=1,choices=PREGUNTA18)
    pregunta29_3 = models.CharField('El ser egresado de la UAZ', max_length=1,choices=PREGUNTA18)
    pregunta29_4 = models.CharField('El tipo de posgrado cursada', max_length=1,choices=PREGUNTA18)
    pregunta29_5 = models.CharField('El estar titulado', max_length=1,choices=PREGUNTA18)
    pregunta29_5 = models.CharField('El promedio de calificaciones', max_length=1,choices=PREGUNTA18)
    pregunta29_6 = models.CharField('Las cartas de recomendación', max_length=1,choices=PREGUNTA18)
    pregunta29_7 = models.CharField('La presentación personal', max_length=1,choices=PREGUNTA18)
    pregunta29_8 = models.CharField('El dominio de un segundo idioma', max_length=1,choices=PREGUNTA18)
    pregunta29_9 = models.CharField('El aprobar examen de conocimientos', max_length=1,choices=PREGUNTA18)
    pregunta29_10 = models.CharField('El aprobar examen psicométrico', max_length=1,choices=PREGUNTA18)

    pregunta30= models.CharField('¿Cuál es el cargo que ocupa en la empresa en la labora actualmente? ', max_length=1,choices=PREGUNTAE37)
    pregunta31= models.CharField('¿Cuál es el promedio de salario total mensual (incluido impuestos) que percibe actualmente? ', max_length=2,choices=PREGUNTAE38)
    pregunta32= models.CharField('¿Considera que el desempeño de su trabajo se relaciona con la formación en el posgrado?', max_length=1,choices=DEACUERDO)
    pregunta33= models.CharField('¿Considera que el trabajo asignado corresponde al nivel de su preparación en el posgrado?', max_length=1,choices=DEACUERDO)
    pregunta34= models.CharField('¿Considera que la preparación recibida en el posgrado impactó en el trabajo obtenido?', max_length=1,choices=DEACUERDO)
    
    indicacionP9=("Indique el impacto de la preparación recibida del posgrado, en los siguientes aspectos ")
    pregunta35 = models.CharField('Indique el impacto de la preparación recibida del posgrado, en los siguientes aspectos:'+
                                    'Incremento en el salario', max_length=1,choices=RESPUESTASINO)
    pregunta35_2 = models.CharField('Cambio en un mejor puesto de trabajo', max_length=1,choices=RESPUESTASINO)
    pregunta35_3 = models.CharField('Incremento en las responsabilidades del trabajo', max_length=1,choices=RESPUESTASINO)
    pregunta35_4 = models.CharField('Incremento en el nivel jerárquico ', max_length=1,choices=RESPUESTASINO)
    pregunta35_5 = models.CharField('Posibilidad de crear su propia empresa ', max_length=1,choices=RESPUESTASINO)

    indicacionP10=("Indique el grado de importancia en las siguientes las habilidades, competencias y conocimientos a atenderse por el posgrado cursado, necesarios para el desarrollo laboral: ")
    pregunta36 = models.CharField('Indique el grado de importancia en las siguientes las habilidades, competencias y conocimientos a atenderse por el posgrado cursado, necesarios para el desarrollo laboral:'+
                                    'Análisis geoespacial de datos', max_length=1,choices=PREGUNTA18)
    pregunta36_2 = models.CharField('Big data', max_length=1,choices=PREGUNTA18)
    pregunta36_3 = models.CharField('Comunicación oral, escrita y gráfica en español', max_length=1,choices=PREGUNTA18)
    pregunta36_4 = models.CharField('Comunicación oral, escrita y gráfica en el idioma inglés.', max_length=1,choices=PREGUNTA18)
    pregunta36_5 = models.CharField('Conocimiento de fondos públicos y privados para apoyo a la investigación', max_length=1,choices=PREGUNTA18)
    pregunta36_6 = models.CharField('Conocimiento de normas de propiedad intelectual', max_length=1,choices=PREGUNTA18)
    pregunta36_7 = models.CharField('Desarrollo de proyectos', max_length=1,choices=PREGUNTA18)
    pregunta36_8 = models.CharField('Diseño de experimentos', max_length=1,choices=PREGUNTA18)
    pregunta36_9 = models.CharField('Epistemología', max_length=1,choices=PREGUNTA18)
    pregunta36_10 = models.CharField('Econometría', max_length=1,choices=PREGUNTA18)
    pregunta36_11 = models.CharField('Etnografía', max_length=1,choices=PREGUNTA18)
    pregunta36_12 = models.CharField('Gestión de patentes, de modelos de utilidad, y registro de marcas', max_length=1,choices=PREGUNTA18)
    pregunta36_13 = models.CharField('Metodología de la investigación', max_length=1,choices=PREGUNTA18)
    pregunta36_14 = models.CharField('Búsqueda y manejo de información y bases de datos', max_length=1,choices=PREGUNTA18)
    pregunta36_15 = models.CharField('Manejo de herramientas computacionales y Tecnologías de la Información (TIC´s)', max_length=1,choices=PREGUNTA18)
    pregunta36_16 = models.CharField('Manejo de Sistemas de Información Geográfica (SIG`s)', max_length=1,choices=PREGUNTA18)
    pregunta36_17 = models.CharField('Preparación de propuestas de proyectos (CONACyT u otra organización similar)', max_length=1,choices=PREGUNTA18)
    pregunta36_18 = models.CharField('Redacción de artículos científicos para publicación en revistas de impacto', max_length=1,choices=PREGUNTA18)

    pregunta37 = models.CharField('¿Participa en alguna organización sin fines de lucro?', max_length=1,choices=ORGANIZACION)
    pregunta38 = models.CharField('¿Está cursando actualmente algún otro posgrado?', max_length=1,choices=RESPUESTASINO)
    pregunta39 = models.CharField('En caso de que esté cursando actualmente otro posgrado, ¿de que nivel es?', max_length=1,choices=NIVELPOSGRADO)
    pregunta40 = models.CharField('¿Ha realizado alguna estancia de posdoctorado?', max_length=1,choices=RESPUESTASINO)
    pregunta41 = models.CharField('¿Tiene planeado realizar una estancia de posdoctorado?', max_length=1,choices=RESPUESTASINO)
    pregunta42 = models.CharField('¿Cuenta con producción académica (Artículos cicnetíficos, capítulo de libro, libro, patente, reporte técnico, etc.) ', max_length=1,choices=RESPUESTASINO)
    pregunta43 = models.CharField('¿Pertenece al Sistema Nacional de Investigadores?', max_length=1,choices=NIVELSNI)


    indicacionP11 =('Indique si desea mantener vinculación académica / profesional con la Universidad Autónoma de Zacatecas, en los siguientes aspectos:')
    preguntaE44 = models.CharField('Indique si desea mantener vinculación académica / profesional con la Universidad Autónoma de Zacatecas, en los siguientes aspectos:'+
                                        'Como asesor o director de tesis',max_length=1,choices=RESPUESTASINO)
    preguntaE44_1 = models.CharField('Como asesor o director en prácticas profesionales de estudiantes en la organización donde labora',max_length=1,choices=RESPUESTASINO)
    preguntaE44_2 = models.CharField('Como conferencista',max_length=1,choices=RESPUESTASINO)
    preguntaE44_3 = models.CharField('Como docente/investigador ',max_length=1,choices=RESPUESTASINO)
    preguntaE44_4 = models.CharField('Como benefactor de la institución',max_length=1,choices=RESPUESTASINO)
    preguntaE44_5 = models.CharField('Como colaborador por parte de la organización donde labora',max_length=1,choices=RESPUESTASINO)
    preguntaE44_6 = models.CharField('Otro (indique)',max_length=1,choices=RESPUESTASINO)

class PreguntasEmpleadores(models.Model):
    empresa = models.CharField('Nombre de la empresa/organización',max_length=100, )
    paginaweb = models.CharField('Página Web de la empresa/organización ', max_length = 100)
    localización = models.CharField('Localización de la empresa/organización ', max_length = 60)
    contacto= models.CharField('Nombre de la persona de contacto ', max_length = 60)
    puestocontacto = models.CharField('Puesto de la persona de contacto', max_length = 20)
    correo = models.CharField('Correo Electrónico:', max_length=100)
    telefono = models.CharField('Teléfono', max_length=100)
    pregunta1 = models.CharField('Sector de la empresa/organización', max_length = 1, choices =EMPRESAORG)
    pregunta2 = models.CharField('Giro de servicio de la empresa/organización', max_length = 1, choices =SERVICIOSEMP)
    pregunta3 = models.CharField('Tamaño de la empresa/organización', max_length = 1, choices =TAMAÑOEMP)
    pregunta4 = models.CharField('Origen de la empresa/organización', max_length = 1, choices =TIPOEMPRESA)
    pregunta5 = models.CharField('Indique la principal fuente de reclutamiento de la empresa/organización', max_length = 1, choices =RECLUTAMIENTO)
    indicacion1=('De acuerdo a las políticas de su empresa/organización, indique la importancia de los siguientes '+
                                 'criterios de selección al momento de contratar a un egresado:')
    pregunta6_1 = models.CharField('De acuerdo a las políticas de su empresa/organización, indique la importancia de los siguientes '+
                                 'criterios de selección al momento de contratar a un egresado: '+
                                 'El sexo', max_length = 1, choices =PREGUNTA18)
    pregunta6_2 = models.CharField('La edad', max_length = 1, choices =PREGUNTA18)
    pregunta6_3 = models.CharField('La nacionalidad', max_length=1, choices=PREGUNTA18)
    pregunta6_4 = models.CharField('La institución del egresado', max_length=1, choices=PREGUNTA18)
    pregunta6_5 = models.CharField('El tipo de carrera cursada', max_length=1, choices=PREGUNTA18)
    pregunta6_6 = models.CharField('El promedio de calificaciones', max_length=1, choices=PREGUNTA18)
    pregunta6_7 = models.CharField('El estar titulado', max_length=1, choices=PREGUNTA18)
    pregunta6_8 = models.CharField('Las cartas de recomendación', max_length=1, choices=PREGUNTA18)
    pregunta6_9 = models.CharField('La presentación personal', max_length=1, choices=PREGUNTA18)
    pregunta6_10 = models.CharField('La entrevistas de selección', max_length=1, choices=PREGUNTA18)
    pregunta6_11 = models.CharField('Dominar de un segundo idioma', max_length=1, choices=PREGUNTA18)
    pregunta6_12 = models.CharField('Aprobar examen de conocimientos', max_length=1, choices=PREGUNTA18)
    pregunta6_13 = models.CharField('Aprobar examen psicométrico', max_length=1, choices=PREGUNTA18)
    pregunta6_14 = models.CharField('Aprobar examen de aptitudes', max_length=1, choices=PREGUNTA18)
    pregunta6_15 = models.CharField('La experiencia profesional', max_length=1, choices=PREGUNTA18)


    indicacio2 = ('De acuerdo a las políticas de su empresa/organización, indique la importancia de los '+
                  'siguientes criterios para asignar mejor sueldo a los egresados')
    pregunta7_1 = models.CharField('De acuerdo a las políticas de su empresa/organización, indique la importancia de los '+
                  'siguientes criterios para asignar mejor sueldo a los egresados' +
        'Los conocimientos de la disciplina', max_length=1, choices=PREGUNTA18)
    pregunta7_2 = models.CharField('La experiencia profesional', max_length=1, choices=PREGUNTA18)
    pregunta7_3 = models.CharField('La especialización de sus actividades', max_length=1, choices=PREGUNTA18)
    pregunta7_4 = models.CharField('El desempeño laboral', max_length=1, choices=PREGUNTA18)
    pregunta7_5 = models.CharField('El grado académico', max_length=1, choices=PREGUNTA18)
    pregunta7_6 = models.CharField('La antigüedad dentro de la empresa', max_length=1, choices=PREGUNTA18)
    pregunta7_7 = models.CharField('El dominio de una lengua extranjera', max_length=1, choices=PREGUNTA18)
    pregunta7_8 = models.CharField('Otro (indique)', max_length=1, choices=PREGUNTA18)

    indicacio3 = ('Mencione el nivel de importancia que su empresa/organización requiere de los empleados profesionistas '+
                  'en los siguientes aspectos, para el desempeño laboral.')

    pregunta8_1 = models.CharField(
        'Mencione el nivel de importancia que su empresa/organización requiere de los empleados profesionistas '+
                  'en los siguientes aspectos, para el desempeño laboral.' +
        'Capacidad de Análisis y síntesis', max_length=1, choices=PREGUNTA18)
    pregunta8_2 = models.CharField('Comunicación oral, escrita y gráfica', max_length=1, choices=PREGUNTA18)
    pregunta8_3 = models.CharField('Dominio de una lengua extranjera', max_length=1, choices=PREGUNTA18)
    pregunta8_4 = models.CharField('Habilidades para la investigación', max_length=1, choices=PREGUNTA18)
    pregunta8_5 = models.CharField('Liderazgo', max_length=1, choices=PREGUNTA18)
    pregunta8_6 = models.CharField('Manejo de tecnologías de la información (TIC’s)', max_length=1, choices=PREGUNTA18)
    pregunta8_7 = models.CharField('Organización y planificación', max_length=1, choices=PREGUNTA18)
    pregunta8_8 = models.CharField('Resolución de problemas', max_length=1, choices=PREGUNTA18)
    pregunta8_9 = models.CharField('Toma de decisiones', max_length=1, choices=PREGUNTA18)
    pregunta8_10 = models.CharField('Trabajo en equipo', max_length=1, choices=PREGUNTA18)

    pregunta9 = models.CharField('¿Trabajan actualmente personas egresadas de la Universidad Autónoma de Zacatecas '+
                                 'en esta empresa/organización?', max_length = 1, choices =RESPUESTASINO)
    pregunta10 = models.CharField('¿Cuántas personas egresadas de la Universidad Autónoma de Zacatecas laboran '+
                                  'actualmente en esta empresa/organización? ', max_length = 1, choices =PERSONASQUELABORAN)
    pregunta11 = models.CharField('¿Cuál es el principal tipo de contrato ofrecido a un egresado de la Universidad '+
                                  'Autónoma de Zacatecas? ', max_length = 1, choices =CONTRATO)
    pregunta12 = models.CharField('¿Cuál es el sueldo promedio mensual aproximado (incluido impuesto) que su empresa/organización'+
                                  ' ofrece a un profesionista egresado de la Universidad Autónoma de Zacatecas?', max_length = 2, choices =PREGUNTAE38)
    pregunta13 = models.CharField('¿Las funciones que desempeñan los egresados de la Universidad Autónoma de Zacatecas en su '+
                                  'empresa/organización tiene relación con su perfil profesional?', max_length = 1, choices =RESPUESTASINO)
    pregunta14 = models.CharField('Indique el puesto que ocupan los egresados de la Universidad Autónoma de Zacatecas en su '+
                                  'empresa/organización', max_length = 1, choices =PUESTO)

    indicacio4 = ('Mencione el nivel de satisfacción que tiene la empresa/organización con los empleados egresados de '+
                  'la Universidad Autónoma de Zacatecas, en las siguientes competencias, habilidades y actitudes.')

    pregunta15_1 = models.CharField('Mencione el nivel de satisfacción que tiene la empresa/organización con los empleados '
                                   'egresados de la Universidad Autónoma '+
                                    'de Zacatecas, en las siguientes competencias, habilidades y actitudes.' +
                                    'Liderazgo', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_2 = models.CharField('Trabajo en equipo', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_3 = models.CharField('Gestión, desarrollo y emprendimiento de proyectos', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_4 = models.CharField('Actitud ética ante los trabajos desarrollados', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_5 = models.CharField('Capacidad de argumentación', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_6 = models.CharField('Capacidad de iniciativa y toma de decisiones', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_7 = models.CharField('Pensamiento crítico y autocrítico', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_8 = models.CharField('Creatividad e innovación', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_9 = models.CharField('Empatía', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_10 = models.CharField('Asertividad', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_11 = models.CharField('Comunicación eficaz en forma oral, escrita y gráfica', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_12 = models.CharField('Comunicación oral, escrita y gráfica en un segundo idioma', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_13 = models.CharField('Manejo de datos y tecnologías de la información (TIC’s)', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_14 = models.CharField('Relaciones humanas', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_15 = models.CharField('Responsabilidad', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_16 = models.CharField('Uso del conocimiento, la experiencia y el razonamiento para emitir opiniones '+
                                    'fundamentadas', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_17 = models.CharField('Habilidad para encontrar soluciones', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_18 = models.CharField('Habilidad para la búsqueda de información', max_length=1, choices=PREGUNTA_SATISFACCION3)
    pregunta15_19 = models.CharField('habilidades para el manejo de paquetes computacionales', max_length=1, choices=PREGUNTA_SATISFACCION3)

    indicacio5 = ('En caso de que los egresados de la Universidad Autónoma de Zacatecas tengan un excelente o buen desempeño laboral, '+
                  'indique el impacto que esto tiene dentro de la empresa/organización, en los siguientes aspectos:')

    pregunta16_1 = models.CharField('En caso de que los egresados de la Universidad Autónoma de Zacatecas tengan un excelente o buen desempeño laboral, '+
                  'indique el impacto que esto tiene dentro de la empresa/organización, en los siguientes aspectos:' +
        'Obtención de mejor puesto', max_length=1, choices=POCONADA2)
    pregunta16_2 = models.CharField('Apoyo para mejorar su formación académica', max_length=1, choices=POCONADA2)
    pregunta16_3 = models.CharField('Mejora en sus condiciones laborales', max_length=1,
                                   choices=POCONADA2)
    pregunta16_4 = models.CharField('Mejora en sus prestaciones', max_length=1,
                                   choices=POCONADA2)
    pregunta16_5 = models.CharField('Estímulos económicos únicos', max_length=1, choices=POCONADA2)

    indicacio6 = ('Mencione la percepción general que tiene la empresa/organización respecto a los egresados de la Universidad Autónoma de Zacatecas')

    pregunta17_1 = models.CharField('Mencione la percepción general que tiene la empresa/organización respecto a los egresados de la Universidad Autónoma de Zacatecas' +
                                'La preparación académica de los egresados de la UAZ es similar o mejora la de egresados de otras instituciones educativas.'
                                    , max_length=1, choices=DEACUERDO2)
    pregunta17_2 = models.CharField('La preparación de los egresados de la UAZ les permitirá ocupar niveles superiores de la empresa.', max_length=1, choices=DEACUERDO2)

    pregunta18 = models.CharField('¿Qué tan probable es que su empresa/organización contrate a más egresados de la Universidad Autónoma de Zacatecas?', max_length = 1, choices =PROBABILIDAD)

    pregunta19 = models.CharField('¿Qué tan competido considera que está el campo laboral de la carrera del egresado contratado de la Universidad Autónoma de Zacatecas? ', max_length = 1, choices =COMPETITIVIDAD)
    pregunta20 = models.CharField('De acuerdo al contexto nacional, ¿considera que la carrera del egresado contratado de la Universidad Autónoma de Zacatecas es una buena opción en el futuro? ', max_length = 1, choices =RESPUESTASINO)
    pregunta21 = models.CharField('¿Visualiza algún cambio en el mercado laboral que afecte negativamente a los egresados de la Universidad Autónoma de Zacatecas? ', max_length = 1, choices =CAMBIOS)
    pregunta22 = models.CharField('Mencione algún cambio en el mercado laboral que afecte negativamente a las personas que cuentan con la carrera del egresado contratado de la Universidad Autónoma de Zacatecas', max_length = 100)
    pregunta23 = models.CharField('Mencione las habilidades/conocimientos/competencias que considere necesitan desarrollar los egresados de la Universidad Autónoma de Zacatecas para mejor desempeño laboral',max_length=100)
    pregunta24 = models.CharField('Indique sus recomendaciones, sugerencias o comentarios finales sobre los egresados de la Universidad Autónoma de Zacatecas',max_length=100)
    # pregunta1=models.CharField(,max_length=1,choices=)
