from django.contrib import admin
from .models import Alumno,PreguntasRecienEgresados,PreguntasEgresados,PreguntasPosgrado,PreguntasEmpleadores


admin.site.register(Alumno)
admin.site.register(PreguntasRecienEgresados)
admin.site.register(PreguntasEgresados)
admin.site.register(PreguntasPosgrado)
admin.site.register(PreguntasEmpleadores)



