from django import forms
from .models import *
#algo


class AlumnoForm(forms.ModelForm):
    genero = forms.ChoiceField(label='Género', choices=Alumno.OPCIONGENERO, widget=forms.RadioSelect())
    class Meta:
        model = Alumno
        # fields='__all__'
        exclude = ['user']
        
        # fields = ['apellidoM','nacimiento','domicilio','correo','facebook','telefono','genero','edad','estadoCivil','Nacionalidad','gradoEstudiosP','gradoEstudiosM']


class PreguntasRecienEgresadosForm(forms.ModelForm):
    # def __init__(self, idalumno, *args, **kwargs):
    #     super(PreguntasRecienEgresadosForm, self).__init__(*args, **kwargs)
    #     self.fields['alumno'].queryset = Alumno.objects.filter(alumno=idalumno)

    pregunta1 = forms.ChoiceField(label='Razón principal por la que decidió estudiar una licenciatura', choices=PREGUNTA1, widget=forms.RadioSelect())
    pregunta2 = forms.ChoiceField(label='Razón principal por la que decidió estudiar la licenciatura cursada', choices=PREGUNTA2, widget=forms.RadioSelect())

    class Meta:
        model = PreguntasRecienEgresados
        fields= '__all__'
        # fields = ['alumno','licenciatura','unidadAcademica','areac','fechaIngreso','fechaEgreso','pregunta1','pregunta2','pregunta3','pregunta4']

class PreguntasEgresadosForm(forms.ModelForm):
    # def __init__(self, idalumno, *args, **kwargs):
    #     super(PreguntasEgresadosForm, self).__init__(*args, **kwargs)
    #     self.fields['alumno'].queryset = Alumno.objects.filter(alumno=idalumno)

    class Meta:
        model = PreguntasEgresados
        # fields = '__all__'
        exclude = ['user']
        # fields = ['licenciatura', 'unidadAcademica', 'areac', 'fechaIngreso', 'fechaEgreso', 'preguntaE1', 'preguntaE2',
        #           'preguntaE3', 'preguntaE4', 'preguntaE5','preguntaE6', 'preguntaE7', 'preguntaE8', 'preguntaE9', 'preguntaE10_1', 'preguntaE10_2',
        #           'preguntaE10_3', 'preguntaE10_4', 'preguntaE10_5', 'preguntaE10_6',
        #           'preguntaE10_7', 'preguntaE10_8', 'preguntaE10_9', 'preguntaE11_1', 'preguntaE11_2', 'preguntaE11_3',
        #           'preguntaE11_4', 'preguntaE11_5', 'preguntaE11_6', 'preguntaE11_7',
        #           'preguntaE11_8', 'preguntaE11_9', 'preguntaE11_10','preguntaE11_11','preguntaE11_12', 'preguntaE12_1', 'preguntaE12_2', 'preguntaE12_3',
        #           'preguntaE12_4', 'preguntaE12_5', 'preguntaE12_6','preguntaE12_7', 'preguntaE12_8','preguntaE12_9', 'preguntaE13_1', 'preguntaE13_2', 'preguntaE13_3',
        #           'preguntaE13_4', 'preguntaE13_5', 'preguntaE13_6', 'preguntaE13_7', 'preguntaE13_8','preguntaE13_9',
        #           'preguntaE14_1', 'preguntaE14_2', 'preguntaE14_3', 'preguntaE14_4', 'preguntaE14_5',
        #           'preguntaE14_6', 'preguntaE14_7', 'preguntaE14_8', 'preguntaE14_9',
        #           'preguntaE14_10', 'preguntaE14_11', 'preguntaE14_12', 'preguntaE14_13','preguntaE14_14', 'preguntaE15',
        #           'preguntaE16', 'preguntaE17', 'preguntaE18','preguntaE19','preguntaE20',
        #           'preguntaE21_1', 'preguntaE21_2', 'preguntaE21_3', 'preguntaE21_4', 'preguntaE21_5','preguntaE21_5','preguntaE21_6',
        #           'preguntaE21_7','preguntaE21_8','preguntaE21_9','preguntaE21_10','preguntaE21_11','preguntaE21_12','preguntaE22',
        #           'pregunta23','preguntaE24','preguntaE24_1','preguntaE24_2','preguntaE24_3','preguntaE24_4','preguntaE24_5','preguntaE24_6',
        #           'preguntaE24_7','preguntaE24_8','preguntaE25','preguntaE26','preguntaE27','preguntaE28','preguntaE29','preguntaE29_1','preguntaE29_2',
        #           'preguntaE29_3','preguntaE29_4','preguntaE29_5','preguntaE29_6','preguntaE29_7','preguntaE30','preguntaE31','preguntaE32','preguntaE33',
        #           'preguntaE34','preguntaE35','preguntaE36','preguntaE36_1','preguntaE36_2','preguntaE36_3','preguntaE36_4','preguntaE36_5','preguntaE36_5',
        #           'preguntaE36_6','preguntaE36_7','preguntaE36_8','preguntaE36_9','preguntaE36_10','preguntaE36_11','preguntaE37','preguntaE38','preguntaE39',
        #           'preguntaE40','preguntaE41','preguntaE42','preguntaE42_1','preguntaE42_2','preguntaE42_3','preguntaE42_4','preguntaE43','preguntaE44',
        #           'preguntaE45','preguntaE46','preguntaE46_1','preguntaE46_2','preguntaE46_3','preguntaE46_4','preguntaE46_5','preguntaE46_6']

    # def clean_status(self):
    #     # when field is cleaned, we always return the existing model field.
    #     return self.instance.status

class PreguntasPosgradoForm(forms.ModelForm):
    class Meta:
        model = PreguntasPosgrado
        fields = '__all__'

class PreguntasEmpleadoresForm(forms.ModelForm):
    #def __init__(self, *args, **kwargs):
     #   super(PreguntasEmpleadoresForm, self).__init__(*args, **kwargs)
      #  instance = getattr(self, 'instance', None)
       # if instance and instance.pk:
        #    self.fields['empresa'].widget.attrs['disabled'] = True
         #   myform = PreguntasEmpleadoresForm(initial={'status': requested_status})
          #  myform.fields['status'].editable = False

  #  def clean_status(self):
        # when field is cleaned, we always return the existing model field.
   #     return self.instance.status

    class Meta:
        model=PreguntasEmpleadores
        fields='__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk and self.instance.empresa:
            self.fields['empresa'].widget.attrs.update({'disabled': true})
