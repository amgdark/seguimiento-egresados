from django.http import response
from django.test import TestCase, client
from django.contrib.auth.models import User
from django.urls.base import reverse

class TestViews(TestCase):
    def test_url_vinculacion(self):
        response = self.client.get('/vinculacion/')
        self.assertEqual(response.status_code, 200)


    def test_template_vinculacion(self):
        response = self.client.get('/vinculacion/')
        self.assertTemplateUsed(response, 'home.html')

    def test_url_datos(self):
        response = self.client.get('/vinculacion/encuestas/alumnos/datos')
        self.assertEqual(response.status_code, 200)


    def test_template_datos(self):
        response = self.client.get('/vinculacion/encuestas/alumnos/datos')
        self.assertTemplateUsed(response, 'encuestas/alumno_form.html')

    def test_url_recienegresados(self):
        response = self.client.get('/vinculacion/encuestas/alumnos/recienEgresados')
        self.assertEqual(response.status_code, 200)

    def test_template_recienegresados(self):
        response = self.client.get('/vinculacion/encuestas/alumnos/recienEgresados')
        self.assertTemplateUsed(response, 'encuestas/preguntasrecienegresados_form.html')

    def test_url_egresados(self):
        response = self.client.get('/vinculacion/encuestas/alumnos/egresados')
        self.assertEqual(response.status_code, 200)

    def test_template_recienegresados(self):
        response = self.client.get('/vinculacion/encuestas/alumnos/egresados')
        self.assertTemplateUsed(response, 'encuestas/preguntasegresados_form.html')

    def test_url_empleadores(self):
        response = self.client.get('/vinculacion/encuestas/empleadores')
        self.assertEqual(response.status_code, 200)

    def test_template_empleadores(self):
        response = self.client.get('/vinculacion/encuestas/empleadores')
        self.assertTemplateUsed(response, 'encuestas/preguntasempleadores_form.html')

    def test_url_egresadosposgrado(self):
        response = self.client.get('/vinculacion/encuestas/alumnos/egresadosPosgrado')
        self.assertEqual(response.status_code, 200)

    def test_template_egresadosposgrado(self):
        response = self.client.get('/vinculacion/encuestas/alumnos/egresadosPosgrado')
        self.assertTemplateUsed(response, 'encuestas/preguntasposgrado_form.html')