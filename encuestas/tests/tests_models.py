from django.http import response
from django.test import TestCase, client
from django.contrib.auth.models import User
from django.urls.base import reverse
from encuestas.models import Alumno, PreguntasEmpleadores

class TestModels(TestCase):
    @classmethod
    def setUpTestData(cls):
        Alumno.objects.create(nombre='Denisse',apellidoP='De Luna',apellidoM='Romo',nacimiento='Aguascalientes',domicilio='Francisco villa 1315',correo='30112717@uaz.edu.mx'
                              ,facebook='Denis de Luna',telefono='4921039318',genero='1',edad='23',
                              estadoCivil='2',Nacionalidad='1',gradoEstudiosP='8',gradoEstudiosM='4')

    def test_nombre_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'Nombres:')

    def test_apellidop_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('apellidoP').verbose_name
        self.assertEquals(field_label, 'Apellido Paterno:')

    def test_apellidom_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('apellidoM').verbose_name
        self.assertEquals(field_label, 'Apellido Materno:')

    def test_nacimiento_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('nacimiento').verbose_name
        self.assertEquals(field_label, 'Lugar de nacimiento')

    def test_domicilio_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('domicilio').verbose_name
        self.assertEquals(field_label, 'Domicilio fijo vigente')

    def test_correo_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('correo').verbose_name
        self.assertEquals(field_label, 'Correo Electrónico:')

    def test_facebook_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('facebook').verbose_name
        self.assertEquals(field_label, 'Facebook:')

    def test_telefono_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('telefono').verbose_name
        self.assertEquals(field_label, 'Teléfono:')

    def test_genero_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('genero').verbose_name
        self.assertEquals(field_label, 'Género:')

    def test_edad_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('edad').verbose_name
        self.assertEquals(field_label, 'edad')

    def test_estadoCivil_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('estadoCivil').verbose_name
        self.assertEquals(field_label, 'Estado Civil:')

    def test_Nacionalidad_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('Nacionalidad').verbose_name
        self.assertEquals(field_label, 'Nacionalidad:')

    def test_gradoEstudiosP_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('gradoEstudiosP').verbose_name
        self.assertEquals(field_label, 'Máximo grado de estudios del padre o tutor:')

    def test_gradoEstudiosM_label(self):
        alumno = Alumno.objects.get(id=1)
        field_label = Alumno._meta.get_field('gradoEstudiosM').verbose_name
        self.assertEquals(field_label, 'Máximo grado de estudios de la madre o tutora:')

class TestModelsEmpleadores(TestCase):
    @classmethod
    def setUpTestData(cls):
        PreguntasEmpleadores.objects.create(empresa='trucka',localización='salida Zac',contacto='Jesus',puestocontacto='Gerente',correo='jesus@hotmail.com',telefono='449156989',pregunta1='1',pregunta2='2',
                                            pregunta3='3',pregunta4='2',pregunta5='3',pregunta6_1='1',pregunta6_2='2',pregunta6_3='2',pregunta6_4='3',pregunta6_5='3',pregunta6_6='1',
                                            pregunta6_7='4',pregunta6_8='1',pregunta6_9='2',pregunta6_10='2',pregunta6_11='1',pregunta6_12='2',pregunta6_13='3',
                                            pregunta6_14='2',pregunta6_15='3',pregunta7_1='4',pregunta7_2='3',pregunta7_3='3',pregunta7_4='1',pregunta7_5='2',pregunta7_6='3',
                                            pregunta7_7='3',pregunta7_8='3',pregunta8_1='4',pregunta8_2='2',pregunta8_3='1',pregunta8_4='3',pregunta8_5='4',pregunta8_6='4',
                                            pregunta8_7='1',pregunta8_8='1',pregunta8_9='2',pregunta8_10='2',pregunta9='1',pregunta10='2',pregunta11='3',pregunta12='2',pregunta13='2',
                                            pregunta14='1',pregunta15_1='1',pregunta15_2='3',pregunta15_3='3',pregunta15_4='3',pregunta15_5='1',pregunta15_6='1',pregunta15_7='2',
                                            pregunta15_8='1',pregunta15_9='2',pregunta15_10='3',pregunta15_11='2',pregunta15_12='1',pregunta15_13='2',pregunta15_14='3',pregunta15_15='4',
                                            pregunta15_16='2',pregunta15_17='3',pregunta15_18='2',pregunta15_19='3',pregunta16_1='1',pregunta16_2='2',pregunta16_3='2',pregunta16_4='1',
                                            pregunta16_5='3',pregunta17_1='2',pregunta17_2='1',pregunta18='2',pregunta19='2',pregunta20='2',pregunta21='3',
                                            pregunta22='No aplica',pregunta23='No aplica',pregunta24='No aplica')

    def test_empresa_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('empresa').verbose_name
        self.assertEquals(field_label, 'Nombre de la empresa/organización')

    def test_paginaweb_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('paginaweb').verbose_name
        self.assertEquals(field_label, 'Página Web de la empresa/organización ')

    def test_localización_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('localización').verbose_name
        self.assertEquals(field_label, 'Localización de la empresa/organización ')

    def test_contacto_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('contacto').verbose_name
        self.assertEquals(field_label, 'Nombre de la persona de contacto ')

    def test_puestocontacto_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('puestocontacto').verbose_name
        self.assertEquals(field_label, 'Puesto de la persona de contacto')

    def test_correo_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('correo').verbose_name
        self.assertEquals(field_label, 'Correo Electrónico:')

    def test_telefono_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('telefono').verbose_name
        self.assertEquals(field_label, 'Teléfono')

    def test_pregunta1_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta1').verbose_name
        self.assertEquals(field_label, 'Sector de la empresa/organización')

    def test_pregunta2_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta2').verbose_name
        self.assertEquals(field_label, 'Giro de servicio de la empresa/organización')

    def test_pregunta3_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta3').verbose_name
        self.assertEquals(field_label, 'Tamaño de la empresa/organización')

    def test_pregunta4_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta4').verbose_name
        self.assertEquals(field_label, 'Origen de la empresa/organización')

    def test_pregunta5_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta5').verbose_name
        self.assertEquals(field_label, 'Indique la principal fuente de reclutamiento de la empresa/organización')

    def test_pregunta6_1_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_1').verbose_name
        self.assertEquals(field_label, 'De acuerdo a las políticas de su empresa/organización, indique la importancia de los siguientes '+
                                 'criterios de selección al momento de contratar a un egresado: '+
                                 'El sexo')

    def test_pregunta6_2_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_2').verbose_name
        self.assertEquals(field_label, 'La edad')

    def test_pregunta6_3_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_3').verbose_name
        self.assertEquals(field_label, 'La nacionalidad')

    def test_pregunta6_4_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_4').verbose_name
        self.assertEquals(field_label, 'La institución del egresado')

    def test_pregunta6_5_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_5').verbose_name
        self.assertEquals(field_label, 'El tipo de carrera cursada')

    def test_pregunta6_6_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_6').verbose_name
        self.assertEquals(field_label, 'El promedio de calificaciones')

    def test_pregunta6_7_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_7').verbose_name
        self.assertEquals(field_label, 'El estar titulado')

    def test_pregunta6_8_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_8').verbose_name
        self.assertEquals(field_label, 'Las cartas de recomendación')

    def test_pregunta6_9_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_9').verbose_name
        self.assertEquals(field_label, 'La presentación personal')

    def test_pregunta6_10_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_10').verbose_name
        self.assertEquals(field_label, 'La entrevistas de selección')

    def test_pregunta6_11_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_11').verbose_name
        self.assertEquals(field_label, 'Dominar de un segundo idioma')

    def test_pregunta6_12_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_12').verbose_name
        self.assertEquals(field_label, 'Aprobar examen de conocimientos')

    def test_pregunta6_13_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_13').verbose_name
        self.assertEquals(field_label, 'Aprobar examen psicométrico')

    def test_pregunta6_14_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_14').verbose_name
        self.assertEquals(field_label, 'Aprobar examen de aptitudes')

    def test_pregunta6_15_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta6_15').verbose_name
        self.assertEquals(field_label, 'La experiencia profesional')

    def test_pregunta7_1_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta7_1').verbose_name
        self.assertEquals(field_label, 'De acuerdo a las políticas de su empresa/organización, indique la importancia de los '+
                  'siguientes criterios para asignar mejor sueldo a los egresados' +
        'Los conocimientos de la disciplina')

    def test_pregunta7_2_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta7_2').verbose_name
        self.assertEquals(field_label, 'La experiencia profesional')

    def test_pregunta7_3_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta7_3').verbose_name
        self.assertEquals(field_label, 'La especialización de sus actividades')
    def test_pregunta7_4_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta7_4').verbose_name
        self.assertEquals(field_label, 'El desempeño laboral')

    def test_pregunta7_5_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta7_5').verbose_name
        self.assertEquals(field_label, 'El grado académico')

    def test_pregunta7_6_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta7_6').verbose_name
        self.assertEquals(field_label, 'La antigüedad dentro de la empresa')

    def test_pregunta7_7_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta7_7').verbose_name
        self.assertEquals(field_label, 'El dominio de una lengua extranjera')

    def test_pregunta7_8_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta7_8').verbose_name
        self.assertEquals(field_label, 'Otro (indique)')

    def test_pregunta8_1_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_1').verbose_name
        self.assertEquals(field_label, 'Mencione el nivel de importancia que su empresa/organización requiere de los empleados profesionistas '+
                  'en los siguientes aspectos, para el desempeño laboral.' +
        'Capacidad de Análisis y síntesis')

    def test_pregunta8_2_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_2').verbose_name
        self.assertEquals(field_label, 'Comunicación oral, escrita y gráfica')

    def test_pregunta8_3_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_3').verbose_name
        self.assertEquals(field_label, 'Dominio de una lengua extranjera')

    def test_pregunta8_4_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_4').verbose_name
        self.assertEquals(field_label, 'Habilidades para la investigación')

    def test_pregunta8_5_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_5').verbose_name
        self.assertEquals(field_label, 'Liderazgo')

    def test_pregunta8_6_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_6').verbose_name
        self.assertEquals(field_label, 'Manejo de tecnologías de la información (TIC’s)')

    def test_pregunta8_7_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_7').verbose_name
        self.assertEquals(field_label, 'Organización y planificación')

    def test_pregunta8_8_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_8').verbose_name
        self.assertEquals(field_label, 'Resolución de problemas')

    def test_pregunta8_9_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_9').verbose_name
        self.assertEquals(field_label, 'Toma de decisiones')

    def test_pregunta8_10_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta8_10').verbose_name
        self.assertEquals(field_label, 'Trabajo en equipo')

    def test_pregunta9_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta9').verbose_name
        self.assertEquals(field_label, '¿Trabajan actualmente personas egresadas de la Universidad Autónoma de Zacatecas '+
                                 'en esta empresa/organización?')

    def test_pregunta10_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta10').verbose_name
        self.assertEquals(field_label, '¿Cuántas personas egresadas de la Universidad Autónoma de Zacatecas laboran '+
                                  'actualmente en esta empresa/organización? ')

    def test_pregunta11_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta11').verbose_name
        self.assertEquals(field_label, '¿Cuál es el principal tipo de contrato ofrecido a un egresado de la Universidad '+
                                  'Autónoma de Zacatecas? ')

    def test_pregunta12_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta12').verbose_name
        self.assertEquals(field_label, '¿Cuál es el sueldo promedio mensual aproximado (incluido impuesto) que su empresa/organización'+
                                  ' ofrece a un profesionista egresado de la Universidad Autónoma de Zacatecas?')

    def test_pregunta13_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta13').verbose_name
        self.assertEquals(field_label, '¿Las funciones que desempeñan los egresados de la Universidad Autónoma de Zacatecas en su '+
                                  'empresa/organización tiene relación con su perfil profesional?')

    def test_pregunta14_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta14').verbose_name
        self.assertEquals(field_label, 'Indique el puesto que ocupan los egresados de la Universidad Autónoma de Zacatecas en su '+
                                  'empresa/organización')

    def test_pregunta15_1_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_1').verbose_name
        self.assertEquals(field_label, 'Mencione el nivel de satisfacción que tiene la empresa/organización con los empleados '
                                   'egresados de la Universidad Autónoma '+
                                    'de Zacatecas, en las siguientes competencias, habilidades y actitudes.' +
                                    'Liderazgo')

    def test_pregunta15_2_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_2').verbose_name
        self.assertEquals(field_label, 'Trabajo en equipo')

    def test_pregunta15_3_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_3').verbose_name
        self.assertEquals(field_label, 'Gestión, desarrollo y emprendimiento de proyectos')

    def test_pregunta15_4_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_4').verbose_name
        self.assertEquals(field_label, 'Actitud ética ante los trabajos desarrollados')

    def test_pregunta15_5_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_5').verbose_name
        self.assertEquals(field_label, 'Capacidad de argumentación')

    def test_pregunta15_6_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_6').verbose_name
        self.assertEquals(field_label, 'Capacidad de iniciativa y toma de decisiones')

    def test_pregunta15_7_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_7').verbose_name
        self.assertEquals(field_label, 'Pensamiento crítico y autocrítico')

    def test_pregunta15_8_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_8').verbose_name
        self.assertEquals(field_label, 'Creatividad e innovación')

    def test_pregunta15_9_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_9').verbose_name
        self.assertEquals(field_label, 'Empatía')

    def test_pregunta15_10_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_10').verbose_name
        self.assertEquals(field_label, 'Asertividad')

    def test_pregunta15_11_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_11').verbose_name
        self.assertEquals(field_label, 'Comunicación eficaz en forma oral, escrita y gráfica')

    def test_pregunta15_12_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_12').verbose_name
        self.assertEquals(field_label, 'Comunicación oral, escrita y gráfica en un segundo idioma')

    def test_pregunta15_13_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_13').verbose_name
        self.assertEquals(field_label, 'Manejo de datos y tecnologías de la información (TIC’s)')

    def test_pregunta15_14_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_14').verbose_name
        self.assertEquals(field_label, 'Relaciones humanas')

    def test_pregunta15_15_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_15').verbose_name
        self.assertEquals(field_label, 'Responsabilidad')

    def test_pregunta15_16_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_16').verbose_name
        self.assertEquals(field_label, 'Uso del conocimiento, la experiencia y el razonamiento para emitir opiniones fundamentadas')

    def test_pregunta15_17_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_17').verbose_name
        self.assertEquals(field_label, 'Habilidad para encontrar soluciones')

    def test_pregunta15_18_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_18').verbose_name
        self.assertEquals(field_label, 'Habilidad para la búsqueda de información')

    def test_pregunta15_19_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta15_19').verbose_name
        self.assertEquals(field_label, 'habilidades para el manejo de paquetes computacionales')

    def test_pregunta16_1_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta16_1').verbose_name
        self.assertEquals(field_label, 'En caso de que los egresados de la Universidad Autónoma de Zacatecas tengan un excelente o buen desempeño laboral, '+
                  'indique el impacto que esto tiene dentro de la empresa/organización, en los siguientes aspectos:' +
        'Obtención de mejor puesto')

    def test_pregunta16_2_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta16_2').verbose_name
        self.assertEquals(field_label, 'Apoyo para mejorar su formación académica')

    def test_pregunta16_3_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta16_3').verbose_name
        self.assertEquals(field_label, 'Mejora en sus condiciones laborales')

    def test_pregunta16_4_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta16_4').verbose_name
        self.assertEquals(field_label, 'Mejora en sus prestaciones')

    def test_pregunta16_5_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta16_5').verbose_name
        self.assertEquals(field_label, 'Estímulos económicos únicos')

    def test_pregunta17_1_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta17_1').verbose_name
        self.assertEquals(field_label, 'Mencione la percepción general que tiene la empresa/organización respecto a los egresados de la Universidad Autónoma de Zacatecas' +
                                'La preparación académica de los egresados de la UAZ es similar o mejora la de egresados de otras instituciones educativas.')

    def test_pregunta17_2_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta17_2').verbose_name
        self.assertEquals(field_label, 'La preparación de los egresados de la UAZ les permitirá ocupar niveles superiores de la empresa.')

    def test_pregunta18_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta18').verbose_name
        self.assertEquals(field_label, '¿Qué tan probable es que su empresa/organización contrate a más egresados de la Universidad Autónoma de Zacatecas?')

    def test_pregunta19_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta19').verbose_name
        self.assertEquals(field_label, '¿Qué tan competido considera que está el campo laboral de la carrera del egresado contratado de la Universidad Autónoma de Zacatecas? ')

    def test_pregunta20_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta20').verbose_name
        self.assertEquals(field_label, 'De acuerdo al contexto nacional, ¿considera que la carrera del egresado contratado de la Universidad Autónoma de Zacatecas es una buena opción en el futuro? ')

    def test_pregunta21_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta21').verbose_name
        self.assertEquals(field_label, '¿Visualiza algún cambio en el mercado laboral que afecte negativamente a los egresados de la Universidad Autónoma de Zacatecas? ')

    def test_pregunta22_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta22').verbose_name
        self.assertEquals(field_label, 'Mencione algún cambio en el mercado laboral que afecte negativamente a las personas que cuentan con la carrera del egresado contratado de la Universidad Autónoma de Zacatecas')

    def test_pregunta23_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta23').verbose_name
        self.assertEquals(field_label, 'Mencione las habilidades/conocimientos/competencias que considere necesitan desarrollar los egresados de la Universidad Autónoma de Zacatecas para mejor desempeño laboral')

    def test_pregunta24_label(self):
        empleador = PreguntasEmpleadores.objects.get(id=1)
        field_label = PreguntasEmpleadores._meta.get_field('pregunta24').verbose_name
        self.assertEquals(field_label, 'Indique sus recomendaciones, sugerencias o comentarios finales sobre los egresados de la Universidad Autónoma de Zacatecas')
