from django.contrib.auth import views as auth_views
from django.urls import path
from . import views

app_name='encuestas'

#Aqui se determinan las urls a las que se puede acceder a la página
urlpatterns = [
    path('alumnos/', views.AlumnosList.as_view(), name='alumnos'),
    path('alumnos/datos', views.AlumnosCrear.as_view(), name='datos'),
    path('alumnos/recienEgresados', views.RecienEgresadosCrear.as_view(), name='recienegresados'),
    path('alumnos/egresados', views.EgresadosCrear.as_view(), name='egresados'),
    path('empleadores', views.EmpleadoresCrear.as_view(), name='empleadores'),
    path('alumnos/egresadosPosgrado', views.EgresadosPosgradoCrear.as_view(), name='egresadosposgrado'),
    path('graficas/', views.Grafica.as_view(), name='graficas'),
]