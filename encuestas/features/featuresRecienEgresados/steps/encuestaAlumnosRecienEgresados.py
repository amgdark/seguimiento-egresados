from behave import when, then, given
from selenium import webdriver
import time



@given(u'que ingreso al sistema con la dirección "{url}",')
def step_impl(context, url):
    context.url = url
    time.sleep(2)


@given(u'me dirijo a la página principal "{pagina}"')
def step_impl(context, pagina):
    context.driver = webdriver.Chrome()
    context.driver.get(f"{context.url}{pagina}")
    time.sleep(2)


@given(u'me dirijo a la página de inicio de sesión "{link}"')
def step_impl(context, link):
    context.driver.find_element_by_link_text(link).click()
    time.sleep(4)


@given(u'capturo el usuario "{usuario}" y la contraseña "{contra}"')
def step_impl(context, usuario, contra):
    context.driver.find_element_by_name('username').send_keys(usuario)
    context.driver.find_element_by_name('password').send_keys(contra)
    time.sleep(2)


@given(u'hago click en el boton "Acceder"')
def step_impl(context):
    context.driver.find_element_by_xpath("/html/body/div[2]/form/div[3]/input").click()
    time.sleep(4)

@when(u'hago click en el boton "Iniciar Encuesta"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(4)
    context.driver.find_element_by_xpath('/html/body/div[3]/div/div/div[2]/a').click()
    time.sleep(4)

@then(u'se pueden ver "{Registro}"')
def step_impl(context,Registro):
    respuesta = context.driver.find_element_by_xpath('/html/body/header/div/div/h1').text
    assert Registro == respuesta, f"esperado es {Registro} y obtenido es {respuesta}"
    time.sleep(4)

@given(u'hago click en el boton "Iniciar Encuesta"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(4)
    context.driver.find_element_by_xpath('/html/body/div[3]/div/div/div[2]/a').click()
    time.sleep(4)


@given(u'puedo ver "{Registro}"')
def step_impl(context,Registro):
    respuesta = context.driver.find_element_by_xpath('/html/body/header/div/div/h1').text
    assert Registro == respuesta, f"esperado es {Registro} y obtenido es {respuesta}"
    time.sleep(4)


@given(u'agrego mis nombres "{nombres}"')
def step_impl(context,nombres):
    context.driver.find_element_by_name('nombre').send_keys(nombres)
    time.sleep(2)


@given(u'agrego mi apellido paterno "{apellido}"')
def step_impl(context,apellido):
    context.driver.find_element_by_name('apellidoP').send_keys(apellido)
    time.sleep(2)


@given(u'agrego mi apellido materno "{apellido}"')
def step_impl(context,apellido):
    context.driver.find_element_by_name('apellidoM').send_keys(apellido)
    time.sleep(2)


@given(u'agrego lugar de nacimiento "{nacimiento}"')
def step_impl(context,nacimiento):
    context.driver.execute_script("window.scrollTo(0,300);")
    time.sleep(2)
    context.driver.find_element_by_name('nacimiento').send_keys(nacimiento)
    time.sleep(2)


@given(u'agrego domicilio fijo vigente "{domiciliov}"')
def step_impl(context,domiciliov):
    context.driver.find_element_by_name('domicilio').send_keys(domiciliov)
    time.sleep(2)


@given(u'agrego correo electronico "{correoE}')
def step_impl(context,correoE):
    context.driver.find_element_by_name('correo').send_keys(correoE)
    time.sleep(2)


@given(u'agrego Facebook "{facebook}"')
def step_impl(context,facebook):
    context.driver.find_element_by_name('facebook').send_keys(facebook)
    time.sleep(2)


@given(u'agrego Teléfono  "{numero}"')
def step_impl(context,numero):
    context.driver.find_element_by_name('telefono').send_keys(numero)
    time.sleep(2)


@given(u'selecciono el genero "femenino"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0,600);")
    time.sleep(2)
    context.driver.find_element_by_xpath('/html/body/header/div/div/div/form/div[9]/ul/li[1]/label/input').click()
    time.sleep(2)


@given(u'agrego mi edad de "{edad}"')
def step_impl(context,edad):
    context.driver.find_element_by_name('edad').send_keys(edad)
    time.sleep(2)


@given(u'selecciono mi estado civil "Soltera"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_estadoCivil']/option[text()='Soltera(o)']").click()
    time.sleep(2)


@given(u'selecciono mi nacionalidad "Mexicana"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_Nacionalidad']/option[text()='Mexicana']").click()
    time.sleep(2)


@given(u'selecciono el grado de estudios de mi papá "Doctorado"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_gradoEstudiosP']/option[text()='Doctorado']").click()
    time.sleep(2)


@given(u'selecciono el grado de estudios de mi mamá "Preparatoria"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_gradoEstudiosM']/option[text()='Preparatoria']").click()
    time.sleep(2)


@when(u'hago click en el boton "Siguiente"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(4)
    context.driver.find_element_by_xpath('//*[@id="formulario"]/button').click()
    time.sleep(2)

@then(u'se pueden ver la página principal "{RecienEgresados}"')
def step_impl(context,RecienEgresados):
    respuesta = context.driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div/h2').text
    assert RecienEgresados == respuesta, f"esperado es {RecienEgresados} y obtenido es {RecienEgresados}"
    time.sleep(4)