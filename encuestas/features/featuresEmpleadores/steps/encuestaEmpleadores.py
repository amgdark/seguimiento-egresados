from behave import when, then, given
from selenium import webdriver
import time


@given(u'que ingreso al sistema con la dirección "{url}",')
def step_impl(context, url):
    context.url = url
    time.sleep(2)


@given(u'me dirijo a la página principal "{pagina}"')
def step_impl(context, pagina):
    context.driver = webdriver.Chrome()
    context.driver.get(f"{context.url}{pagina}")
    time.sleep(2)


@given(u'me dirijo a la página de inicio de sesión "{link}"')
def step_impl(context, link):
    context.driver.find_element_by_link_text(link).click()
    time.sleep(4)


@given(u'capturo el usuario "{usuario}" y la contraseña "{contra}"')
def step_impl(context, usuario, contra):
    context.driver.find_element_by_name('username').send_keys(usuario)
    context.driver.find_element_by_name('password').send_keys(contra)
    time.sleep(2)


@given(u'hago click en el boton "Acceder"')
def step_impl(context):
    context.driver.find_element_by_xpath("/html/body/div[2]/form/div[3]/input").click()
    time.sleep(4)

@when(u'hago click en el boton "Iniciar Encuesta"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(4)
    context.driver.find_element_by_xpath('/html/body/div[3]/div/div/div[2]/a').click()
    time.sleep(4)

@then(u'se pueden ver "Empleadores"')
def step_impl(context):
    context.driver.find_element_by_tag_name('h1')
    time.sleep(4)

@given(u'hago click en el boton "Iniciar Encuesta"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(4)
    context.driver.find_element_by_xpath('/html/body/div[3]/div/div/div[2]/a').click()
    time.sleep(4)


@given(u'agrego el nombre de la empresa "{empresa}"')
def step_impl(context,empresa):
    context.driver.find_element_by_id('id_empresa').send_keys(empresa)
    time.sleep(2)

@given(u'ingreso la página web "{pagina}"')
def step_impl(context,pagina):
    context.driver.find_element_by_id('id_paginaweb').send_keys(pagina)
    time.sleep(2)


@given(u'agrego la direccion "{direccion}"')
def step_impl(context,direccion):
    context.driver.find_element_by_id('id_localización').send_keys(direccion)
    time.sleep(2)


@given(u'agrego nombre de a persona de contacton "{nombre}"')
def step_impl(context,nombre):
    context.driver.find_element_by_id('id_contacto').send_keys(nombre)
    time.sleep(2)
    context.driver.execute_script("window.scrollTo(0,300);")
    time.sleep(2)

@given(u'agrego su puesto de "{puesto}"')
def step_impl(context,puesto):
    context.driver.find_element_by_id('id_puestocontacto').send_keys(puesto)
    time.sleep(2)


@given(u'agrego su correo electronico "{correo}"')
def step_impl(context,correo):
    context.driver.find_element_by_id('id_correo').send_keys(correo)
    time.sleep(2)


@given(u'agrego su número telefonico "{telefono}"')
def step_impl(context,telefono):
    context.driver.find_element_by_id('id_telefono').send_keys(telefono)
    time.sleep(2)
    context.driver.execute_script("window.scrollTo(0,600);")
    time.sleep(2)

@given(u'selecciono la organizacion de la empresa "Sector público/gobierno"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta1']/option[text()='Sector público/gobierno']").click()
    time.sleep(2)

@given(u'selecciono el giro de servicio de la empresa "industria"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta2']/option[text()='Servicios']").click()
    time.sleep(2)

@given(u'selecciono el tamaño de la empresa "entre 11 y 50 empleados (pequeña)"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta3']/option[text()='Entre 11 y 50 empleados (pequeña)']").click()
    time.sleep(2)

@given(u'selecciono el origen de la empresa/organizacion "Empresa Mexicana"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta4']/option[text()='Empresa mexicana']").click()
    time.sleep(2)


@given(u'selecciono la principal fuente de reclutamiento de la empresa "Bolsa de trabajo de la empresa/organización"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0,900);")
    time.sleep(2)
    context.driver.find_element_by_xpath("//select[@id='id_pregunta5']/option[text()='Bolsa de trabajo de la empresa/organización']").click()
    time.sleep(2)



@given(u'selecciono la importancia de los siguientes criterios para ser contratados, el sexo "Nada importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_1']/option[text()='Nada importante']").click()
    time.sleep(2)

@given(u'en la edad selecciono "Nada importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_2']/option[text()='Nada importante']").click()
    time.sleep(2)

@given(u'en la nacionalida "Nada importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_3']/option[text()='Nada importante']").click()
    time.sleep(2)

@given(u'en la institución egresada "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_4']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'en el tipo de carrera cursada "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_5']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'en el promedio de calificaciones "Importante"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0,1200);")
    time.sleep(2)
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_6']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'en el estar titulado "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_7']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'en las cartas de recomendacion "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_8']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'en la presentación personal "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_9']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'en la entrevistas de selección "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_10']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'en el dominio de una segunda lengua "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_11']/option[text()='Importante']").click()
    time.sleep(2)
    context.driver.execute_script("window.scrollTo(0,1600);")
    time.sleep(2)

@given(u'en el haber aprobado el examen de conocimientos "Poco importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_12']/option[text()='Poco importante']").click()
    time.sleep(2)

@given(u'en el haber aprobado el examen psicométrico "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_13']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'en el haber aprobado el examen de aptitudes "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_14']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'finalmente la experiencia profesioonal "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta6_15']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'de acuerdo a las politicas de la empresa se deben de evaluar los criterios para asignar un mejor sueldo, Los conocimientos de la disciplina: "Muy importante"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0,2000);")
    time.sleep(2)
    context.driver.find_element_by_xpath("//select[@id='id_pregunta7_1']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'en la experiencia profesional "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta7_2']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'en la especialización de sus actividades "poco importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta7_3']/option[text()='Poco importante']").click()
    time.sleep(2)

@given(u'en el desemnpeño laboral "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta7_4']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'en el grado académico "Muy importante"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0,2500);")
    time.sleep(2)
    context.driver.find_element_by_xpath("//select[@id='id_pregunta7_5']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'la antigüedad dentro de la empresa "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta7_6']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'el dominio de una lengua extranjera "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta7_7']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'finalmente en otro acuerdo "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta7_8']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'ahora se debe de mencionar el nivel de importancia que requieren los empleados para su desempeño laboral en su capacidad de analisis y síntesis "Poco importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_1']/option[text()='Poco importante']").click()
    time.sleep(2)

@given(u'en la comunicación oral, escrita y gráfica "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_2']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'en el dominio de una lengua extranjera "Poco importante"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0,2900);")
    time.sleep(2)
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_3']/option[text()='Poco importante']").click()
    time.sleep(2)

@given(u'en las habilidades para la inestigación "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_4']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'en el liderazgo "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_5']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'en el manejo de tecnologías "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_6']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'en la organización y planificación "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_7']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'en la resolución de problemas "Muy importante"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0,3300);")
    time.sleep(2)
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_8']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'en la toma de decisiones "Importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_9']/option[text()='Importante']").click()
    time.sleep(2)

@given(u'finalmente en el trabajo en equipo "Muy importante"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta8_10']/option[text()='Muy importante']").click()
    time.sleep(2)

@given(u'selecciono que "Si "trabajan personas egresadas de la UAZ')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta9']/option[text()='Si']").click()
    time.sleep(2)

@given(u'selecciono que "4 personas" trabajan en la empresa')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta10']/option[text()='4 personas']").click()
    time.sleep(2)

@given(u'selecciono que el contrato se ha hecho por "Contrado por honorarios"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta11']/option[text()='Contrato por honorarios']").click()
    time.sleep(2)

@given(u'selecciono el sueldo promedio "$5,000 a $8,000"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta12']/option[text()='$5,000 a $8,000']").click()
    time.sleep(2)

@given(u'selecciono que el treabajo desempeñado "Si" tiene relación')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta13']/option[text()='Si']").click()
    time.sleep(2)

@given(u'selecciono que el puesto que desempeña es de "Supervisor"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta14']/option[text()='Supervisor']").click()
    time.sleep(2)

@given(u'ahora se selecciona el nivel de satisfacción que tiene la empresa con los egresados por su liderazgo "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_1']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su trabajo en equipo "Excelente"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0,3700);")
    time.sleep(2)
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_2']/option[text()='Excelente']").click()
    time.sleep(2)

@given(u'por su gestion, desallorro y emprendimiento "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_3']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su actitud ética ante los trabajos desarrollados "Excelente"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_4']/option[text()='Excelente']").click()
    time.sleep(2)

@given(u'por la capacidad de argumentación "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_5']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su capacidad de iniciativa y toma de decisiones "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_6']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su pensamiento crítico y autocrítico "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_7']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su creatividad e innovación "Excelente"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_8']/option[text()='Excelente']").click()
    time.sleep(2)

@given(u'por su empatía "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_9']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su asertividad "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_10']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su comunicación eficaz en forma oral, escrita y gráfica "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_11']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su comunicación eficaz en forma oral, escrita y gráfica en un segundo idioma "Suficiente"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_12']/option[text()='Suficiente']").click()
    time.sleep(2)

@given(u'por su manejo de datos y tecnologías de la información "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_13']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por sus relaciones humanas "Excelente"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_14']/option[text()='Excelente']").click()
    time.sleep(2)

@given(u'por su responsabilidad "Excelente"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_15']/option[text()='Excelente']").click()
    time.sleep(2)

@given(u'por su uso del conocimiento, la experiencia y el razonamiento para emitir opiniones fundamentadas  "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_16']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su habilidad para encontrar soluciones "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_17']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'por su habilidad para la búsqueda de información "Excelente"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_18']/option[text()='Excelente']").click()
    time.sleep(2)

@given(u'finalmente por sus habilidades para el manejo de paquetes computacionales "Buena"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta15_19']/option[text()='Buena']").click()
    time.sleep(2)

@given(u'ahora se selecciona el impacto que los egresados tienen dentro de la empresa en los siguientes aspectos: Obtención de mejor puesto "Mucho"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta16_1']/option[text()='Mucho']").click()
    time.sleep(2)

@given(u'por el apoyo para mejorar su formación académica "Mucho"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta16_2']/option[text()='Mucho']").click()
    time.sleep(2)

@given(u'para la mejora en sus condiciones laborales "Mucho"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta16_3']/option[text()='Mucho']").click()
    time.sleep(2)

@given(u'para la mejora en sus prestaciones "Mucho"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta16_4']/option[text()='Mucho']").click()
    time.sleep(2)

@given(u'finalmente para los estímulos económicos únicos "Algo"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta16_5']/option[text()='Algo']").click()
    time.sleep(2)

@given(u'ahora se debe mencionar la percepción que tiene la empresa respecto a la UAZ respecto a su preparación académica es similar o mejora la de egresados de otras instituciones educativas "En desacuerdo "')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta17_1']/option[text()='En desacuerdo']").click()
    time.sleep(2)

@given(u'en la preparación de los egresados de la UAZ les permitirá ocupar niveles superiores de la empresa "Totalmente de acuerdo"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta17_2']/option[text()='Totalmente de acuerdo']").click()
    time.sleep(2)

@given(u'ahora se selecciona la probabilidad en que la empresa contrate a más egresados de la Universidad Autónoma de Zacatecas "Muy probable"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta18']/option[text()='Muy probable']").click()
    time.sleep(2)

@given(u'ahora se selecciona si el campo laboral del egresado es competido "Muy competido"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta19']/option[text()='Muy competido']").click()
    time.sleep(2)

@given(u'ahora se selecciona si la carrera del egresado contratado es una buena ocion para el futuro "Si"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta20']/option[text()='Si']").click()
    time.sleep(2)

@given(u'ahora se selecciona si se ve un cambio en el mercado laboral que afecte a los egresados "Ningun cambio"')
def step_impl(context):
    context.driver.find_element_by_xpath("//select[@id='id_pregunta21']/option[text()='Ningun cambio']").click()
    time.sleep(2)

@given(u'ahora se escribirá un cambio en el mercado laboral que afecte a las personas con la misma carrera del egresado "{respuesta}"')
def step_impl(context,respuesta):
    context.driver.find_element_by_id('id_pregunta22').send_keys(respuesta)
    time.sleep(2)


@given(u'ahora se escribirá las habilidades, conocimientos, competencias que se concideren "{respuesta}"')
def step_impl(context,respuesta):
    context.driver.find_element_by_id('id_pregunta23').send_keys(respuesta)
    time.sleep(2)

@given(u'ahora se escribira las recomentaciones o sugerencias sobre los egresados "{respuesta}"')
def step_impl(context,respuesta):
    context.driver.find_element_by_id('id_pregunta24').send_keys(respuesta)
    time.sleep(2)


@when(u'doy click en "Finalizar"')
def step_impl(context):
    context.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(4)
    context.driver.find_element_by_xpath('//*[@id="formulario3"]/div[85]/button').click()
    time.sleep(4)

@then(u'se puede ver "{Empleador}"')
def step_impl(context,Empleador):
    respuesta = context.driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div/h2').text
    assert Empleador == respuesta, f"esperado es {Empleador} y obtenido es {respuesta}"
    time.sleep(4)
