Característica: Como alumno de posgrado registrado quiero contestar
                la encuesta que me pertenece

    Escenario: iniciar encuesta correctamente
        Dado que ingreso al sistema con la dirección " http://127.0.0.1:8000/",
        Y me dirijo a la página principal "vinculacion"
        Y me dirijo a la página de inicio de sesión "INICIAR SESIÓN"
        Y capturo el usuario "30112720" y la contraseña "Denisse30112717"
        Y hago click en el boton "Acceder"
        Cuando hago click en el boton "Iniciar Encuesta"
        Entonces se pueden ver "Registro"

    Escenario: registro de datos exitoso
        Dado que ingreso al sistema con la dirección " http://127.0.0.1:8000/",
        Y me dirijo a la página principal "vinculacion"
        Y me dirijo a la página de inicio de sesión "INICIAR SESIÓN"
        Y capturo el usuario "30112720" y la contraseña "Denisse30112717"
        Y hago click en el boton "Acceder"
        y hago click en el boton "Iniciar Encuesta"
        y puedo ver "Registro"
        y agrego mis nombres "Denisse Estefanía"
        y agrego mi apellido paterno "De Luna"
        y agrego mi apellido materno "Romo"
        y agrego lugar de nacimiento "Aguascalientes"
        y agrego domicilio fijo vigente "Francisco Villa 1315"
        y agrego correo electronico "30112717@uaz.edu.mx"
        y agrego Facebook "Denis de Luna"
        y agrego Teléfono  "4921039318"
        y selecciono el genero "femenino"
        y agrego mi edad de "23"
        y selecciono mi estado civil "Soltera"
        y selecciono mi nacionalidad "Mexicana"
        y selecciono el grado de estudios de mi papá "Doctorado"
        y selecciono el grado de estudios de mi mamá "Preparatoria"
        Cuando hago click en el boton "Siguiente"
        Entonces se pueden ver la página principal "Egresado de Posgrado:"