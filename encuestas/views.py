from urllib import request

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import ListView
from django.views.generic.edit import CreateView
from django.views.generic.base import TemplateView
from django.http import HttpResponseRedirect
from .forms import Alumno, PreguntasRecienEgresados,PreguntasEgresados, AlumnoForm, PreguntasPosgrado,PreguntasRecienEgresadosForm,PreguntasEgresadosForm,PreguntasPosgradoForm,PreguntasEmpleadores,PreguntasEmpleadoresForm
from django.urls import reverse_lazy
from django.contrib.auth.models import Group,User




class AlumnosList(ListView):
    model=Alumno
    # recienE = User.objects.filter(groups__name__in=['alumno_rec_egresado'])
    # alumno = User.objects.filter(groups__name__in=['alumno'])
    # empleador = User.objects.filter(groups__name__in=['empleador'])
    # user = User.objects.all()
    # print("Holaaaaaaaaaaaaa")
    # print(recienE)
    # print(alumno)
    # print(empleador)




class AlumnosCrear(CreateView):
    model = Alumno
    form_class = AlumnoForm
    template_name = 'encuestas/alumno_form.html'
    # recienE = User.objects.filter(groups__name__in=['alumno_rec_egresado'])
    # alumno = User.objects.filter(groups__name__in=['alumno'])
    # empleador = User.objects.filter(groups__name__in=['empleador'])
    # user = User.objects.all() # no deben de ponber consultas directo en la clase, se debe de poner en algún método
    # success_url = 
    # print("Holaaaaaaaaaaaaa")
    # print(recienE)
    # print(alumno)
    # print(empleador)
    # def get_success_url(request):
    #     if request.user.groups.name == "alumno_rec_egresado":
    #            HttpResponseRedirect('/vinculacion/encuestas/alumnos/egresados')
    def get_success_url(self):
        # for grupo in self.request.user.groups.all():
        #     print (grupo) 
        if str(self.request.user.groups.all()[0]) == "alumno":
            url = '/vinculacion/encuestas/alumnos/egresados'
        else:
            url = None
        return url
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        
        return super().form_valid(form)
    
    
class RecienEgresadosCrear(CreateView):
    model = PreguntasRecienEgresados
    form_class = PreguntasRecienEgresadosForm
    #Metodo de create view, crear
    #render to response
    #formValid
    # def form_valid(self,form):
    #     self.object = form.save()
    #     groupAR = Group.objects.filter(name='alumno_rec_egresado').exists()
    #     return super().form_valid(form)
    #
    # def existe_usuario (user):
    #     usuario = user.groups.filter(name__in=['alumno_rec_egresado', 'posgrado_alumnoRecienE']).exists()
    #
    #     #return super().form_valid(form)
    '''
    def form_valid(self, form):
    """If the form is valid, save the associated model."""
    self.object = form.save()
    #self user = a que grupo pertenece
    return super().form_valid(form)
    '''
    success_url = reverse_lazy('eduacionapp:home2')

class EgresadosCrear(CreateView):
    model = PreguntasEgresados
    form_class = PreguntasEgresadosForm
    success_url = reverse_lazy('eduacionapp:home')
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        
        return super().form_valid(form)

class EgresadosPosgradoCrear(CreateView):
    model = PreguntasPosgrado
    form_class = PreguntasPosgradoForm
    success_url = reverse_lazy('eduacionapp:home2')

class EmpleadoresCrear(CreateView):
    template_name = 'encuestas/preguntasempleadores_form.html'
    model = PreguntasEmpleadores
    form_class = PreguntasEmpleadoresForm
    success_url = reverse_lazy('eduacionapp:home2')

    #myform = PreguntasEmpleadoresForm(initial={'status': requested_status})
    #myform.fields['status'].editable = False
    #myform.fields['status'].widget.attrs['readonly'] = True  # text input
    #myform.fields['status'].widget.attrs['disabled'] = True  # radio / checkbox

class Grafica(TemplateView):
    template_name = 'encuestas/graficas.html'
    
    def get(self, request, *args, **kwargs):
        respuestasRE=PreguntasRecienEgresados.objects.all()
        resultados ={
            '1':{
                '1': 0,
                '2': 0,
                '3': 0,
                '4': 0,
                '5': 0,
            },
            # '2':{
            #     '1': 0,
            #     '2': 0,
            #     '3': 0,
            #     '4': 0,
            #     '5': 0,
            # },

        }

        for respuesta in respuestasRE:    
            if ('1')==(respuesta.pregunta1):
                resultados['1']['1']=resultados['1']['1'] +1
            if ('2')==(respuesta.pregunta1):
                resultados['1']['2']=resultados['1']['2'] +1
            if ('3')==(respuesta.pregunta1):
                resultados[1][3] = resultados[1][3] +1
            if ('4')==(respuesta.pregunta1):
                resultados[1][4] = resultados[1][4] +1
            if ('5')==(respuesta.pregunta1):
                resultados[1][5] = resultados[1][5] +1
                
        
        # for r in resultados:

        #     for re in r:
        print(resultados)
        
        

        self.extra_context = {'resultados': resultados}
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

# @login_required
# def buscar_destinos(request):
#     destino=request.GET.get('destino')
#     destinos=[]
#     if destino:
#         destinos_encontrados=Destino.objects.filter(nombre__icontains=destino).values("id","nombre")
#         for d in destinos_encontrados:
#             destinos.append(d)
#     return JsonResponse({'status' : 200 , 'data' : destinos})